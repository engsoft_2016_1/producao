
#Tutorial para uso do Jasmine#

Nesta pasta encontram-se os arquivos de testes automatizados.


Nela temos um arquivo de comfiguração chamado [jasmine.json] que 
indica ao jasmine onde estão os arquivos de testes e quando devem ser executados.


Funciona da seguinte forma:

1. Depois do Jasmine ter sido instalado por npm, crie um novo projeto com o 
'Jasmine init'.

2. Logo depois será criada uma pasta com o nome [spec], com a seguinte estrutura:
spec
  --> support
        --> jasmine.json
        

3. Como estamos utilizando o Transpiler Typescript, nós primeiro compilamos pelo 
Typescript e geramos os arquivos .js.

4. Se quiser testar o código, crie um código de testes e coloque - o na pasta spec no formato 
[nome do arquivo].spec.js.nome

5. Execute no prompt de comando o jasmine.