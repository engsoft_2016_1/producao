var gulp = require('gulp');
var git = require('gulp-git');
var runSequence = require('run-sequence');
var ts = require('gulp-typescript');
var tsProject = ts.createProject("tsconfig.json");
var typedoc = require("gulp-typedoc");
var minify = require('gulp-minify');
 
var cwd = '.';
/*gulp.task('default', function() {
  // place code for your default task here
  var watcher = gulp.watch(cwd, ['git']);
  watcher.on('change',function(event){
    console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
  });
  watcher.on('unlink',function(event){
    console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
  });
  console.log("Watcher inicializado");
});*/


gulp.task('default', function(){
       gulp.watch(["www/aplication/controller/**/*.ts",
                  "www/aplication/service/**/*.ts",
                  "www/aplication/factory/**/*.ts"],
                   ['typescript']);
  });


gulp.task('typescript',  function(done) {
  try{
    runSequence('ts','tsDoc');
  }catch(err){
      throw err;
  } finally{
      done();  
  } 
});
 
gulp.task('ts', function () {
   var result = tsProject.src()
        .pipe(tsProject());
    return result.js.pipe(gulp.dest("www/aplication/build"));
});

gulp.task("tsDoc", function() {
    return gulp
        .src(["www/aplication/controller/**/*.ts",
              "www/aplication/service/**/*.ts",
              "www/aplication/factory/**/*.ts"
          ])
        .pipe(typedoc({
            module: "commonjs",
            target: "es6",
            out: "docs/",
            name: "Controle Colesterol",
            mode: "modules",
            exclude: "**/*.d.ts",
            includeDeclarations: false
        }))
    ;
});


gulp.task('mini', function() {
  gulp.src('www/aplication/build/**/*.js')
    .pipe(minify({
        ext:{
            src:'-debug.js',
            min:'.js'
        },
        exclude: ['tasks'],
        ignoreFiles: ['.combo.js', '-min.js']
    }))
    .pipe(gulp.dest('dist'))   

});



gulp.task('git',  function(done) {s
  try{
    runSequence('add','commit','push');
  }catch(err){
      throw err;
  } finally{
      done();  
  } 
});
       
gulp.task('add', function() {
    return gulp.src(cwd).pipe(git.add());
});

gulp.task('commit', function() {
   var stream;      
   var t; 
   t = gulp.src(cwd)
          .pipe( stream = git.commit(prompt("Digite o título para o commit:")))
          .on('data',function(data) {
            console.log(data);
              return t;
          })
          .on('error',function(){
            console.log("Nada para comitar!\n");
            return 0;
          });
  });

gulp.task('push',function(){ 
  git.push('origin','Producao', function(err){

          if(err){
              console.log('git push not finished sucessfull.');
              throw err;
          }else{
            console.log('finished sucessfull!');
          }
})});

