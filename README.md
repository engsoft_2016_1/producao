# Controle de colesterol #

## Prefácio do projeto ##

  Este projeto foi desenvolvido por alunos da Graduação de Sistemas de Informação da Universidade Federal de Sergipe durante  durante o curso de Engenharia de Software I, 2016.1, para o fim de avaliação dos alunos envolvidos.

## Como instalar o aplicativo em seu smartphone ##
  O aplicativo funciona na plataforma Android em versões 
  acima de 5.0. 

  1. Passe o arquivo .apk para o celular. Isso pode ser feito através do cabo
  USB.
  2. Clique no arquivo para intalá-lo.
  3. Siga os passos do instalador (Next...Next...).
  4. Ao final clique em "Abrir" e será executada a aplicação.

## Versonamento adotado ##
  
  [1].[2].[3].[4]
  
  [1] => nova tecnologia adicionada, removida, atualizada.
  [2] => nova funcionalidade adicionada, removida.
  [3] => novo módulo adicionado, removido.
  [4] => correções de código, bugs e manutenção.

  Exemplos:  0.0.0.0
             0.1.34.345
             12.23.345.601


## Tecnologias utilizadas ##
   
  Front - end
     
     HTML 5
     CSS 3
     Angular 1
     Javascript ES6
     Typescript 3.4.0
     Ionic 1
     ui-router
   
   IDE - Editores de código
      Visual Studio Code
      Notepad++
      Bloco de notas

   Ferramentas de ambiente -> Front - end

     Jarsmine  -      Framework para testes javascript.
     Typedoc   -      Framework para documentação de código.
     Gulp       -     Automatizador de tarefas.
     Bower      -     Gerenciador de dependências de front-end.
     Sass       -     Ferramenta de automatização de folhas de estilo.
     Weinre     -     Console-server para smartphones.
     Phonegap-CLi -   Servidor para testes nativos com aparelhos mobiles.

       
   Compilador -> Back - end 
     
     Node.js    -    Linguagem do servidor.

   Compilador -> Front-end
     tsc        -    Compilador do typescript.

   Ferramentas de ambiente -> Back - end
     
     Express   -      Framwork de um servidor Node.
     Npm       -      Gerenciador de depências de Back-end e front-end.

   Serviços PaaS

     OpenShift    - máquina virtual.
     Codeanywhere - IDE e máquina virtual.

   Builders
     Phonegapbiuld - Compilador na núvem.
     Cordova       - Compilador local.
     

Ferramentas     
    Plugins

        Banco de dados
          
          Cordova/PhoneGap SQLCipher adapter plugin.
          https://www.npmjs.com/package/com.peerio.cordova.plugin.sqlcipher



## Integrantes do projeto ##
  Carlos Alberto

  Wallacy Ronan

  
## Instruções para os colaboradores

Baixe os arquivos através do "git clone" para a sua máquina.
Depois é nescessário que você instale todas as dependências do projeto, que são as ferramentas e bibliotecas. Caso não queira instalar ou queira ter um ambiente pronto para programar em qualquer lugar, já existe uma máquina virtual na núvem com todas as feramentas já prontas no site Codeanywhere. Peça o acesso ao administrador deste repositório.

   !!! IMPORTANTE !!! - Antes de começar a programar, faça um git pull, e ao finalizar, comite as suas alterações e as envie com o git push. Uma outra forma (mas não recomendada) é usar o gulp. Caso tenha instalado o gulp em sua máquina, basta executar o CMD na raiz do projeto (no diretório do arquivo gulpfile.js), e digitar "gulp", depois pressione enter.
   
   
## Instruções mais usadas do git 

	git add --all

 Agrupa todas as alterações feitas.

    git commit -m "[mensagem]" 

Reúne as alterações em um commit e cria uma mensagem para o commit.

    git push origin [branch]

Envia os commits para o branch especificado.	

    git pull oririn [branch]

Atualiza os arquicos com os que estão armazenados na nuvem.

    git checkout [branch]

Troca o branch atual para o branch especificado.

    git merge [branch]

Faz a combinação entre o branch atual com o branch especificado.

