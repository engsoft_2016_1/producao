
var express = require('express');
var app = express();
var http = require('http').Server(app);

app.use(express.static('www'));

app.get('/', function (req, res) {
 res.sendFile('index.html');   
});

http.listen(3000,'0.0.0.0', function () {
  console.log('Example app listening on port 3000!');
});

