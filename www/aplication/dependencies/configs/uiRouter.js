app.config(function($stateProvider,$urlRouterProvider) {
  $stateProvider
  .state('index', {
    url: '/',
    templateUrl: 'aplication/view/visoes/telaPrincipal.html',
     controller: 'principal'
  })
   .state('resultado', {
    url: '/resultado',
    templateUrl: 'aplication/view/visoes/resultado.html',
    controller: 'resultado'
  })
  .state('estatinas', {
    url: '/estatinas',
    templateUrl: 'aplication/view/visoes/estatinas.html',
    controller: 'estatinas'
  })
 
  $urlRouterProvider.otherwise("/");
});