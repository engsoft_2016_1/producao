var app = angular.module('app', ['ionic','$actionButton']) ;
 app.service('dao', StorageDAO.DAO);
 app.service('juiz', JudgeService.Judge );
 app.service('referencialColesterol', 
              ReferencialColesterol.ReferencialColesterolFactory);
 app.service('calculoColesterol', ['referencialColesterol', 
                                    CalculoColesterolService.CalculadoraColesterolService]);
 app.service('contraindicacoes', ['dao', 
                                    ContraindicacoesService.Contraindicacoes]);
 app.service('resultadoPaciente', ['referencialColesterol',
                                    'calculoColesterol', 
                                    ResultadoColesterol.Resultado]);
 app.service('prescritor', ['dao',
                            'calculoColesterol',
                            'avaliador',
                            'juiz',
                            'referencialColesterol',
                            PrescritorPrincipioativo.Prescritor] );
 app.service('avaliador', AvaliadorPrincipioativoService.AvaliadorService );


 app.service('params', ParamsService.Params);

 app.controller('principal', ['$scope',
                               '$ionicPopup',
                               '$state',
                               'prescritor',
                               'resultadoPaciente',
                               'contraindicacoes',
                               'params',
                               '$ionicPopover',
                                PrincipalController]);

app.controller('resultado', ['$scope',
                               '$ionicPopup',
                               '$state',
                               '$stateParams',
                               'params',
                               '$actionButton',
                                ResultadoController]);   

app.controller('estatinas', ['$scope',
                               '$ionicPopup',
                               '$state',
                               '$stateParams',
                               'params',
                               '$actionButton',
                                EstatinasController]);   

