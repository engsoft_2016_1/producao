/// <reference path="../../../componentesNegocio/resultadoColesterol/businessObject/informacoesColesterol.ts" />
/// <reference path="../../../componentesNegocio/prescritorPrincipioativo/businessObject/prescritorPrincipioativo.ts" />
/**
 * @author Carlos Alberto
 * @module ParamsService
 * @version 0.0.0.1
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo manter objetos de parametros entre as views.
 * @description Data de criação : 12/04/2017
 * @description Data da última alteração : 12/04/2017
 * @file www/aplication/service/componentesAplicacao/params/businessObject/params.ts
 */
var ParamsService;
(function (ParamsService) {
    /**
    * Esta classe é responsável por compartilhar parametros entre estados
    * das views da aplicação.
    */
    var Params = (function () {
        function Params() {
        }
        Params.prototype.getteste = function () {
            return this.teste;
        };
        Params.prototype.setteste = function (v) {
            this.teste = v;
        };
        Params.prototype.getResultado = function () {
            return this.resultado;
        };
        Params.prototype.setResultado = function (v) {
            this.resultado = v;
        };
        Params.prototype.getEstatinas = function () {
            return this.estatinas;
        };
        Params.prototype.setEstatinas = function (v) {
            this.estatinas = v;
        };
        return Params;
    }());
    ParamsService.Params = Params;
})(ParamsService || (ParamsService = {}));
