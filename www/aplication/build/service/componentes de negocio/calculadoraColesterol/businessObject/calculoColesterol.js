/// <reference path="../intefaces/icalculadoraColesterol.ts" />
/**
 * @author Carlos Alberto
 * @module CalculoColesterol
 * @version 0.0.0.1
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo manter as funções que calculam os níveis de LDL, NoHDL e
 * o percentual de redução LDL.
 * @description Data de criação : 19/03/2017
 * @description Data da última alteração : 19/03/2017
 * @file www/aplication/service/calculadoraColesterol/calculoColesterol.ts
 */
var CalculoColesterol;
(function (CalculoColesterol) {
    /**
    * Esta classe é responsável por calcular os valores do colesterol LDL,NoHDL e de redução do LDL.
    */
    var CalculadoraColesterol = (function () {
        function CalculadoraColesterol() {
        }
        CalculadoraColesterol.prototype.calcularLDL = function (paciente) {
            var CT = paciente.getColesterolTotal();
            var hdl = paciente.getHdl();
            var TR = paciente.getTriglicerides();
            return (CT - hdl - (TR / 5));
        };
        CalculadoraColesterol.prototype.calcularNoHDL = function (paciente) {
            throw new Error('Method not implemented.');
        };
        CalculadoraColesterol.prototype.calcularPercentualReducaoLDL = function (paciente) {
            throw new Error('Method not implemented.');
        };
        return CalculadoraColesterol;
    }());
    CalculoColesterol.CalculadoraColesterol = CalculadoraColesterol;
})(CalculoColesterol || (CalculoColesterol = {}));
