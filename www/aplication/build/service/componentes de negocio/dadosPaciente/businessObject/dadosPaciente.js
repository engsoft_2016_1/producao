/**
 * @author Carlos Alberto
 * @module DadosPaciente
 * @version 0.0.0.1
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo armazenar as informações que forem passadas pelo
 * médico sobre o paciente.
 * @description Data de criação : 19/03/2017
 * @description Data da última alteração : 19/03/2017
 * @file www/aplication/service/dadosPaciente/dadosPaciente.ts
 */
var DadosPaciente;
(function (DadosPaciente) {
    /**
    * Esta classe agrupa as informações informadas pelo médico sobre o paciente na tela principal.
    */
    var Paciente = (function () {
        function Paciente(idFaixaEtaria, colesterolTotal, hdl, triglicerides, ldl, noHdl, contraindicacoes) {
            if (idFaixaEtaria === void 0) { idFaixaEtaria = 0; }
            if (colesterolTotal === void 0) { colesterolTotal = 0; }
            if (hdl === void 0) { hdl = 0; }
            if (triglicerides === void 0) { triglicerides = 0; }
            if (ldl === void 0) { ldl = 0; }
            if (noHdl === void 0) { noHdl = 0; }
            if (contraindicacoes === void 0) { contraindicacoes = []; }
        }
        Paciente.prototype.setIdFaixaEtaria = function (v) {
            this.idFaixaEtaria = v;
        };
        Paciente.prototype.getIdFaixaEtaria = function () {
            return this.idFaixaEtaria;
        };
        Paciente.prototype.setColesterolTotal = function (v) {
            this.colesterolTotal = v;
        };
        Paciente.prototype.getColesterolTotal = function () {
            return this.colesterolTotal;
        };
        Paciente.prototype.setHdl = function (v) {
            this.hdl = v;
        };
        Paciente.prototype.getHdl = function () {
            return this.hdl;
        };
        Paciente.prototype.setTriglicerides = function (v) {
            this.triglicerides = v;
        };
        Paciente.prototype.getTriglicerides = function () {
            return this.triglicerides;
        };
        Paciente.prototype.setLdl = function (v) {
            this.ldl = v;
        };
        Paciente.prototype.getLdl = function () {
            return this.ldl;
        };
        Paciente.prototype.setNoHdl = function (v) {
            this.noHdl = v;
        };
        Paciente.prototype.getNoHdl = function () {
            return this.noHdl;
        };
        Paciente.prototype.setContraindicacoes = function (v) {
            this.contraindicacoes = v;
        };
        Paciente.prototype.getContraindicacoes = function () {
            return this.contraindicacoes;
        };
        return Paciente;
    }());
    DadosPaciente.Paciente = Paciente;
})(DadosPaciente || (DadosPaciente = {}));
