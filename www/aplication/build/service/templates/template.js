/**
 * @author Carlos Alberto
 * @module MyModule
 * @version 0.0.0.1
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo ser um template de
 * documentação de código Typescript.
 * @description Data de criação : 02/03/2017
 * @description Data da última alteração : 02/03/2017
 * @file www/aplication/service/templates/template.js
 */
var MyModule;
(function (MyModule) {
    /**
     * Esta Classe que representa um exemplo de documentação.
     * @export
     */
    var Example = (function () {
        /**
        * Exemplo de documentação de um construtor.
        * @param parameters {any}  - aqui parametros do construtor da Classe
        * são descritos e o tipo é colocado entre parênteses.
        */
        function Example(parameters) {
            this.name = parameters;
        }
        /**
         * Exemplo de documentação de uma operação/metodo/função.
         * @param params {any} - descrição do parâmetro e seu tipo.
         */
        Example.prototype.setName = function (params) {
            this.name = params;
        };
        /**
         * Exemplo de documentação de uma operação/metodo/function com notação lâmbda
         * @returns {String}
         */
        Example.prototype.getName = function () {
            return 'text';
        };
        Example.prototype.showName = function () {
            throw new Error('Method not implemented.');
        };
        return Example;
    }());
    MyModule.Example = Example;
    /**
     * Esta Classe que representa um exemplo de implementação
     * de uma interface.
     */
    var Teste2 = (function () {
        function Teste2() {
        }
        Teste2.prototype.showName = function () {
            throw new Error('Method not implemented.');
        };
        return Teste2;
    }());
})(MyModule || (MyModule = {}));
