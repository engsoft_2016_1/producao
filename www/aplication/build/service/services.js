/// <reference path="../factory/banco/config/storage.ts"/>
/**
 * Classe responsável por disponibilizar os serviços aos
 * controllers.
 */
var ServiceFacade = (function () {
    function ServiceFacade(dao) {
        this.factory = dao;
    }
    ServiceFacade.prototype.servico = function () {
    };
    return ServiceFacade;
}());
