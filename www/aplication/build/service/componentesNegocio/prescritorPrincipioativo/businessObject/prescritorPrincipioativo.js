/// <reference path="../interfaces/iservicePrescritor.ts" />
/// <reference path="../../../../factory/banco/interfaceBD/IFacadeBD.ts" />
/// <reference path="../../calculadoraColesterol/interfaces/icalculadoraColesterol.ts" />
/// <reference path="item.ts" />
/// <reference path="../../avaliador/interfaces/iserviceAvaliador.ts" />
/// <reference path="../../judge/businessObject/judge.ts" />
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t;
    return { next: verb(0), "throw": verb(1), "return": verb(2) };
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
/**
 * @author Carlos Alberto
 * @module PrescritorPrincipioativo
 * @version 0.0.0.1
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo s funções que calculam os níveis de LDL, NoHDL e
 * o percentual de redução LDL.
 * @description Data de criação : 19/03/2017
 * @description Data da última alteração : 19/03/2017
 * @file www/aplication/service/calculadoraColesterol/calculoColesterol.ts
 */
var PrescritorPrincipioativo;
(function (PrescritorPrincipioativo) {
    /**
    * Esta classe é responsável por calcular os valores do colesterol LDL,NoHDL e de redução do LDL.
    */
    var Prescritor = (function () {
        function Prescritor(dao, calculoColesterol, avaliador, judge) {
            this.dao = dao;
            this.calculoColesterol = calculoColesterol;
            this.avaliador = avaliador;
            this.judge = judge;
        }
        Prescritor.prototype.consultarPrincipioativo = function (paciente) {
            return __awaiter(this, void 0, void 0, function () {
                var _this = this;
                var ldl, percentualReducao, itens, array, itensJulgados;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            ldl = this.calculoColesterol.calcularLDL(paciente);
                            if (ldl < 0) {
                                return [2 /*return*/, Promise.reject(new Error('Ooops...Parece que algum dos dados'
                                        + ' de colesterol estão incorretos. Por favor corrija os valores do '
                                        + 'Colesterol Toral, HDL-C, ou Triglicerídeos. '))];
                            }
                            paciente.setLdl(ldl);
                            console.log('Prescritor : valor do LDL paciente = ' + paciente.getLdl());
                            return [4 /*yield*/, this.calculoColesterol.calcularPercentualReducaoLDL(paciente).then(function (res) {
                                    percentualReducao = res;
                                })];
                        case 1:
                            _a.sent();
                            console.log('Prescritor : percentual de redução LDL = ' + percentualReducao);
                            paciente.setPercentualReducaoLdl(percentualReducao);
                            return [4 /*yield*/, this.calculoColesterol.calcularPercentualReducaoTG(paciente).then(function (res) {
                                    console.log('Prescritor : percentual de redução TG = ' + res);
                                    paciente.setPercentualReducaoTg(res);
                                })];
                        case 2:
                            _a.sent();
                            return [4 /*yield*/, this.calculoColesterol.calcularPercentualAumentoHdl(paciente).then(function (res) {
                                    console.log('Prescritor : percentual de aumento HDL = ' + res);
                                    paciente.setPercentualAumentoHdl(res);
                                })];
                        case 3:
                            _a.sent();
                            itens = [];
                            return [4 /*yield*/, this.dao.obterPrincipiosativos().then(function (res) {
                                    console.log('Prescritor : quantidade de princ, retornados ' + res.length);
                                    for (var i = 0; i < res.length; i++) {
                                        itens[i] = new PrescritorPrincipioativo.Item(res[i]);
                                        itens[i].setDadosPaciente(paciente);
                                    }
                                }).catch(function (reason) {
                                    console.log('PrescritorPrincipioativo : ' + reason.message);
                                    return Promise.resolve([]);
                                })];
                        case 4:
                            _a.sent();
                            array = itens.map(function (e) {
                                return new Promise(function (resolve, reject) {
                                    _this.dao.obterContraindicacaoPrincipioativo(e.getPrincipioativo().getId(), paciente.getContraindicacoes())
                                        .then(function (res) {
                                        e.setContraindicacoes(res);
                                        resolve(e);
                                    }).catch(function (err) {
                                        console.log('PrescritorPrincipioativo : Error = ' + err.message);
                                        resolve(e);
                                    });
                                });
                            });
                            itensJulgados = [];
                            return [4 /*yield*/, Promise.all(array).then(function (list) {
                                    console.log('Prescritor : setPercentualReducaoTg = ' + paciente.getPercentualReducaoTg());
                                    list.forEach(function (e) {
                                        console.log('Nome :' + e.getPrincipioativo().getNome());
                                        e.getContraindicacoes().forEach(function (e) {
                                            console.log('Contraindicação ' + e.getNome());
                                        });
                                    });
                                    //os principios ativos seriam passados para o avaliador.
                                    var itensAvaliados = _this.avaliador.avaliar(list);
                                    //o juiz comparará os valores de todos os requisitos para obter as melhores estatinas.
                                    itensJulgados = _this.judge.julgar(itensAvaliados);
                                }).catch(function (err) {
                                    console.log('PrescritorPrincipioativo : Error = ' + err.message);
                                })];
                        case 5:
                            _a.sent();
                            /*});*/
                            return [2 /*return*/, Promise.resolve(itensJulgados)];
                    }
                });
            });
        };
        return Prescritor;
    }());
    PrescritorPrincipioativo.Prescritor = Prescritor;
})(PrescritorPrincipioativo || (PrescritorPrincipioativo = {}));
