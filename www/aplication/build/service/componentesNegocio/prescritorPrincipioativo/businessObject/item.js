/// <reference path="../../dadosPaciente/businessObject/dadosPaciente.ts" />
/// <reference path="../../../../factory/banco/servicesBD/principioativo/businessObject/principioativo.ts" />
/// <reference path="../../../../factory/banco/servicesBD/contraindicacao/businessObject/contraindicacao.ts" />
var PrescritorPrincipioativo;
(function (PrescritorPrincipioativo) {
    /**
    * Esta classe agrupa cada principioativo com metadados
    * que serão gerados posteriormente.
    */
    var Item = (function () {
        function Item(principioativo) {
            this.pontuacaoReducaoLdl = 0;
            this.pontuacaoReducaoTg = 0;
            this.pontuacaoAumentoHdl = 0;
            this.dadosPaciente = null;
            this.contraindicacoes = [];
            this.total = 0;
            this.principioativo = principioativo;
        }
        /**
          * Obtém um objeto do tipo Principioativo.
          */
        Item.prototype.getPrincipioativo = function () {
            return this.principioativo;
        };
        /**
         * Retorna a pontuação dada pelo avaliador quanto a redução do LDL.
         */
        Item.prototype.getPontuacaoReducaoLdl = function () {
            return this.pontuacaoReducaoLdl;
        };
        /**
         * Define a pontuação dada pelo avaliador quanto a redução do LDL.
         */
        Item.prototype.setPontuacaoReducaoLdl = function (v) {
            this.pontuacaoReducaoLdl = v;
        };
        /**
        * Obtém o resultado da pontuação dada pelo avaliador quanto a redução do triglicérides.
        */
        Item.prototype.getPontuacaoReducaoTg = function () {
            return this.pontuacaoReducaoTg;
        };
        /**
         * Define a pontuação dada pelo avaliador quanto a redução do triglicérides.
         */
        Item.prototype.setPontuacaoReducaoTg = function (v) {
            this.pontuacaoReducaoTg = v;
        };
        /**
        * Obtém o resultado da pontuação dada pelo avaliador quanto a redução do triglicérides.
        */
        Item.prototype.getPontuacaoAumentoHdl = function () {
            return this.pontuacaoAumentoHdl;
        };
        /**
         * Define a pontuação dada pelo avaliador quanto a redução do triglicérides.
         */
        Item.prototype.setPontuacaoAumentoHdl = function (v) {
            this.pontuacaoAumentoHdl = v;
        };
        /**
         * Retorna a pontuação total dada pelo avaliador com base em todos os requisitos
         * avaliados. Quanto maior este resultado, mais este principio deve ser indicado.
         */
        Item.prototype.getTotal = function () {
            return this.total;
        };
        /**
        * Define a pontuação total dada pelo avaliador com base em todos os requisitos
        * avaliados. Quanto maior este resultado, mais este principio deve ser indicado.
        */
        Item.prototype.setTotal = function (v) {
            this.total = v;
        };
        /**
         * Retorna os dados do paciente que foram usados como base na classificação do
         * principioativo.
         */
        Item.prototype.getDadosPaciente = function () {
            return this.dadosPaciente;
        };
        /**
         * Define os dados do paciente que foram usados como base na classificação do
         * principioativo.
         */
        Item.prototype.setDadosPaciente = function (v) {
            this.dadosPaciente = v;
        };
        /**
        * Retorna as informações de contraindicações que casam com as que
        *  esta estatina possui.
        */
        Item.prototype.getContraindicacoes = function () {
            return this.contraindicacoes;
        };
        /**
         * Define as informações de contraindicações que casam com as que
         *  esta estatina possui.
         */
        Item.prototype.setContraindicacoes = function (v) {
            this.contraindicacoes = v;
        };
        return Item;
    }());
    PrescritorPrincipioativo.Item = Item;
})(PrescritorPrincipioativo || (PrescritorPrincipioativo = {}));
