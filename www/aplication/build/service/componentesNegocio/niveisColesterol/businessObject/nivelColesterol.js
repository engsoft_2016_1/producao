/// <reference path="../interfaces/ireferencial.ts" />
/// <reference path="../interfaces/ifactory.ts" />
/// <reference path="../businessObject/referenciais/adultosAcima20anos.ts" />
/**
 * @author Carlos Alberto
 * @module ReferencialColesterol
 * @version 0.0.0.1
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo de armazenar os valores de cada categoria dos referenciais lipídicos
 * e exibir cada nível de colesterol do paciente.
 * @description Data de criação : 27/03/2017
 * @description Data da última alteração : 27/03/2017
 * @file www/aplication/service/componentesNegocio/niveisColesterol/businessObject/niveisColesterol.ts
 */
var ReferencialColesterol;
(function (ReferencialColesterol) {
    /**
    * Esta classe é responsável por disponibilizar os dados dos referênciais lipidicos
    * e seus níveis de colesterol.
    */
    var ReferencialColesterolFactory = (function () {
        function ReferencialColesterolFactory() {
            this.referenciaisMap = new Map();
            this.referenciaisMap.set(ReferencialColesterol.AdultosAcima20Anos.keyReferencial, new ReferencialColesterol.AdultosAcima20Anos());
        }
        ReferencialColesterolFactory.prototype.obterReferencial = function (key) {
            return this.referenciaisMap.get(key);
        };
        return ReferencialColesterolFactory;
    }());
    ReferencialColesterol.ReferencialColesterolFactory = ReferencialColesterolFactory;
})(ReferencialColesterol || (ReferencialColesterol = {}));
