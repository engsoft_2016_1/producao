/// <reference path="../categoria.ts" />
var ReferencialColesterol;
(function (ReferencialColesterol) {
    /**
     * Esta classe representa o refrencial de adultos com faixa etária
     * acima dos 20 anos.
     */
    var AdultosAcima20Anos = (function () {
        function AdultosAcima20Anos() {
            this.colesterolTotal = [
                new ReferencialColesterol.Categoria('Desejável', 0, 199),
                new ReferencialColesterol.Categoria('Limítrofe', 200, 239),
                new ReferencialColesterol.Categoria('Alto', 240, 0)
            ];
            this.triglicerides = [
                new ReferencialColesterol.Categoria('Desejável', 0, 149),
                new ReferencialColesterol.Categoria('Limítrofe', 150, 199),
                new ReferencialColesterol.Categoria('Alto', 200, 499),
                new ReferencialColesterol.Categoria('Muito Alto', 500, 0)
            ];
            this.hdl = [
                new ReferencialColesterol.Categoria('Desejável', 61, 0),
                new ReferencialColesterol.Categoria('Limítrofe', 40, 60),
                new ReferencialColesterol.Categoria('Baixo', 0, 40)
            ];
            this.ldl = [
                new ReferencialColesterol.Categoria('Ótimo', 0, 99),
                new ReferencialColesterol.Categoria('Desejável', 100, 129),
                new ReferencialColesterol.Categoria('Limitrofe', 130, 159),
                new ReferencialColesterol.Categoria('Alto', 160, 189),
                new ReferencialColesterol.Categoria('Muito Alto', 190, 0)
            ];
            this.noHdl = [
                new ReferencialColesterol.Categoria('Ótimo', 0, 129),
                new ReferencialColesterol.Categoria('Desejável', 130, 159),
                new ReferencialColesterol.Categoria('Alto', 160, 189),
                new ReferencialColesterol.Categoria('Muito Alto', 190, 0)
            ];
        }
        AdultosAcima20Anos.prototype.getColesterolTotal = function () {
            return this.colesterolTotal;
        };
        AdultosAcima20Anos.prototype.setColesterol = function (v) {
            this.colesterolTotal = v;
        };
        AdultosAcima20Anos.prototype.getTriglicerides = function () {
            return this.triglicerides;
        };
        AdultosAcima20Anos.prototype.setTriglicerides = function (v) {
            this.triglicerides = v;
        };
        AdultosAcima20Anos.prototype.getHdl = function () {
            return this.hdl;
        };
        AdultosAcima20Anos.prototype.setHdl = function (v) {
            this.hdl = v;
        };
        AdultosAcima20Anos.prototype.getLdl = function () {
            return this.ldl;
        };
        AdultosAcima20Anos.prototype.setLdl = function (v) {
            this.ldl = v;
        };
        AdultosAcima20Anos.prototype.getNoHdl = function () {
            return this.noHdl;
        };
        AdultosAcima20Anos.prototype.setNoHdl = function (v) {
            this.noHdl = v;
        };
        return AdultosAcima20Anos;
    }());
    AdultosAcima20Anos.keyReferencial = 1;
    ReferencialColesterol.AdultosAcima20Anos = AdultosAcima20Anos;
})(ReferencialColesterol || (ReferencialColesterol = {}));
