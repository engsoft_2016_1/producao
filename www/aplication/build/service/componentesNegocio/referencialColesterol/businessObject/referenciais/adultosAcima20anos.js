/// <reference path="../categoria.ts" />
/// <reference path="../../../referencialColesterol/enums/tipoCategoria.ts" />
/// <reference path="../../interfaces/ireferencial.ts" />
var ReferencialColesterol;
(function (ReferencialColesterol) {
    /**
     * Esta classe representa o refrencial de adultos com faixa etária
     * acima dos 20 anos.
     */
    var AdultosApartir20Anos = (function () {
        function AdultosApartir20Anos() {
            this.colesterolTotal = [
                new ReferencialColesterol.Categoria(1 /* Desejavel */, 'Desejável', Number.MIN_VALUE, 199),
                new ReferencialColesterol.Categoria(2 /* Limitrofe */, 'Limítrofe', 200, 239),
                new ReferencialColesterol.Categoria(3 /* Alto */, 'Alto', 240, Number.MAX_VALUE)
            ];
            this.triglicerides = [
                new ReferencialColesterol.Categoria(1 /* Desejavel */, 'Desejável', Number.MIN_VALUE, 149),
                new ReferencialColesterol.Categoria(2 /* Limitrofe */, 'Limítrofe', 150, 199),
                new ReferencialColesterol.Categoria(3 /* Alto */, 'Alto', 200, 499),
                new ReferencialColesterol.Categoria(4 /* MuitoAlto */, 'Muito Alto', 500, Number.MAX_VALUE)
            ];
            this.hdl = [
                new ReferencialColesterol.Categoria(1 /* Desejavel */, 'Desejável', 61, Number.MAX_VALUE),
                new ReferencialColesterol.Categoria(2 /* Limitrofe */, 'Limítrofe', 40, 60),
                new ReferencialColesterol.Categoria(5 /* Baixo */, 'Baixo', Number.MIN_VALUE, 40)
            ];
            this.ldl = [
                new ReferencialColesterol.Categoria(0 /* Otimo */, 'Ótimo', Number.MIN_VALUE, 99),
                new ReferencialColesterol.Categoria(1 /* Desejavel */, 'Desejável', 100, 129),
                new ReferencialColesterol.Categoria(2 /* Limitrofe */, 'Limitrofe', 130, 159),
                new ReferencialColesterol.Categoria(3 /* Alto */, 'Alto', 160, 189),
                new ReferencialColesterol.Categoria(4 /* MuitoAlto */, 'Muito Alto', 190, Number.MAX_VALUE)
            ];
            this.noHdl = [
                new ReferencialColesterol.Categoria(0 /* Otimo */, 'Ótimo', Number.MIN_VALUE, 129),
                new ReferencialColesterol.Categoria(1 /* Desejavel */, 'Desejável', 130, 159),
                new ReferencialColesterol.Categoria(3 /* Alto */, 'Alto', 160, 189),
                new ReferencialColesterol.Categoria(4 /* MuitoAlto */, 'Muito Alto', 190, Number.MAX_VALUE)
            ];
        }
        AdultosApartir20Anos.prototype.obterColesterolTotal = function () {
            return this.colesterolTotal;
        };
        AdultosApartir20Anos.prototype.definirColesterol = function (v) {
            this.colesterolTotal = v;
        };
        AdultosApartir20Anos.prototype.obterTriglicerides = function () {
            return this.triglicerides;
        };
        AdultosApartir20Anos.prototype.definirTriglicerides = function (v) {
            this.triglicerides = v;
        };
        AdultosApartir20Anos.prototype.obterHdl = function () {
            return this.hdl;
        };
        AdultosApartir20Anos.prototype.definirHdl = function (v) {
            this.hdl = v;
        };
        AdultosApartir20Anos.prototype.obterLdl = function () {
            return this.ldl;
        };
        AdultosApartir20Anos.prototype.definirLdl = function (v) {
            this.ldl = v;
        };
        AdultosApartir20Anos.prototype.obterNoHdl = function () {
            return this.noHdl;
        };
        AdultosApartir20Anos.prototype.definirNoHdl = function (v) {
            this.noHdl = v;
        };
        return AdultosApartir20Anos;
    }());
    AdultosApartir20Anos.keyReferencial = 1;
    ReferencialColesterol.AdultosApartir20Anos = AdultosApartir20Anos;
})(ReferencialColesterol || (ReferencialColesterol = {}));
