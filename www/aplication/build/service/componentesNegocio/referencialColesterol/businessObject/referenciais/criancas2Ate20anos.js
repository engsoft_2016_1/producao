/// <reference path="../categoria.ts" />
/// <reference path="../../../referencialColesterol/enums/tipoCategoria.ts" />
/// <reference path="../../interfaces/ireferencial.ts" />
var ReferencialColesterol;
(function (ReferencialColesterol) {
    /**
     * Esta classe representa o refrencial de crianças com faixa etária
     * dos 2 a 20 anos.
     */
    var Criancas2Ate19Anos = (function () {
        function Criancas2Ate19Anos() {
            this.colesterolTotal = [
                new ReferencialColesterol.Categoria(1 /* Desejavel */, 'Desejável', Number.MIN_VALUE, 149),
                new ReferencialColesterol.Categoria(2 /* Limitrofe */, 'Limítrofe', 150, 169),
                new ReferencialColesterol.Categoria(3 /* Alto */, 'Alto', 170, Number.MAX_VALUE)
            ];
            this.triglicerides = [
                new ReferencialColesterol.Categoria(1 /* Desejavel */, 'Desejável', Number.MIN_VALUE, 99),
                new ReferencialColesterol.Categoria(2 /* Limitrofe */, 'Limítrofe', 100, 129),
                new ReferencialColesterol.Categoria(3 /* Alto */, 'Alto', 130, Number.MAX_VALUE)
            ];
            this.hdl = [
                new ReferencialColesterol.Categoria(1 /* Desejavel */, 'Desejável', 45, Number.MAX_VALUE),
                new ReferencialColesterol.Categoria(5 /* Baixo */, 'Baixo', Number.MIN_VALUE, 39)
            ];
            this.ldl = [
                new ReferencialColesterol.Categoria(1 /* Desejavel */, 'Desejável', Number.MIN_VALUE, 99),
                new ReferencialColesterol.Categoria(2 /* Limitrofe */, 'Limitrofe', 100, 129),
                new ReferencialColesterol.Categoria(3 /* Alto */, 'Alto', 130, Number.MAX_VALUE)
            ];
        }
        Criancas2Ate19Anos.prototype.obterColesterolTotal = function () {
            return this.colesterolTotal;
        };
        Criancas2Ate19Anos.prototype.definirColesterol = function (v) {
            this.colesterolTotal = v;
        };
        Criancas2Ate19Anos.prototype.obterTriglicerides = function () {
            return this.triglicerides;
        };
        Criancas2Ate19Anos.prototype.definirTriglicerides = function (v) {
            this.triglicerides = v;
        };
        Criancas2Ate19Anos.prototype.obterHdl = function () {
            return this.hdl;
        };
        Criancas2Ate19Anos.prototype.definirHdl = function (v) {
            this.hdl = v;
        };
        Criancas2Ate19Anos.prototype.obterLdl = function () {
            return this.ldl;
        };
        Criancas2Ate19Anos.prototype.definirLdl = function (v) {
            this.ldl = v;
        };
        return Criancas2Ate19Anos;
    }());
    Criancas2Ate19Anos.keyReferencial = 2;
    ReferencialColesterol.Criancas2Ate19Anos = Criancas2Ate19Anos;
})(ReferencialColesterol || (ReferencialColesterol = {}));
