/// <reference path="../enums/tipoCategoria.ts" />
var ReferencialColesterol;
(function (ReferencialColesterol) {
    /**
     * Esta classe armazena o nome de uma categoria do referencial
     * lipídico e seu intervalo.
     */
    var Categoria = (function () {
        function Categoria(tipoCategoria, nome, min, max) {
            this.setTipoCategoria(tipoCategoria);
            this.setNome(nome);
            this.setMin(min);
            this.setMax(max);
        }
        Categoria.prototype.getNome = function () {
            return this.nome;
        };
        Categoria.prototype.setNome = function (v) {
            this.nome = v;
        };
        Categoria.prototype.getMin = function () {
            return this.min;
        };
        Categoria.prototype.setMin = function (v) {
            this.min = v;
        };
        Categoria.prototype.getMax = function () {
            return this.max;
        };
        Categoria.prototype.setMax = function (v) {
            this.max = v;
        };
        Categoria.prototype.getTipoCategoria = function () {
            return this.tipoCategoria;
        };
        Categoria.prototype.setTipoCategoria = function (v) {
            this.tipoCategoria = v;
        };
        return Categoria;
    }());
    ReferencialColesterol.Categoria = Categoria;
})(ReferencialColesterol || (ReferencialColesterol = {}));
