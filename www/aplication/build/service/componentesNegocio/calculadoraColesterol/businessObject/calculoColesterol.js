/// <reference path="../interfaces/icalculadoraColesterol.ts" />
/// <reference path="../../../../factory/banco/servicesBD/referencialLipidico/businessObject/referencialLipidico.ts" />
/// <reference path="../../../../factory/banco/interfaceBD/IFacadeBD.ts" />
/// <reference path="../../dadosPaciente/businessObject/dadosPaciente.ts" />
/// <reference path="../../referencialColesterol/interfaces/ifactory.ts" />
/// <reference path="../../referencialColesterol/businessObject/referenciais/adultosAcima20anos.ts" />
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t;
    return { next: verb(0), "throw": verb(1), "return": verb(2) };
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
/**
 * @author Carlos Alberto
 * @module CalculoColesterolService
 * @version 0.0.0.1
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo manter as funções que calculam os níveis de LDL, NoHDL e
 * o percentual de redução LDL.
 * @description Data de criação : 19/03/2017
 * @description Data da última alteração : 19/03/2017
 * @file www/aplication/service/calculadoraColesterol/calculoColesterol.ts
 */
var CalculoColesterolService;
(function (CalculoColesterolService) {
    /**
    * Esta classe é responsável por calcular os valores do colesterol LDL,NoHDL e de redução do LDL.
    */
    var CalculadoraColesterolService = (function () {
        function CalculadoraColesterolService(referencialColesterol) {
            this.referencialColesterol = referencialColesterol;
        }
        CalculadoraColesterolService.prototype.calcularLDL = function (paciente) {
            if (!paciente) {
                throw new Error('CalculadoraColesterolService.calcularLDL : Parâmetro paciente é nulo.');
            }
            var ct = paciente.getColesterolTotal();
            var hdl = paciente.getHdl();
            var tr = paciente.getTriglicerides();
            var result = ct - hdl - (tr / 5);
            return Number(result.toFixed(2));
        };
        CalculadoraColesterolService.prototype.calcularNoHDL = function (paciente) {
            if (!paciente) {
                throw new Error('CalculadoraColesterol : Parâmetro paciente é undefined.');
            }
            var CT = paciente.getColesterolTotal();
            var hdl = paciente.getHdl();
            var result = CT - hdl;
            return Number(result.toFixed(2));
        };
        CalculadoraColesterolService.prototype.calcularPercentualReducaoLDL = function (paciente) {
            return __awaiter(this, void 0, void 0, function () {
                var referencial, result, percentual;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!paciente) {
                                return [2 /*return*/, Promise.reject(new Error('CalculadoraColesterolService.calcularPercentualReducaoLDL : Parâmetro paciente é undefined.'))];
                            }
                            return [4 /*yield*/, this.referencialColesterol.obterReferencial(paciente.getCodReferencial()).then(function (res) {
                                    referencial = res.obterLdl().find(function (categoria) {
                                        if (categoria.getTipoCategoria() == 1 /* Desejavel */) {
                                            return true;
                                        }
                                    });
                                }).catch(function (reason) {
                                    console.log('CalculadoraColesterolService.calcularPercentualReducaoLDL : obterReferencial : ' + reason.message);
                                    return Promise.reject(new Error('CalculadoraColesterolService.calcularPercentualReducaoLDL : obterReferencial : ' + reason.message));
                                })];
                        case 1:
                            _a.sent();
                            if (paciente.getLdl() > 0) {
                                console.log('CalculadoraColesterolService.calcularPercentualReducaoLDL : ' +
                                    ' Dados do paciente, LDL : ' + paciente.getLdl());
                                result = (paciente.getLdl() * 100) / referencial.getMax();
                                percentual = result - 100;
                                console.log('CalculadoraColesterolService.calcularPercentualReducaoLDL : ' +
                                    ' resultado : ' + result);
                                return [2 /*return*/, Promise.resolve(Number(percentual.toFixed(2)))];
                            }
                            return [2 /*return*/, Promise.reject(new Error('O Ldl deve ser maior que zero.'))];
                    }
                });
            });
        };
        CalculadoraColesterolService.prototype.calcularPercentualReducaoTG = function (paciente) {
            return __awaiter(this, void 0, void 0, function () {
                var referencial, result, percentual;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!paciente) {
                                return [2 /*return*/, Promise.reject(new Error('CalculadoraColesterol : Parâmetro paciente é nulo.'))];
                            }
                            return [4 /*yield*/, this.referencialColesterol.obterReferencial(paciente.getCodReferencial()).then(function (res) {
                                    referencial = res.obterTriglicerides().find(function (categoria, i, o) {
                                        if (categoria.getTipoCategoria() == 1 /* Desejavel */) {
                                            return true;
                                        }
                                        return false;
                                    });
                                }).catch(function (reason) {
                                    console.log(reason.message);
                                    return Promise.reject(new Error('CalculoColesterol : Erro = ' + reason.message));
                                })];
                        case 1:
                            _a.sent();
                            if (paciente.getTriglicerides() > 0) {
                                result = (paciente.getTriglicerides() * 100) / referencial.getMax();
                                percentual = result - 100;
                                console.log('CalculadoraColesterolService.calcularPercentualReducaoTG : ' +
                                    ' resultado : ' + result);
                                return [2 /*return*/, Promise.resolve(Number(percentual.toFixed(2)))];
                            }
                            return [2 /*return*/, Promise.reject(new Error('CalculoColesterol : O Triglicérides deve ser maior que zero.'))];
                    }
                });
            });
        };
        CalculadoraColesterolService.prototype.calcularPercentualAumentoHdl = function (paciente) {
            return __awaiter(this, void 0, void 0, function () {
                var referencial, result, percentual;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!paciente) {
                                return [2 /*return*/, Promise.reject(new Error('CalculadoraColesterol : Parâmetro paciente é nulo.'))];
                            }
                            return [4 /*yield*/, this.referencialColesterol.obterReferencial(paciente.getCodReferencial()).then(function (res) {
                                    referencial = res.obterHdl().find(function (categoria, i, o) {
                                        if (categoria.getTipoCategoria() == 1 /* Desejavel */) {
                                            return true;
                                        }
                                        return false;
                                    });
                                    console.log('Referencial : getHdl = ' + referencial.getMin());
                                }).catch(function (reason) {
                                    console.log(reason.message);
                                    return Promise.reject(new Error('CalculadoraColesterol : Error = ' + reason.message));
                                })];
                        case 1:
                            _a.sent();
                            if (paciente.getHdl() > 0) {
                                result = (paciente.getHdl() * 100) / referencial.getMin();
                                percentual = 100 - result;
                                console.log('CalculadoraColesterolService.calcularPercentualAumentoHdl : ' +
                                    ' resultado : ' + result);
                                return [2 /*return*/, Promise.resolve(Number(percentual.toFixed(2)))];
                            }
                            return [2 /*return*/, Promise.reject(new Error('O Hdl deve ser maior que zero.'))];
                    }
                });
            });
        };
        return CalculadoraColesterolService;
    }());
    CalculoColesterolService.CalculadoraColesterolService = CalculadoraColesterolService;
})(CalculoColesterolService || (CalculoColesterolService = {}));
