/**
 * @author Carlos Alberto
 * @module DadosPacienteService
 * @version 0.0.0.1
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo armazenar as informações que forem passadas pelo
 * médico sobre o paciente.
 * @description Data de criação : 19/03/2017
 * @description Data da última alteração : 19/03/2017
 * @file www/aplication/service/dadosPaciente/dadosPaciente.ts
 */
var DadosPacienteService;
(function (DadosPacienteService) {
    /**
    * Esta classe agrupa as informações informadas pelo médico sobre o paciente na tela principal.
    */
    var Paciente = (function () {
        /**
         * @param codReferencial - @type number - Armazena o id da faixa etária do paciente.
         * @param sexo - @type number - Armazena o id do sexo do paciente.
         * @param colesterolTotal - @type number - Armazena o valor do ColesterolTotal do paciente.
         * @param hdl - @type number - Armazena o valor do HDL do paciente.
         * @param triglicerides - @type number - Armazena o valor dos Triglicérides do paciente.
         * @param contraindicacoes - @type number[] - Armazena um array com os ids das contraindicações do paciente.
         */
        function Paciente(codReferencial, sexo, colesterolTotal, hdl, triglicerides, contraindicacoes) {
            if (codReferencial === void 0) { codReferencial = 0; }
            if (sexo === void 0) { sexo = 0; }
            if (colesterolTotal === void 0) { colesterolTotal = 0; }
            if (hdl === void 0) { hdl = 0; }
            if (triglicerides === void 0) { triglicerides = 0; }
            if (contraindicacoes === void 0) { contraindicacoes = []; }
            this.codReferencial = 0;
            this.sexo = 0;
            this.colesterolTotal = 0;
            this.hdl = 0;
            this.triglicerides = 0;
            this.ldl = 0;
            this.noHdl = 0;
            this.contraindicacoes = [];
            this.percentualReducaoLdl = 0;
            this.percentualAumentoHdl = 0;
            this.percentualReducaoTg = 0;
            this.setCodReferencial(codReferencial);
            this.setSexo(sexo);
            this.setColesterolTotal(colesterolTotal);
            this.setHdl(hdl);
            this.setTriglicerides(triglicerides);
            this.setContraindicacoes(contraindicacoes);
        }
        Paciente.prototype.setCodReferencial = function (v) {
            this.codReferencial = v;
        };
        Paciente.prototype.getCodReferencial = function () {
            return this.codReferencial;
        };
        Paciente.prototype.setColesterolTotal = function (v) {
            this.colesterolTotal = v;
        };
        Paciente.prototype.getColesterolTotal = function () {
            return this.colesterolTotal;
        };
        Paciente.prototype.setHdl = function (v) {
            this.hdl = v;
        };
        Paciente.prototype.getHdl = function () {
            return this.hdl;
        };
        Paciente.prototype.setTriglicerides = function (v) {
            this.triglicerides = v;
        };
        Paciente.prototype.getTriglicerides = function () {
            return this.triglicerides;
        };
        Paciente.prototype.setLdl = function (v) {
            this.ldl = v;
        };
        Paciente.prototype.getLdl = function () {
            return this.ldl;
        };
        Paciente.prototype.setNoHdl = function (v) {
            this.noHdl = v;
        };
        Paciente.prototype.getNoHdl = function () {
            return this.noHdl;
        };
        Paciente.prototype.setContraindicacoes = function (v) {
            this.contraindicacoes = v;
        };
        Paciente.prototype.getContraindicacoes = function () {
            return this.contraindicacoes;
        };
        Paciente.prototype.getPercentualReducaoLdl = function () {
            return this.percentualReducaoLdl;
        };
        Paciente.prototype.setPercentualReducaoLdl = function (v) {
            this.percentualReducaoLdl = v;
        };
        Paciente.prototype.getPercentualAumentoHdl = function () {
            return this.percentualAumentoHdl;
        };
        Paciente.prototype.setPercentualAumentoHdl = function (v) {
            this.percentualAumentoHdl = v;
        };
        Paciente.prototype.getPercentualReducaoTg = function () {
            return this.percentualReducaoTg;
        };
        Paciente.prototype.setPercentualReducaoTg = function (v) {
            this.percentualReducaoTg = v;
        };
        Paciente.prototype.getSexo = function () {
            return this.sexo;
        };
        Paciente.prototype.setSexo = function (v) {
            this.sexo = v;
        };
        return Paciente;
    }());
    DadosPacienteService.Paciente = Paciente;
})(DadosPacienteService || (DadosPacienteService = {}));
