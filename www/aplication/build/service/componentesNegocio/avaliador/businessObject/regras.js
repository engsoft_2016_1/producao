/// <reference path="../interfaces/iregras.ts" />
/// <reference path="../../calculadoraColesterol/businessObject/calculoColesterol.ts" />
var AvaliadorPrincipioativoService;
(function (AvaliadorPrincipioativoService) {
    /**
    * Esta classe armazena as regras que serão testadas em cada principioativo.
    */
    var Regras = (function () {
        function Regras(melhorPontuacao, pontuacaoMediana, piorPontuacao) {
            this.melhorPontuacao = melhorPontuacao;
            this.pontuacaoMediana = pontuacaoMediana;
            this.piorPontuacao = piorPontuacao;
        }
        Regras.prototype.melhorReducaoLdl = function (lista) {
            var _this = this;
            lista.forEach(function (element) {
                var a = element.getPrincipioativo().getReducaoLdl();
                var b = Number((element.getDadosPaciente().getPercentualReducaoLdl() / 100)
                    .toFixed(2));
                if (a == b) {
                    var result = _this.melhorPontuacao + (_this.melhorPontuacao - element.getPrincipioativo().getReducaoLdl());
                    element.setPontuacaoReducaoLdl(Number(result.toFixed(2)));
                }
                else if (a > b) {
                    var result = _this.pontuacaoMediana + (_this.pontuacaoMediana - element.getPrincipioativo().getReducaoLdl());
                    element.setPontuacaoReducaoLdl(Number(result.toFixed(2)));
                }
                else {
                    var result = _this.piorPontuacao + (_this.piorPontuacao + element.getPrincipioativo().getReducaoLdl());
                    element.setPontuacaoReducaoLdl(Number(result.toFixed(2)));
                }
            });
            return lista;
        };
        Regras.prototype.melhorReducaoTg = function (lista) {
            var _this = this;
            lista.forEach(function (element) {
                var a = element.getPrincipioativo().getReducaoTriglicerides();
                var b = Number((element.getDadosPaciente().getPercentualReducaoLdl() / 100)
                    .toFixed(2));
                if (a == b) {
                    var result = (_this.melhorPontuacao - 30) + ((_this.melhorPontuacao - 30) - element.getPrincipioativo().getReducaoTriglicerides());
                    element.setPontuacaoReducaoTg(Number(result.toFixed(2)));
                }
                else if (a > b) {
                    var result = (_this.pontuacaoMediana - 30) + ((_this.pontuacaoMediana - 30) - element.getPrincipioativo().getReducaoTriglicerides());
                    element.setPontuacaoReducaoTg(Number(result.toFixed(2)));
                }
                else {
                    var result = (_this.piorPontuacao - 30) + ((_this.piorPontuacao - 30) + element.getPrincipioativo().getReducaoTriglicerides());
                    element.setPontuacaoReducaoTg(Number(result.toFixed(2)));
                }
            });
            return lista;
        };
        Regras.prototype.melhorAumentoHdl = function (lista) {
            var _this = this;
            lista.forEach(function (element) {
                var a = element.getPrincipioativo().getAumentoHDL();
                var b = Number((element.getDadosPaciente().getPercentualReducaoLdl() / 100)
                    .toFixed(2));
                if (a == b) {
                    var result = (_this.melhorPontuacao - 60) + ((_this.melhorPontuacao - 60) - element.getPrincipioativo().getAumentoHDL());
                    element.setPontuacaoAumentoHdl(Number(result.toFixed(2)));
                }
                else if (a > b) {
                    var result = (_this.pontuacaoMediana - 60) + ((_this.pontuacaoMediana - 60) - element.getPrincipioativo().getAumentoHDL());
                    element.setPontuacaoAumentoHdl(Number(result.toFixed(2)));
                }
                else {
                    var result = (_this.piorPontuacao - 60) + ((_this.piorPontuacao - 60) + element.getPrincipioativo().getAumentoHDL());
                    element.setPontuacaoAumentoHdl(Number(result.toFixed(2)));
                }
            });
            return lista;
        };
        Regras.prototype.estatinasContraindicadas = function (lista) {
            lista.forEach(function (e) {
                if (e.getContraindicacoes().length > 0) {
                    e.setPontuacaoReducaoLdl(0);
                    e.setPontuacaoReducaoTg(0);
                    e.setPontuacaoAumentoHdl(0);
                }
            });
            return lista;
        };
        return Regras;
    }());
    AvaliadorPrincipioativoService.Regras = Regras;
})(AvaliadorPrincipioativoService || (AvaliadorPrincipioativoService = {}));
