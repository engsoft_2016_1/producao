/// <reference path="../../prescritorPrincipioativo/businessObject/item.ts" />
/// <reference path="../../dadosPaciente/businessObject/dadosPaciente.ts" />
/// <reference path="../interfaces/iserviceAvaliador.ts" />
/// <reference path="../interfaces/iregras.ts" />
/// <reference path="../businessObject/regras.ts" />
/**
 * @author Carlos Alberto
 * @module AvaliadorPrincipioativo
 * @version 0.0.0.1
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo de definir os pesos e atribuir notas as estatinas
 * de acordo com os dados do paciente.
 * @description Data de criação : 20/03/2017
 * @description Data da última alteração : 20/03/2017
 * @file www/aplication/service/componentesNegocio/avaliador/businessObject/avaliador.ts
 */
var AvaliadorPrincipioativo;
(function (AvaliadorPrincipioativo) {
    /**
    * Esta classe é responsável por atribuir os pesos e dar pontuações aos principiosativos
    * que mais se adequam ao problema do paciente.
    */
    var Avaliador = (function () {
        function Avaliador() {
            this.regras = new AvaliadorPrincipioativo.Regras(100, 90, 80);
        }
        Avaliador.prototype.avaliar = function (lista, paciente) {
            var list = this.regras.melhorReducaoLdl(lista, paciente);
            list = this.regras.melhorReducaoTg(list, paciente);
            list = this.regras.melhorAumentoHdl(list, paciente);
            return list;
        };
        return Avaliador;
    }());
    AvaliadorPrincipioativo.Avaliador = Avaliador;
})(AvaliadorPrincipioativo || (AvaliadorPrincipioativo = {}));
