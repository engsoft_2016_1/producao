/// <reference path="../../prescritorPrincipioativo/businessObject/item.ts" />
/// <reference path="../../dadosPaciente/businessObject/dadosPaciente.ts" />
/// <reference path="../interfaces/iserviceJudge.ts" />
/// <reference path="../interfaces/iserviceJudge.ts" />
/**
 * @author Carlos Alberto
 * @module JudgeService
 * @version 0.0.0.1
 * @since 0.0.0.1
 * @description Este módulo é o componente responsável por produzir um resultado que indique
 * qual(ais) são os melhores principios ativos para o tratamento do paciente.
 * @description Data de criação : 23/03/2017
 * @description Data da última alteração : 23/03/2017
 * @file www/aplication/service/componentesNegocio/judge/businessObject/judge.ts
 */
var JudgeService;
(function (JudgeService) {
    /**
    * Esta classe é o serviço que reune as informações do avaliador e calcula um único
    * valor que representa as melhores características para o tratamento de um
    * paciente.
    */
    var Judge = (function () {
        function Judge() {
        }
        Judge.prototype.julgar = function (lista) {
            lista.sort(function (a, b) {
                if (a.getPontuacaoReducaoLdl() != b.getPontuacaoReducaoLdl()) {
                    return b.getPontuacaoReducaoLdl() - a.getPontuacaoReducaoLdl();
                }
                if (a.getPontuacaoReducaoTg() != b.getPontuacaoReducaoTg()) {
                    return b.getPontuacaoReducaoTg() - a.getPontuacaoReducaoTg();
                }
                return b.getPontuacaoAumentoHdl() - a.getPontuacaoAumentoHdl();
            });
            return lista;
        };
        Judge.prototype.total = function (param) {
            var total = 0;
            param.forEach(function (e) {
                total += e;
            });
            return total;
        };
        return Judge;
    }());
    JudgeService.Judge = Judge;
})(JudgeService || (JudgeService = {}));
