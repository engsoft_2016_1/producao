/// <reference path="../interfaces/iinformacoesPaciente.ts" />
/// <reference path="../../referencialColesterol/businessObject/categoria.ts" />
/// <reference path="../../dadosPaciente/businessObject/dadosPaciente.ts" />
/// <reference path="../../referencialColesterol/businessObject/referencialColesterol.ts" />
/// <reference path="../../calculadoraColesterol/businessObject/calculoColesterol.ts" />
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t;
    return { next: verb(0), "throw": verb(1), "return": verb(2) };
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var ResultadoColesterol;
(function (ResultadoColesterol) {
    /**
     * Esta classe extrai informações apartir dos dados do paciente.
     */
    var InformacoesPaciente = (function () {
        function InformacoesPaciente(paciente) {
            this.paciente = paciente;
        }
        InformacoesPaciente.prototype.nivelLdl = function (referenciais) {
            return __awaiter(this, void 0, void 0, function () {
                var tipoCategoria, ldl;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            ldl = this.paciente.getLdl();
                            if (ldl == null || ldl == undefined) {
                                Promise.reject(new Error('O ldl não possui um valor válido'));
                            }
                            console.log("code referencial : " + this.paciente.getCodReferencial());
                            return [4 /*yield*/, referenciais.obterReferencial(this.paciente.getCodReferencial()).then(function (res) {
                                    res.obterLdl().find(function (element) {
                                        if (ldl >= element.getMin() && ldl <= element.getMax()) {
                                            tipoCategoria = element.getTipoCategoria();
                                            return true;
                                        }
                                    });
                                }).catch(function (err) {
                                    Promise.reject('InformacoesPaciente : ' + err.message);
                                })];
                        case 1:
                            _a.sent();
                            return [2 /*return*/, Promise.resolve(tipoCategoria)];
                    }
                });
            });
        };
        InformacoesPaciente.prototype.nivelHdl = function (referenciais) {
            return __awaiter(this, void 0, void 0, function () {
                var tipoCategoria, hdl;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            hdl = this.paciente.getHdl();
                            if (hdl == null || hdl == undefined) {
                                Promise.reject(new Error('O HDL não possui um valor válido'));
                            }
                            return [4 /*yield*/, referenciais.obterReferencial(this.paciente.getCodReferencial()).then(function (res) {
                                    res.obterHdl().find(function (element) {
                                        if (hdl >= element.getMin() && hdl <= element.getMax()) {
                                            tipoCategoria = element.getTipoCategoria();
                                            return true;
                                        }
                                    });
                                }).catch(function (err) {
                                    Promise.reject(new Error('InformacoesPaciente : ' + err.message));
                                })];
                        case 1:
                            _a.sent();
                            return [2 /*return*/, Promise.resolve(tipoCategoria)];
                    }
                });
            });
        };
        InformacoesPaciente.prototype.nivelTriglicerides = function (referenciais) {
            return __awaiter(this, void 0, void 0, function () {
                var tipoCategoria, tg;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            tg = this.paciente.getTriglicerides();
                            if (tg == null || tg == undefined) {
                                Promise.reject(new Error('O HDL não possui um valor válido'));
                            }
                            return [4 /*yield*/, referenciais.obterReferencial(this.paciente.getCodReferencial()).then(function (res) {
                                    res.obterTriglicerides().find(function (element) {
                                        if (tg >= element.getMin() && tg <= element.getMax()) {
                                            tipoCategoria = element.getTipoCategoria();
                                            return true;
                                        }
                                    });
                                }).catch(function (err) {
                                    Promise.reject(new Error('InformacoesPaciente : ' + err.message));
                                })];
                        case 1:
                            _a.sent();
                            return [2 /*return*/, Promise.resolve(tipoCategoria)];
                    }
                });
            });
        };
        InformacoesPaciente.prototype.nivelColesterolTotal = function (referenciais) {
            return __awaiter(this, void 0, void 0, function () {
                var tipoCategoria, ct;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            ct = this.paciente.getColesterolTotal();
                            if (ct == null || ct == undefined) {
                                Promise.reject(new Error('O HDL não possui um valor válido'));
                            }
                            return [4 /*yield*/, referenciais.obterReferencial(this.paciente.getCodReferencial()).then(function (res) {
                                    res.obterColesterolTotal().find(function (element) {
                                        if (ct >= element.getMin() && ct <= element.getMax()) {
                                            tipoCategoria = element.getTipoCategoria();
                                            return true;
                                        }
                                    });
                                }).catch(function (err) {
                                    Promise.reject(new Error('InformacoesPaciente : ' + err.message));
                                })];
                        case 1:
                            _a.sent();
                            return [2 /*return*/, Promise.resolve(tipoCategoria)];
                    }
                });
            });
        };
        InformacoesPaciente.prototype.reducaoLdl = function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    if (this.paciente.getPercentualReducaoLdl() > 0) {
                        return [2 /*return*/, Promise.resolve(this.paciente.getPercentualReducaoLdl())];
                    }
                    return [2 /*return*/, Promise.reject(new Error('InformacoesPaciente : O percentual de redução de LDL é menor que zero.'))];
                });
            });
        };
        InformacoesPaciente.prototype.reducaoTriglicerides = function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    if (this.paciente.getTriglicerides() > 0) {
                        return [2 /*return*/, Promise.resolve(this.paciente.getPercentualReducaoTg())];
                    }
                    return [2 /*return*/, Promise.reject(new Error('InformacoesPaciente : O percentual de redução  de triglicérides é menor que zero.'))];
                });
            });
        };
        InformacoesPaciente.prototype.aumentoHdl = function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    if (this.paciente.getPercentualAumentoHdl() > 0) {
                        return [2 /*return*/, Promise.resolve(this.paciente.getPercentualAumentoHdl())];
                    }
                    return [2 /*return*/, Promise.reject(new Error('InformacoesPaciente : O percentual de aumento  do HDL é menor que zero.'))];
                });
            });
        };
        InformacoesPaciente.prototype.informacoesComplementaresLDL = function (nivelLdl, referencialDoPaciente, referencialFactory) {
            return __awaiter(this, void 0, void 0, function () {
                var categoriaLDL;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, referencialFactory.obterReferencial(referencialDoPaciente).then(function (res) {
                                categoriaLDL = res.obterLdl().find(function (element) {
                                    if (element.getTipoCategoria() == nivelLdl) {
                                        categoriaLDL = element;
                                        return true;
                                    }
                                });
                            })];
                        case 1:
                            _a.sent();
                            console.log('InformacoesPaciente.informacoesComplementaresLDL() :categoriaLDL: = ' + categoriaLDL.getNome());
                            switch (nivelLdl) {
                                case 0 /* Otimo */: {
                                    return [2 /*return*/, Promise.resolve('Segundo a 5º diretriz Brasileira de '
                                            + 'dislipidemias e prevenção'
                                            + ' da aterosclerose: seu LDL-C está <= ' + categoriaLDL.getMax() + ', Ótimo.')];
                                }
                                case 1 /* Desejavel */: {
                                    if (ReferencialColesterol.Criancas2Ate19Anos.keyReferencial == referencialDoPaciente) {
                                        return [2 /*return*/, Promise.resolve('Segundo a 5º diretriz Brasileira de '
                                                + 'dislipidemias e prevenção'
                                                + ' da aterosclerose: seu LDL-C está'
                                                + ' <= ' + categoriaLDL.getMax() + ', Desejável.')];
                                    }
                                    return [2 /*return*/, Promise.resolve('Segundo a 5º diretriz Brasileira de '
                                            + 'dislipidemias e prevenção'
                                            + ' da aterosclerose: seu LDL-C está >= ' + categoriaLDL.getMin()
                                            + ' e <= ' + categoriaLDL.getMax() + ', Desejável.')];
                                }
                                case 2 /* Limitrofe */: {
                                    return [2 /*return*/, Promise.resolve('Atenção. Segundo a 5º diretriz Brasileira de '
                                            + 'dislipidemias e prevenção'
                                            + ' da aterosclerose: seu LDL-C está no limite, de ' + categoriaLDL.getMin()
                                            + ' à ' + categoriaLDL.getMax())];
                                }
                                case 3 /* Alto */: {
                                    if (ReferencialColesterol.Criancas2Ate19Anos.keyReferencial == referencialDoPaciente) {
                                        return [2 /*return*/, Promise.resolve('Cuidado. Segundo a 5º diretriz Brasileira de '
                                                + 'dislipidemias e prevenção'
                                                + ' da aterosclerose: o LDL-C está em nível elevado, '
                                                + ' acima de ' + categoriaLDL.getMin())];
                                    }
                                    return [2 /*return*/, Promise.resolve('Cuidado. Segundo a 5º diretriz Brasileira de '
                                            + 'dislipidemias e prevenção'
                                            + ' da aterosclerose: o LDL-C está em nível alto, de ' + categoriaLDL.getMin()
                                            + ' à ' + categoriaLDL.getMax())];
                                }
                                case 4 /* MuitoAlto */: {
                                    return [2 /*return*/, Promise.resolve('Muito Cuidado! Segundo a 5º diretriz Brasileira de '
                                            + 'dislipidemias e prevenção'
                                            + ' da aterosclerose: o LDL-C está muito alto, acima de ' + categoriaLDL.getMin())];
                                }
                                default:
                                    break;
                            }
                            return [2 /*return*/];
                    }
                });
            });
        };
        InformacoesPaciente.prototype.resultadoLdl = function (calculoColesterol) {
            var ldl = calculoColesterol.calcularLDL(this.paciente);
            if (ldl < 0) {
                return Promise.reject('Erro nos valores do colesterol.(CT),(TG),(HDL)');
            }
            this.paciente.setLdl(ldl);
            return Promise.resolve(this.paciente);
        };
        return InformacoesPaciente;
    }());
    ResultadoColesterol.InformacoesPaciente = InformacoesPaciente;
})(ResultadoColesterol || (ResultadoColesterol = {}));
