/// <reference path="../../referencialColesterol/interfaces/ifactory.ts" />
/// <reference path="../interfaces/iresultadoColesterol.ts" />
/// <reference path="../../resultadoColesterol/businessObject/informacoesColesterol.ts" />
/// <reference path="../interfaces/iinformacoesPaciente.ts" />
/// <reference path="../businessObject/informacoesPaciente.ts" />
/// <reference path="../../calculadoraColesterol/interfaces/icalculadoraColesterol.ts" />
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t;
    return { next: verb(0), "throw": verb(1), "return": verb(2) };
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
/**
 * @author Carlos Alberto
 * @module ResultadoColesterol
 * @version 0.0.0.1
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo agrupar e fornecer as informações sobre
 * o colesterol do paciente e tornar transparente os cálculos do aplicativo.
 * @description Data de criação : 28/03/2017
 * @description Data da última alteração : 28/03/2017
 * @file www/aplication/service/resultadoColesterol/resultadoColesterol.ts
 */
var ResultadoColesterol;
(function (ResultadoColesterol) {
    /**
     * Esta classe disponibiliza os dados que indicam os níveis de colesterol do paciente.
     */
    var Resultado = (function () {
        function Resultado(referencialColesterol, calculoColesterol) {
            this.referencialColesterol = referencialColesterol;
            this.calculoColesterol = calculoColesterol;
        }
        Resultado.prototype.obterResultado = function (paciente) {
            return __awaiter(this, void 0, void 0, function () {
                var informacoesPaciente, informacoes;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            console.log('Resultado.obterResultado(paciente) : Iniciado');
                            if (paciente == null || paciente == undefined) {
                                return [2 /*return*/, Promise.reject(new Error('Resultado.obterResultado(paciente) : obterResultado : Parâmetro null'))];
                            }
                            if (paciente.getLdl() <= 0) {
                                paciente.setLdl(this.calculoColesterol.calcularLDL(paciente));
                            }
                            if (!(paciente.getPercentualReducaoLdl() <= 0)) return [3 /*break*/, 2];
                            return [4 /*yield*/, this.calculoColesterol.calcularPercentualReducaoLDL(paciente).then(function (res) {
                                    paciente.setPercentualReducaoLdl(res);
                                }).catch(function (err) {
                                    return Promise.reject(new Error('Resultado.obterResultado(paciente) : calcularPercentualReducaoLDL : Ocorreu um problema no cálculo do Percentual de redução LDL.'));
                                })];
                        case 1:
                            _a.sent();
                            _a.label = 2;
                        case 2:
                            if (!(paciente.getPercentualReducaoTg() <= 0)) return [3 /*break*/, 4];
                            return [4 /*yield*/, this.calculoColesterol.calcularPercentualReducaoTG(paciente).then(function (res) {
                                    paciente.setPercentualReducaoTg(res);
                                }).catch(function (err) {
                                    return Promise.reject(new Error('Resultado.obterResultado(paciente) : setPercentualReducaoTg : Ocorreu um problema no cálculo do Percentual de redução do Triglicerídeos (TG).'));
                                })];
                        case 3:
                            _a.sent();
                            _a.label = 4;
                        case 4:
                            if (!(paciente.getPercentualAumentoHdl() <= 0)) return [3 /*break*/, 6];
                            return [4 /*yield*/, this.calculoColesterol.calcularPercentualAumentoHdl(paciente).then(function (res) {
                                    paciente.setPercentualAumentoHdl(res);
                                }).catch(function (err) {
                                    return Promise.reject(new Error('Resultado.obterResultado(paciente) : getPercentualAumentoHdl : Ocorreu um problema no cálculo do Percentual de aumento do HDL.'));
                                })];
                        case 5:
                            _a.sent();
                            _a.label = 6;
                        case 6:
                            informacoesPaciente = new ResultadoColesterol.InformacoesPaciente(paciente);
                            informacoes = new ResultadoColesterol.Informacoes();
                            return [4 /*yield*/, informacoesPaciente.nivelLdl(this.referencialColesterol).then(function (res) {
                                    informacoes.setNivelLdl(res);
                                }).catch(function (err) {
                                    Promise.reject(new Error('Resultado.obterResultado(paciente) : nivelLdl : ' + err.message));
                                })];
                        case 7:
                            _a.sent();
                            return [4 /*yield*/, informacoesPaciente.nivelHdl(this.referencialColesterol).then(function (res) {
                                    informacoes.setNivelHdl(res);
                                }).catch(function (err) {
                                    Promise.reject(new Error('Resultado.obterResultado(paciente) : nivelHdl : ' + err.message));
                                })];
                        case 8:
                            _a.sent();
                            return [4 /*yield*/, informacoesPaciente.nivelTriglicerides(this.referencialColesterol).then(function (res) {
                                    informacoes.setNivelTriglicerides(res);
                                }).catch(function (err) {
                                    Promise.reject(new Error('Resultado.obterResultado(paciente) : nivelTriglicerides : ' + err.message));
                                })];
                        case 9:
                            _a.sent();
                            return [4 /*yield*/, informacoesPaciente.nivelColesterolTotal(this.referencialColesterol).then(function (res) {
                                    informacoes.setNivelColesterolTotal(res);
                                }).catch(function (err) {
                                    Promise.reject(new Error('Resultado.obterResultado(paciente) : nivelColesterolTotal : ' + err.message));
                                })];
                        case 10:
                            _a.sent();
                            return [4 /*yield*/, informacoesPaciente.reducaoLdl().then(function (res) {
                                    informacoes.setReducaoLdl(res);
                                }).catch(function (err) {
                                    Promise.reject(new Error('Resultado.obterResultado(paciente) : reducaoLdl : ' + err.message));
                                })];
                        case 11:
                            _a.sent();
                            return [4 /*yield*/, informacoesPaciente.reducaoTriglicerides().then(function (res) {
                                    informacoes.setReducaoTriglicerides(res);
                                }).catch(function (err) {
                                    Promise.reject(new Error('Resultado.obterResultado(paciente) : reducaoTriglicerides : ' + err.message));
                                })];
                        case 12:
                            _a.sent();
                            return [4 /*yield*/, informacoesPaciente.aumentoHdl().then(function (res) {
                                    informacoes.setAumentoHdl(res);
                                }).catch(function (err) {
                                    Promise.reject(new Error('Resultado.obterResultado(paciente) : aumentoHdl() : ' + err.message));
                                })];
                        case 13:
                            _a.sent();
                            return [4 /*yield*/, informacoesPaciente.resultadoLdl(this.calculoColesterol).then(function (res) {
                                    informacoes.setDadosPaciente(res);
                                }).catch(function (err) {
                                    Promise.reject(new Error('Resultado.setDadosPaciente(calculoColesterol) : resultadoLdl : ' + err.message));
                                })];
                        case 14:
                            _a.sent();
                            return [4 /*yield*/, informacoesPaciente.informacoesComplementaresLDL(informacoes.getNivelLdl(), paciente.getCodReferencial(), this.referencialColesterol).then(function (res) {
                                    informacoes.setInformacoesComplementaresLDL(res);
                                }).catch(function (err) {
                                    console.log('Resultado.obterResultado(paciente) : informacoesComplementaresLDL : ' + err.message);
                                    Promise.reject(new Error('Resultado.obterResultado(paciente) : informacoesComplementaresLDL : ' + err.message));
                                })];
                        case 15:
                            _a.sent();
                            console.log('Resultado.obterResultado(paciente) : Finalizado');
                            return [2 /*return*/, Promise.resolve(informacoes)];
                    }
                });
            });
        };
        return Resultado;
    }());
    ResultadoColesterol.Resultado = Resultado;
})(ResultadoColesterol || (ResultadoColesterol = {}));
