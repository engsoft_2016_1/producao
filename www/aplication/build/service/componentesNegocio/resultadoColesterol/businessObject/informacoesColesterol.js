/// <reference path="../../referencialColesterol/businessObject/referencialColesterol.ts" />
/// <reference path="../../dadosPaciente/businessObject/dadosPaciente.ts" />
var ResultadoColesterol;
(function (ResultadoColesterol) {
    /**
     * Esta classe agrupa todas as informações do paciente
     * para poderem ser exibidas da melhor forma pelas UI (User Interface).
     */
    var Informacoes = (function () {
        function Informacoes(nivelLdl, nivelHdl, nivelTriglicerides, nivelColesterolTotal, reducaoLdl, reducaoTriglicerides, aumentoHdl) {
            if (nivelLdl === void 0) { nivelLdl = 0; }
            if (nivelHdl === void 0) { nivelHdl = 0; }
            if (nivelTriglicerides === void 0) { nivelTriglicerides = 0; }
            if (nivelColesterolTotal === void 0) { nivelColesterolTotal = 0; }
            if (reducaoLdl === void 0) { reducaoLdl = 0; }
            if (reducaoTriglicerides === void 0) { reducaoTriglicerides = 0; }
            if (aumentoHdl === void 0) { aumentoHdl = 0; }
            this.dadosPaciente = null;
            this.nivelLdl = nivelLdl;
            this.nivelHdl = nivelHdl;
            this.nivelTriglicerides = nivelTriglicerides;
            this.nivelColesterolTotal = nivelColesterolTotal;
            this.reducaoLdl = reducaoLdl;
            this.reducaoTriglicerides = reducaoTriglicerides;
            this.aumentoHdl = aumentoHdl;
        }
        Informacoes.prototype.getNivelLdl = function () {
            return this.nivelLdl;
        };
        Informacoes.prototype.setNivelLdl = function (v) {
            this.nivelLdl = v;
        };
        Informacoes.prototype.getNivelHdl = function () {
            return this.nivelHdl;
        };
        Informacoes.prototype.setNivelHdl = function (v) {
            this.nivelHdl = v;
        };
        Informacoes.prototype.getNivelTriglicerides = function () {
            return this.nivelTriglicerides;
        };
        Informacoes.prototype.setNivelTriglicerides = function (v) {
            this.nivelTriglicerides = v;
        };
        Informacoes.prototype.getNivelColesterolTotal = function () {
            return this.nivelColesterolTotal;
        };
        Informacoes.prototype.setNivelColesterolTotal = function (v) {
            this.nivelColesterolTotal = v;
        };
        Informacoes.prototype.getReducaoLdl = function () {
            return this.reducaoLdl;
        };
        Informacoes.prototype.setReducaoLdl = function (v) {
            this.reducaoLdl = v;
        };
        Informacoes.prototype.getReducaoTriglicerides = function () {
            return this.reducaoTriglicerides;
        };
        Informacoes.prototype.setReducaoTriglicerides = function (v) {
            this.reducaoTriglicerides = v;
        };
        Informacoes.prototype.getAumentoHdl = function () {
            return this.aumentoHdl;
        };
        Informacoes.prototype.setAumentoHdl = function (v) {
            this.aumentoHdl = v;
        };
        Informacoes.prototype.getInformacoesComplementaresLDL = function () {
            return this.informacoesComplementaresLDL;
        };
        Informacoes.prototype.setInformacoesComplementaresLDL = function (v) {
            this.informacoesComplementaresLDL = v;
        };
        Informacoes.prototype.getDadosPaciente = function () {
            return this.dadosPaciente;
        };
        Informacoes.prototype.setDadosPaciente = function (v) {
            this.dadosPaciente = v;
        };
        return Informacoes;
    }());
    ResultadoColesterol.Informacoes = Informacoes;
})(ResultadoColesterol || (ResultadoColesterol = {}));
