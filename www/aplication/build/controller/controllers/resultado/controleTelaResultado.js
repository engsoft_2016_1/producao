/// <reference path="../../../service/componentesAplicacao/params/businessObject/params.ts" />
/**
 * Controller da resultado
 */
var ResultadoController = (function () {
    function ResultadoController($scope, $ionicPopup, $state, $stateParams, params, $actionButton) {
        $scope.resultado = params.getResultado();
        $scope.melhorNivel = function (nivel) {
            var niveis = ['balanced',
                'calm',
                'energized',
                'assertive',
                'assertive',
                'assertive'];
            return niveis[nivel];
        };
        $scope.mostrarEstatinas = function () {
            if (params.getResultado().getReducaoLdl() <= 0) {
                showAlert('Não é recomendado o uso de estatinas.', 'Seu colesterol LDL-C está bom. Procure fazer'
                    + ' exercícios e se alimentar corretamente.', null);
            }
            else {
                $state.go('estatinas');
            }
        };
        var showAlert = function (title, msg, callback) {
            var alertPopup = $ionicPopup.alert({
                title: title,
                template: msg
            });
            alertPopup.then(function (res) {
                if (callback != null) {
                    callback();
                }
            });
        };
    }
    return ResultadoController;
}());
