/// <reference path="../../../service/componentesNegocio/dadosPaciente/businessObject/dadosPaciente.ts" />
/// <reference path="../../../service/componentesNegocio/calculadoraColesterol/businessObject/calculoColesterol.ts" />
/// <reference path="../../../service/componentesNegocio/prescritorPrincipioativo/interfaces/iservicePrescritor.ts" />
/// <reference path="../../../service/componentesNegocio/avaliador/interfaces/iregras.ts" />
/// <reference path="../../../service/componentesNegocio/avaliador/interfaces/iserviceAvaliador.ts" />
/// <reference path="../../../service/componentesNegocio/referencialColesterol/interfaces/ifactory.ts" />
/// <reference path="../../../service/componentesNegocio/referencialColesterol/businessObject/referencialColesterol.ts" />
/// <reference path="../../../service/componentesNegocio/resultadoColesterol/interfaces/iresultadoColesterol.ts" />
/// <reference path="../../../service/componentesNegocio/contraindicacoes/interfaces/icontraindicacoesService.ts" />
/// <reference path="../../../service/componentesAplicacao/params/businessObject/params.ts" />
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t;
    return { next: verb(0), "throw": verb(1), "return": verb(2) };
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
/**
 * Controller da tela principal
 */
var PrincipalController = (function () {
    /**
     * @param scope
     * @param ionicModal
     * @param dao  @type Storage
     */
    function PrincipalController($scope, $ionicPopup, $state, prescritor, resultadoPaciente, contraindicacoes, params, $ionicPopover) {
        var _this = this;
        $scope.referencial = '3'; //padrão para ambas as idades
        $scope.contraindicacoes = [];
        $scope.ct = { value: '' };
        $scope.tg = { value: '' };
        $scope.hdl = { value: '' };
        var paciente = new DadosPacienteService.Paciente();
        document.addEventListener('bancoPronto', function () {
            var obterContraindicacoes = function () { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, contraindicacoes.obterContraindicacoes(Number($scope.sexo), Number($scope.referencial)).then(function (res) {
                                var itens = [];
                                res.forEach(function (e) {
                                    itens.push({ selected: false, item: e });
                                });
                                $scope.contraindicacoes = itens;
                                $scope.$apply();
                                /**
                                 * Evento para ser lançado quando os dados da tela estiver
                                 *  totalmente carregada.
                                 *  @event telapronta
                                 */
                                var event = new CustomEvent("telapronta", {});
                                // Dispatch/Trigger/Fire the event
                                document.dispatchEvent(event);
                            }).catch(function (err) {
                                console.log('Contraindicações : ' + err.message);
                            })];
                        case 1:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            }); };
            /**
             * Função que inicia os watcher depois que o dispositivo estiver pronto.
             */
            var initWatchers = function () {
                $scope.$watch('choose.selected', function (newValue, oldValue) {
                    console.log('choose.selected alterado!');
                    $scope.sexo = newValue;
                    obterContraindicacoes();
                });
                $scope.$watch('faixaEtaria.selected', function (newValue, oldValue) {
                    console.log('faixaEtaria.selected alterado!');
                    if (newValue === '0') {
                        $scope.referencial = ReferencialColesterol.Criancas2Ate19Anos.keyReferencial;
                    }
                    else {
                        $scope.referencial = ReferencialColesterol.AdultosApartir20Anos.keyReferencial;
                    }
                    obterContraindicacoes();
                });
            };
            initWatchers();
            $scope.choose = {
                masculino: '1',
                feminino: '0',
                selected: '1'
            }; //padrão para ambos os sexos
            $scope.faixaEtaria = {
                maiorDe20anos: '1',
                menorDe20anos: '0',
                selected: '1'
            };
            $scope.$apply();
        });
        $scope.choose = {
            masculino: '1',
            feminino: '0',
            selected: '1'
        }; //padrão para ambos os sexos
        $scope.faixaEtaria = {
            maiorDe20anos: '1',
            menorDe20anos: '0',
            selected: '1'
        };
        console.log('choose: = ' + $scope.choose);
        console.log('choose: = ' + $scope.faixaEtaria);
        //Exemplo de uso do consultarPrincipioativo.         
        $scope.calcularColesterol = function () { return __awaiter(_this, void 0, void 0, function () {
            var ct, tg, hdl, sexo, faixaEtaria, contraindicacoes, informacoes;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ct = $scope.ct.value;
                        tg = $scope.tg.value;
                        hdl = $scope.hdl.value;
                        sexo = Number($scope.choose.selected);
                        faixaEtaria = Number($scope.referencial);
                        if (validar(ct, tg, hdl).length > 0) {
                            return [2 /*return*/];
                        }
                        paciente = new DadosPacienteService.Paciente(faixaEtaria, sexo, ct, hdl, tg, []);
                        contraindicacoes = [];
                        $scope.contraindicacoes.forEach(function (e) {
                            if (e.selected) {
                                contraindicacoes.push(e.item.getId());
                            }
                        });
                        paciente.setContraindicacoes(contraindicacoes);
                        return [4 /*yield*/, resultadoPaciente.obterResultado(paciente).then(function (res) {
                                console.log('Teste nivelLDL: =' + res.getNivelLdl());
                                console.log('Teste nivelHDL: =' + res.getNivelHdl());
                                console.log('Teste nivelTG: =' + res.getNivelTriglicerides());
                                console.log('Teste nivelCT: =' + res.getNivelColesterolTotal());
                                console.log('Teste redução Ldl: =' + res.getReducaoLdl());
                                console.log('Teste aumento Hdl: =' + res.getAumentoHdl());
                                console.log('Teste redução TG: =' + res.getReducaoTriglicerides());
                                informacoes = res;
                            }).catch(function (err) {
                                console.log('ControllerPrincipal : ' + err.message);
                            })];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, prescritor.consultarPrincipioativo(paciente).then(function (res) {
                                params.setResultado(informacoes);
                                params.setEstatinas(res);
                                $state.go('resultado');
                            }).catch((function (err) {
                                showAlert('Desculpe o incoveniente...', err.message, null);
                            }))];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); };
        /**
         * Esta função valida o valor do colesterol total.
         * @param ct
         */
        var validarCT = function (ct) {
            if (ct == undefined) {
                return new Error('Digite o valor do colesterol total (CT).');
            }
            if (ct > 99999) {
                return new Error('O valor do Colesterol total não é válido.(CT)');
            }
            return undefined;
        };
        /**
         * Valida o valor do triglicerido.
         * @param tg
         */
        var validarTG = function (tg) {
            if (tg == undefined) {
                return new Error('Digite o valor do Triglicerides (TG).');
            }
            if (tg > 400) {
                return new Error('O valor do triglicerídeos não pode ser maior que 400.');
            }
            return undefined;
        };
        /**
         * Valida o valor do HDL.
         * @param hdl
         */
        var validarHDL = function (hdl) {
            if (hdl == undefined) {
                return new Error('Digite o valor do HDL (HDL).');
            }
            if (hdl > 99999) {
                return new Error('O valor do HDL-colesterol não é válido.(HDL-c)');
            }
            return undefined;
        };
        /**
         * Reune todos os validadores dos inputs da tela.
         * @param ct
         * @param tg
         * @param hdl
         */
        var validar = function (ct, tg, hdl) {
            var a = [];
            if (validarCT(ct) != undefined) {
                a.push(validarCT(ct));
            }
            if (validarTG(tg) != undefined) {
                a.push(validarTG(tg));
            }
            if (validarHDL(hdl) != undefined) {
                a.push(validarHDL(hdl));
            }
            $scope.validar = a;
            return a;
        };
        /**Este alerte mostra um simples popup com um botão ok
        *  @param title @type string
        *  @description Recebe uma string que será o título da aplicação.
        *  @param msg   @type string
        *  @description Recebe uma string que será exibida no corpo do alert.
        *  @param callback @type function
        *  @description Recebe uma função que será executada ao clicar no botão ok.
        */
        var showAlert = function (title, msg, callback) {
            var alertPopup = $ionicPopup.alert({
                title: title,
                template: msg
            });
            alertPopup.then(function (res) {
                if (callback != null) {
                    callback();
                }
            });
        };
        // .fromTemplate() method
        $scope.message = "testando o popover asfkahlfkhsdfuwqef wiefhijhksdlh lqiuwhfklasjhf qiuehfalksjdfh qiwerhkladsjfh";
        $ionicPopover.fromTemplateUrl('aplication/view/popovers/templatePopover.html', {
            scope: $scope
        }).then(function (popover) {
            $scope.popover = popover;
        });
        $scope.openPopover = function ($event) {
            $scope.popover.show($event);
        };
        $scope.closePopover = function () {
            $scope.popover.hide();
        };
    }
    return PrincipalController;
}());
