/// <reference path="../../../service/componentesAplicacao/params/businessObject/params.ts" />
/**
 * Controller da resultado
 */
var EstatinasController = (function () {
    function EstatinasController($scope, $ionicPopup, $state, $stateParams, params, $actionButton) {
        $scope.estatinas = params.getEstatinas();
    }
    return EstatinasController;
}());
