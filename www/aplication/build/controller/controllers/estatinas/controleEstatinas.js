/// <reference path="../../../service/componentesAplicacao/params/businessObject/params.ts" />
/**
 * Controller da resultado
 */
var EstatinasController = (function () {
    function EstatinasController($scope, $ionicPopup, $state, $stateParams, params, $actionButton) {
        $scope.estatinas = params.getEstatinas();
        /* Esta função atribui um css para definir as melhores estatinas */
        $scope.melhorEstatina = function (colocacao) {
            if (colocacao > 2) {
                return '';
            }
            var colocacoes = ['h2 icon ion-trophy gold',
                'h2 icon ion-ribbon-a plate',
                'h2 icon ion-ribbon-b bronze'];
            return colocacoes[colocacao];
        };
        $scope.estatinas2 = [{
                getPrincipioativo: function () {
                    return { getNome: function () {
                            return "teste";
                        },
                        getDosagem: function () {
                            return "teste";
                        } };
                }
            }];
    }
    return EstatinasController;
}());
