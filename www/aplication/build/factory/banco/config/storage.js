/// <reference path='references.ts'/>
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t;
    return { next: verb(0), "throw": verb(1), "return": verb(2) };
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
/**
 * @author Carlos Alberto
 * @module StorageDAO
 * @version 0.0.0.1
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo construir e gerenciar as dependencias do banco de dados bem como
 * adaptá-lo para a aplicação.
 * @description Data de criação : 19/03/2017
 * @description Data da última alteração : 24/03/2017
 * @file www/aplication/factory/banco/config/storage.ts
 */
var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por acessar os dados do banco de dados.
     */
    var DAO = (function () {
        function DAO() {
            var _this = this;
            this.banco = null;
            this.check = null;
            this.versaoBanco = 16;
            document.addEventListener('deviceready', function () {
                _this.banco = window.sqlitePlugin.openDatabase({ name: 'demo.db', key: 'your-password-here', location: 'default' }, function () {
                    console.log("banco aberto.");
                    /**
                 * Evento que mostra que o banco já criou as tabelas e está pronto para
                 * ser usado.
                 * @event bancoPronto
                 */
                    var event = new CustomEvent("bancoPronto", {});
                    setTimeout(function () {
                        // Dispatch/Trigger/Fire the event
                        document.dispatchEvent(event);
                    }, 3000);
                }, function () {
                    console.log('O banco não foi aberto.');
                });
                var tables = [
                    new StorageDAO.ConfiguracoesTable(),
                    new StorageDAO.PrincipioativoTable(),
                    new StorageDAO.LinkTable(),
                    new StorageDAO.EfeitosColateraisTable(),
                    new StorageDAO.PrincipioativoContraindicacoesTable(),
                    new StorageDAO.ContraindicacaoTable(),
                    new StorageDAO.SexoTable(),
                    new StorageDAO.FaixaEtariaTable(),
                    new StorageDAO.ReferencialLipidicoTable()
                ];
                _this.check = new StorageDAO.Check();
                if (!_this.check.bancoInstalado(_this.versaoBanco)) {
                    console.log('StorageDAO : App aberto pela primeira vez!');
                    tables.forEach(function (element) {
                        element.createTable(_this.banco);
                        element.populateTable(_this.banco);
                    });
                }
                if (_this.check.novaVersaoBanco(_this.versaoBanco)) {
                    tables.forEach(function (element) {
                        element.dropTable(_this.banco);
                        element.createTable(_this.banco);
                        element.populateTable(_this.banco);
                    });
                }
            });
        } //fim do constructor			
        DAO.prototype.obterConfiguracoes = function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new StorageDAO.ConfiguracoesDAO(this.banco).obterConfiguracoes()];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        DAO.prototype.obterContraindicacoes = function (sexo, faixaEtaria) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new StorageDAO.ContraindicacaoDAO(this.banco).obterContraindicacoes(sexo, faixaEtaria)];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        DAO.prototype.obterEfeitosColaterais = function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new StorageDAO.EfeitosColateraisDAO(this.banco).obterEfeitosColaterais()];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        DAO.prototype.obterFaixasEtarias = function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new StorageDAO.FaixaEtariaDAO(this.banco).obterFaixasEtarias()];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        DAO.prototype.obterLinks = function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new StorageDAO.LinkDAO(this.banco).obterLinks()];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        DAO.prototype.obterLinksDoPrincipioativo = function (idPrincipioativo) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new StorageDAO.LinkDAO(this.banco).obterLinksDoPrincipioativo(idPrincipioativo)];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        DAO.prototype.obterPrincipiosativos = function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new StorageDAO.PrincipioativoDAO(this.banco).obterPrincipiosativos()];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        DAO.prototype.obterReferencialLipidico = function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new StorageDAO.ReferencialLipidicoDAO(this.banco).obterReferencialLipidico()];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        DAO.prototype.pesquisarPrincipiosativosPorReducao = function (percentualReducao) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new StorageDAO.PrincipioativoDAO(this.banco).pesquisarPrincipiosativosPorReducao(percentualReducao)];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        DAO.prototype.pesquisarReferencialLipidico = function (idFaixaEtaria) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new StorageDAO.ReferencialLipidicoDAO(this.banco).pesquisarReferencialLipidico(idFaixaEtaria)];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        DAO.prototype.obterContraindicacaoPrincipioativo = function (codPrincipioativo, codContraindicacaoPaciente) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, new StorageDAO.ContraindicacaoDAO(this.banco).obterContraindicacaoPrincipioativo(codPrincipioativo, codContraindicacaoPaciente)];
                        case 1: return [2 /*return*/, _a.sent()];
                    }
                });
            });
        };
        return DAO;
    }());
    StorageDAO.DAO = DAO;
})(StorageDAO || (StorageDAO = {}));
