/// <reference path="../servicesBD/configuracao/dao/configuracoesDao.ts" />
/// <reference path="../servicesBD/configuracao/businessObject/configuracao.ts" />
/// <reference path="../interfaceBD/itable.ts" />
var StorageDAO;
(function (StorageDAO) {
    /**
     * Responsável por checar se é a primeira vez que o banco é aberto.
     */
    var Check = (function () {
        function Check() {
        }
        Check.prototype.bancoInstalado = function (versao) {
            if (localStorage.getItem('bancoInstalado') == '1') {
                return true;
            }
            localStorage.setItem('bancoInstalado', '1');
            localStorage.setItem('versaoBanco', String(versao));
            return false;
        };
        Check.prototype.novaVersaoBanco = function (versao) {
            if (Number(localStorage.getItem('versaoBanco')) < versao) {
                localStorage.setItem('versaoBanco', String(versao));
                return true;
            }
            return false;
        };
        return Check;
    }());
    StorageDAO.Check = Check;
})(StorageDAO || (StorageDAO = {}));
