var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por agrupar os dados do banco para objeto de negócio Contraindicacoes.
     * @param id @type Number
     * @param nome @type string
     * @param generoAlvo  @type Number
     * @param faixaEtaria  @type Number
     */
    var Contraindicacao = (function () {
        function Contraindicacao(id, nome, generoAlvo, faixaEtaria, descricao) {
            if (id === void 0) { id = 0; }
            if (nome === void 0) { nome = ''; }
            if (generoAlvo === void 0) { generoAlvo = 0; }
            if (faixaEtaria === void 0) { faixaEtaria = 0; }
            if (descricao === void 0) { descricao = ''; }
            this.id = 0;
            this.nome = '';
            this.codGeneroAlvo = 0;
            this.codFaixaEtaria = 0;
            this.descricao = '';
            this.setId(id);
            this.setNome(nome);
            this.setGeneroAlvo(generoAlvo);
            this.setFaixaEtaria(faixaEtaria);
            this.setDescricao(descricao);
        }
        Contraindicacao.prototype.getId = function () {
            return this.id;
        };
        Contraindicacao.prototype.setId = function (v) {
            this.id = v;
        };
        Contraindicacao.prototype.getNome = function () {
            return this.nome;
        };
        Contraindicacao.prototype.setNome = function (v) {
            this.nome = v;
        };
        Contraindicacao.prototype.getGeneroAlvo = function () {
            return this.codGeneroAlvo;
        };
        Contraindicacao.prototype.setGeneroAlvo = function (v) {
            this.codGeneroAlvo = v;
        };
        Contraindicacao.prototype.getFaixaEtaria = function () {
            return this.codFaixaEtaria;
        };
        Contraindicacao.prototype.setFaixaEtaria = function (v) {
            this.codFaixaEtaria = v;
        };
        Contraindicacao.prototype.getDescricao = function () {
            return this.descricao;
        };
        Contraindicacao.prototype.setDescricao = function (v) {
            this.descricao = v;
        };
        return Contraindicacao;
    }());
    StorageDAO.Contraindicacao = Contraindicacao;
})(StorageDAO || (StorageDAO = {}));
