/// <reference path="../../../interfaceBD/itable.ts" />
/// <reference path="../../../../../service/componentesNegocio/referencialColesterol/businessObject/referencialColesterol.ts" />
var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por manter as informações da tabela de
     * Contraindicacao para inicializar o banco de dados.
     */
    var ContraindicacaoTable = (function () {
        function ContraindicacaoTable() {
        }
        ContraindicacaoTable.prototype.populateTable = function (banco) {
            banco.transaction(function (tx) {
                var campos = '(id,nome,idGeneroAlvo,idFaixaEtaria,descricao)';
                tx.executeSql('INSERT INTO Contraindicacao ' + campos + ' VALUES (?,?,?,?,?)', [1, 'Hepatopatia', 2, 3, '']);
                tx.executeSql('INSERT INTO Contraindicacao ' + campos + ' VALUES (?,?,?,?,?)', [2, 'Miopatia', 2, 3, '']);
                tx.executeSql('INSERT INTO Contraindicacao ' + campos + ' VALUES (?,?,?,?,?)', [3, 'Entre 2 e 7 anos.', 2, ReferencialColesterol.Criancas2Ate19Anos.keyReferencial, '']);
                tx.executeSql('INSERT INTO Contraindicacao ' + campos + ' VALUES (?,?,?,?,?)', [4, 'Grávida ou Lactente', 0, ReferencialColesterol.AdultosApartir20Anos.keyReferencial, '']);
                tx.executeSql('INSERT INTO Contraindicacao ' + campos + ' VALUES (?,?,?,?,?)', [5, 'Mulher em idade fértil', 0, 3, 'Que não faz o uso de anticoncepcional.']);
                tx.executeSql('INSERT INTO Contraindicacao ' + campos + ' VALUES (?,?,?,?,?)', [6, 'Problemas na tireóide.', 2, 3, '']);
                tx.executeSql('INSERT INTO Contraindicacao ' + campos + ' VALUES (?,?,?,?,?)', [7, 'Diabético', 2, 3, '']);
                tx.executeSql('INSERT INTO Contraindicacao ' + campos + ' VALUES (?,?,?,?,?)', [8, 'Problemas renais.', 2, 3, '']);
                tx.executeSql('INSERT INTO Contraindicacao ' + campos + ' VALUES (?,?,?,?,?)', [9, 'Alcoolismo', 2, 3, '']);
                tx.executeSql('INSERT INTO Contraindicacao ' + campos + ' VALUES (?,?,?,?,?)', [10, 'Entre 8 e 18 anos.', 2, ReferencialColesterol.Criancas2Ate19Anos.keyReferencial, '']);
                tx.executeSql('INSERT INTO Contraindicacao ' + campos + ' VALUES (?,?,?,?,?)', [11, 'Idoso.', 2, ReferencialColesterol.AdultosApartir20Anos.keyReferencial, 'Acima de 65 anos.']);
                tx.executeSql('INSERT INTO Contraindicacao ' + campos + ' VALUES (?,?,?,?,?)', [12, 'Polifarmácia.', 2, 3, 'Para quem já utiliza muitos medicamentos.']);
            }, function (error) {
                console.log('ContraindicacaoTable : Transaction ERROR: ' + error.message);
            }, function () {
                console.log('ContraindicacaoTable : Populated table OK');
            });
            ;
        };
        ContraindicacaoTable.prototype.dropTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql('DROP TABLE IF EXISTS Contraindicacao;');
            }, function (error) {
                console.log('ContraindicacaoTable : Delete Configuracoes ERROR: ' + error.message);
            }, function () {
                console.log('ContraindicacaoTable : Delete table Configuracoes OK');
            });
        };
        ContraindicacaoTable.prototype.createTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS Contraindicacao ( "
                    + " id INTEGER PRIMARY KEY,"
                    + " nome TEXT NOT NULL,"
                    + " idGeneroAlvo INTEGER NOT NULL,"
                    + " idFaixaEtaria INTEGER NOT NULL,"
                    + " descricao TEXT NOT NULL,"
                    + " FOREIGN KEY (idGeneroAlvo) REFERENCES Sexo(id),"
                    + " FOREIGN KEY (idFaixaEtaria) REFERENCES FaixaEtaria(id))");
            }, function (error) {
                console.log('ContraindicacaoTable : Create Contraindicacao ERROR: ' + error.message);
            }, function () {
                console.log('ContraindicacaoTable : Create table ContraindicacaO OK');
            });
        };
        return ContraindicacaoTable;
    }());
    StorageDAO.ContraindicacaoTable = ContraindicacaoTable;
})(StorageDAO || (StorageDAO = {}));
