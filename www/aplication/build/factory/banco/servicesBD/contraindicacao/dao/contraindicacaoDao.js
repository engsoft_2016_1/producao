/// <reference path="../interface/iserviceContraindicacao.ts" />
var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por disponiblilizar o acesso aos dados do banco
     * referentes as contraindicações dos principiosativos.
     */
    var ContraindicacaoDAO = (function () {
        function ContraindicacaoDAO(banco) {
            this.banco = banco;
        }
        ContraindicacaoDAO.prototype.obterContraindicacaoPrincipioativo = function (codPrincipioativo, codContraindicacaoPaciente) {
            var _this = this;
            return new Promise(function (resolve, reject) {
                console.log('estatina: ' + codPrincipioativo);
                console.log('contraindicação: ' + codContraindicacaoPaciente);
                _this.banco.transaction(function (tx) {
                    tx.executeSql('SELECT '
                        + ' c.id AS id,'
                        + ' c.nome AS nome,'
                        + ' c.idGeneroAlvo AS idGeneroAlvo,'
                        + ' c.idFaixaEtaria AS idFaixaEtaria,'
                        + ' c.descricao AS descricao'
                        + ' FROM PrincipioativoPossuiContraindicacao AS p '
                        + ' JOIN Contraindicacao AS c ON (p.contraindicacaoId = c.id)'
                        + ' WHERE  p.contraindicacaoId IN (' + codContraindicacaoPaciente.toString() + ') AND p.principioativoId = ' + codPrincipioativo + '', [], function (tx, rs) {
                        console.log('Contraindicacao : SELECT ok');
                        if (rs.rows.length > 0) {
                            console.log('pelo menos um registro foi encontrado: ' + rs.rows.length);
                            var list = [];
                            for (var i = 0; i < rs.rows.length; i++) {
                                list[i] = new StorageDAO.Contraindicacao(Number(rs.rows.item(i).id), String(rs.rows.item(i).nome), Number(rs.rows.item(i).idGeneroAlvo), Number(rs.rows.item(i).idFaixaEtaria), String(rs.rows.item(i).descricao));
                            }
                            resolve(list);
                        }
                        else {
                            reject(new Error('Não foram encontradas contraindicações.'));
                        }
                    }, function (tx, error) {
                        console.log('ContraindicacaoDAO : SELECT error: ' + error.message);
                        reject(new Error(error.message));
                    });
                }); //fim transaction
            });
        };
        ContraindicacaoDAO.prototype.obterContraindicacoes = function (sexo, faixaEtaria) {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this.banco.transaction(function (tx) {
                    tx.executeSql('SELECT'
                        + ' r.id AS id,'
                        + ' r.idSexo, '
                        + ' f.id AS idFaixaEtaria, '
                        + ' r.nome AS nome, '
                        + ' r.descricao AS descricao,'
                        + ' r.idGeneroAlvo AS idGeneroAlvo,'
                        + ' r.idFaixaEtaria AS idFaixaEtaria'
                        + ' FROM (SELECT c.id,'
                        + ' s.id AS idSexo,'
                        + ' c.nome, '
                        + ' c.idGeneroAlvo,'
                        + ' c.idFaixaEtaria, '
                        + ' c.descricao'
                        + ' FROM Contraindicacao AS c '
                        + ' JOIN Sexo AS s ON(c.idGeneroAlvo = s.id)) AS r'
                        + ' JOIN FaixaEtaria AS f ON(r.idFaixaEtaria = f.id)'
                        + ' WHERE (r.idSexo = ? OR r.idSexo = 2) AND (f.id = ? OR f.id = 3);', [sexo, faixaEtaria], function (tx, rs) {
                        console.log('Contraindicacao : SELECT ok');
                        var list = [];
                        if (rs.rows.length > 0) {
                            console.log('pelo menos um registro foi encontrado: ' + rs.rows.length);
                            for (var i = 0; i < rs.rows.length; i++) {
                                list[i] = new StorageDAO.Contraindicacao(Number(rs.rows.item(i).id), String(rs.rows.item(i).nome), Number(rs.rows.item(i).idGeneroAlvo), Number(rs.rows.item(i).idFaixaEtaria), String(rs.rows.item(i).descricao));
                            }
                            resolve(list);
                        }
                        else {
                            reject(new Error('Não foi encontradas Contraindicações.'));
                        }
                    }, function (tx, error) {
                        console.log('ContraindicacaoDAO : SELECT error: ' + error.message);
                        reject(new Error(error.message));
                    });
                });
            });
        };
        return ContraindicacaoDAO;
    }());
    StorageDAO.ContraindicacaoDAO = ContraindicacaoDAO;
})(StorageDAO || (StorageDAO = {}));
