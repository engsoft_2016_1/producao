/// <reference path="../../../interfaceBD/itable.ts" />
/// <reference path="../../../../../service/componentesNegocio/referencialColesterol/businessObject/referenciais/adultosAcima20anos.ts" />
/// <reference path="../../../../../service/componentesNegocio/referencialColesterol/businessObject/referenciais/criancas2Ate20anos.ts" />
var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por manter as informações da tabela de
     * FaixaEtaria para inicializar o banco de dados.
     */
    var FaixaEtariaTable = (function () {
        function FaixaEtariaTable() {
        }
        FaixaEtariaTable.prototype.populateTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql('INSERT INTO FaixaEtaria (id,intervalo) VALUES (?,?)', [ReferencialColesterol.Criancas2Ate19Anos.keyReferencial, '2 a 19 anos']);
                tx.executeSql('INSERT INTO FaixaEtaria (id,intervalo) VALUES (?,?)', [ReferencialColesterol.AdultosApartir20Anos.keyReferencial, '20 a 65 anos']);
                tx.executeSql('INSERT INTO FaixaEtaria (id,intervalo) VALUES (?,?)', [3, 'Todas as idades.']);
            }, function (error) {
                console.log('FaixaEtariaTable : Transaction ERROR: ' + error.message);
            }, function () {
                console.log('FaixaEtariaTable : Populated table OK');
            });
            ;
        };
        FaixaEtariaTable.prototype.dropTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql('DROP TABLE IF EXISTS FaixaEtaria;');
            }, function (error) {
                console.log('FaixaEtariaTable : Delete Configuracoes ERROR: ' + error.message);
            }, function () {
                console.log('FaixaEtariaTable : Delete table FaixaEtaria OK');
            });
        };
        FaixaEtariaTable.prototype.createTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS FaixaEtaria ( "
                    + "id INTEGER PRIMARY KEY,"
                    + "intervalo TEXT NOT NULL)");
            }, function (error) {
                console.log('FaixaEtariaTable : Create FaixaEtaria ERROR: ' + error.message);
            }, function () {
                console.log('FaixaEtariaTable : Create table FaixaEtaria OK');
            });
        };
        return FaixaEtariaTable;
    }());
    StorageDAO.FaixaEtariaTable = FaixaEtariaTable;
})(StorageDAO || (StorageDAO = {}));
