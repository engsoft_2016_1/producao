var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por agrupar os dados do banco para objeto de negócio para FaixaEtaria.
     * @param id @type number
     * @param intervalo @type string
     * @param referencialLipidico  @type number
     */
    var FaixaEtaria = (function () {
        function FaixaEtaria(id, intervalo) {
            if (id === void 0) { id = 0; }
            if (intervalo === void 0) { intervalo = ''; }
            this.setId(id);
            this.setIntervalo(intervalo);
        }
        FaixaEtaria.prototype.getId = function () {
            return this.id;
        };
        FaixaEtaria.prototype.setId = function (v) {
            this.id = v;
        };
        FaixaEtaria.prototype.getIntervalo = function () {
            return this.intervalo;
        };
        FaixaEtaria.prototype.setIntervalo = function (v) {
            this.intervalo = v;
        };
        return FaixaEtaria;
    }());
    StorageDAO.FaixaEtaria = FaixaEtaria;
})(StorageDAO || (StorageDAO = {}));
