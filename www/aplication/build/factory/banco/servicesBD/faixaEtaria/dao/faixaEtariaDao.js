/// <reference path="../interface/iserviceFaixaEtaria.ts" />
var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por disponiblilizar o acesso aos dados do banco
     * referentes as Faixas etárias dos pacientes.
     */
    var FaixaEtariaDAO = (function () {
        function FaixaEtariaDAO(banco) {
            this.banco = banco;
        }
        FaixaEtariaDAO.prototype.obterFaixasEtarias = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this.banco.transaction(function (tx) {
                    tx.executeSql('SELECT * FROM FaixaEtaria', [], function (tx, rs) {
                        console.log('FaixaEtariaDAO : SELECT ok');
                        if (rs.rows.length > 0) {
                            var list = [];
                            for (var i = 0; i < rs.rows.length; i++) {
                                list[i] = new StorageDAO.FaixaEtaria(Number(rs.rows.item(0).id), String(rs.rows.item(0).intervalo));
                            }
                            resolve(list);
                        }
                        else {
                            reject(new Error('Não foi encontradas Faixa Etária.'));
                        }
                    }, function (tx, error) {
                        console.log('FaixaEtariaDAO : SELECT error: ' + error.message);
                        reject(new Error(error.message));
                    });
                });
            });
        };
        return FaixaEtariaDAO;
    }());
    StorageDAO.FaixaEtariaDAO = FaixaEtariaDAO;
})(StorageDAO || (StorageDAO = {}));
