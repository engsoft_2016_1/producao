/// <reference path="../interface/iserviceEfeitosColaterais.ts" />
var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por disponiblilizar o acesso aos dados do banco
     * referentes aos efeitos colaterais da aplicação.
     */
    var EfeitosColateraisDAO = (function () {
        function EfeitosColateraisDAO(banco) {
            this.banco = banco;
        }
        EfeitosColateraisDAO.prototype.obterEfeitosColaterais = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this.banco.transaction(function (tx) {
                    tx.executeSql('SELECT * FROM EfeitoColateral', [], function (tx, rs) {
                        console.log('EfeitosColateraisDAO : SELECT ok');
                        if (rs.rows.length > 0) {
                            resolve(new StorageDAO.EfeitoColateral(Number(rs.rows.item(0).id), String(rs.rows.item(0).nome), Number(rs.rows.item(0).idPrincipioAtivo)));
                        }
                        else {
                            reject(new Error('Não foi encontrados Efeitos Colaterais.'));
                        }
                    }, function (tx, error) {
                        console.log('EfeitosColateraisDAO : SELECT error: ' + error.message);
                        reject(error);
                    });
                });
            });
        };
        return EfeitosColateraisDAO;
    }());
    StorageDAO.EfeitosColateraisDAO = EfeitosColateraisDAO;
})(StorageDAO || (StorageDAO = {}));
