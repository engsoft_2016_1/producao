var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por agrupar os dados do banco para objeto de negócio EfeitoColateral.
     * @param id @type Number
     * @param nome @type String
     * @param idPrincipioativo  @type Number
     */
    var EfeitoColateral = (function () {
        function EfeitoColateral(id, nome, idPrincipioativo) {
            if (id === void 0) { id = 0; }
            if (nome === void 0) { nome = ''; }
            if (idPrincipioativo === void 0) { idPrincipioativo = 0; }
            this.setId(id);
            this.setNome(nome);
            this.setIdPrincipioativo(idPrincipioativo);
        }
        EfeitoColateral.prototype.getId = function () {
            return this.id;
        };
        EfeitoColateral.prototype.setId = function (v) {
            this.id = v;
        };
        EfeitoColateral.prototype.getIdPrincipioativo = function () {
            return this.idPrincipioativo;
        };
        EfeitoColateral.prototype.setIdPrincipioativo = function (v) {
            this.idPrincipioativo = v;
        };
        EfeitoColateral.prototype.getNome = function () {
            return this.nome;
        };
        EfeitoColateral.prototype.setNome = function (v) {
            this.nome = v;
        };
        return EfeitoColateral;
    }());
    StorageDAO.EfeitoColateral = EfeitoColateral;
})(StorageDAO || (StorageDAO = {}));
