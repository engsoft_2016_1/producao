/// <reference path="../../../interfaceBD/itable.ts" />
var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por manter as informações da tabela de
     * EfeitosColaterais para inicializar o banco de dados.
     */
    var EfeitosColateraisTable = (function () {
        function EfeitosColateraisTable() {
        }
        EfeitosColateraisTable.prototype.populateTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql('INSERT INTO EfeitoColateral VALUES (?,?,?)', [0, '', 0]);
            }, function (error) {
                console.log('EfeitoColateral : Transaction ERROR: ' + error.message);
            }, function () {
                console.log('EfeitoColateral : Populated table OK');
            });
            ;
        };
        EfeitosColateraisTable.prototype.dropTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql('DROP TABLE IF EXISTS EfeitoColateral;');
            }, function (error) {
                console.log('EfeitoColateral : Delete Configuracoes ERROR: ' + error.message);
            }, function () {
                console.log('EfeitoColateral : Delete table Configuracoes OK');
            });
        };
        EfeitosColateraisTable.prototype.createTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS EfeitoColateral ( "
                    + " id INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + " nome TEXT NOT NULL,"
                    + " idPrincipioAtivo INTEGER NOT NULL,"
                    + " FOREIGN KEY (idPrincipioAtivo) REFERENCES Principioativo(id))");
            }, function (error) {
                console.log('EfeitoColateral : Create EfeitoColateral ERROR: ' + error.message);
            }, function () {
                console.log('EfeitoColateral : Create table EfeitoColateral OK');
            });
        };
        return EfeitosColateraisTable;
    }());
    StorageDAO.EfeitosColateraisTable = EfeitosColateraisTable;
})(StorageDAO || (StorageDAO = {}));
