/// <reference path="../../../interfaceBD/itable.ts" />
var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por manter as informações da tabela de
     * Contraindicacao para inicializar o banco de dados.
     */
    var SexoTable = (function () {
        function SexoTable() {
        }
        SexoTable.prototype.populateTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql('INSERT INTO Sexo VALUES (?,?)', [0, 'Feminino']);
                tx.executeSql('INSERT INTO Sexo VALUES (?,?)', [1, 'Masculino']);
                tx.executeSql('INSERT INTO Sexo VALUES (?,?)', [2, 'Ambos']);
            }, function (error) {
                console.log('SexoTable : Transaction ERROR: ' + error.message);
            }, function () {
                console.log('SexoTable : Populated table OK');
            });
            ;
        };
        SexoTable.prototype.dropTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql('DROP TABLE IF EXISTS Sexo;');
            }, function (error) {
                console.log('SexoTable : Delete Configuracoes ERROR: ' + error.message);
            }, function () {
                console.log('SexoTable : Delete table Configuracoes OK');
            });
        };
        SexoTable.prototype.createTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS Sexo ( "
                    + "id INTEGER PRIMARY KEY,"
                    + "nome TEXT NOT NULL)");
            }, function (error) {
                console.log('SexoTable : Create Sexo ERROR: ' + error.message);
            }, function () {
                console.log('SexoTable : Create table Sexo OK');
            });
        };
        return SexoTable;
    }());
    StorageDAO.SexoTable = SexoTable;
})(StorageDAO || (StorageDAO = {}));
