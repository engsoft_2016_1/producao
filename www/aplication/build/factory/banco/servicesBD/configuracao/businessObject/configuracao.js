var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por agrupar os dados do banco para objeto de negócio Configuracoes.
     * @param versaoSoftware @type Number
     * @param versaoBanco @type Number
     * @param exibirAutoajuda  @type Number
     */
    var Configuracoes = (function () {
        function Configuracoes(versaoSoftware, versaoBanco, exibirAutoajuda) {
            if (versaoSoftware === void 0) { versaoSoftware = 0; }
            if (versaoBanco === void 0) { versaoBanco = 0; }
            if (exibirAutoajuda === void 0) { exibirAutoajuda = 0; }
            this.setVersaoBanco(versaoBanco);
            this.setVersaoSoftware(versaoSoftware);
            this.setExibirAutoajuda(exibirAutoajuda);
        }
        Configuracoes.prototype.getVersaoSoftware = function () {
            return this.versaoSoftware;
        };
        Configuracoes.prototype.setVersaoSoftware = function (v) {
            this.versaoSoftware = v;
        };
        Configuracoes.prototype.getVersaoBanco = function () {
            return this.versaoBanco;
        };
        Configuracoes.prototype.setVersaoBanco = function (v) {
            this.versaoBanco = v;
        };
        Configuracoes.prototype.getExibirAutoajuda = function () {
            return this.exibirAutoajuda;
        };
        Configuracoes.prototype.setExibirAutoajuda = function (v) {
            this.exibirAutoajuda = v;
        };
        return Configuracoes;
    }());
    StorageDAO.Configuracoes = Configuracoes;
})(StorageDAO || (StorageDAO = {}));
