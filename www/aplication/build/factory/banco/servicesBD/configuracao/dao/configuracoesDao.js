/// <reference path="../interface/iserviceConfiguracoes.ts" />
var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por disponiblilizar o acesso aos dados do banco
     * referentes as configurações do aplicativo.
     */
    var ConfiguracoesDAO = (function () {
        function ConfiguracoesDAO(banco) {
            this.banco = null;
            this.banco = banco;
        }
        ConfiguracoesDAO.prototype.obterConfiguracoes = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this.banco.transaction(function (tx) {
                    tx.executeSql('SELECT * FROM Configuracoes', [], function (tx, rs) {
                        console.log('ConfiguracoesDAO : SELECT ok');
                        if (rs.rows.length > 0) {
                            resolve(new StorageDAO.Configuracoes(Number(rs.rows.item(0).versaoSoftware), Number(rs.rows.item(0).versaoBanco), Number(rs.rows.item(0).exibirAutoajuda)));
                        }
                        else {
                            reject(new Error('Não foi encontradas configurações.'));
                        }
                    }, function (tx, error) {
                        console.log('ConfiguracoesDAO : SELECT error: ' + error.message);
                        reject(error);
                    });
                });
            });
        };
        return ConfiguracoesDAO;
    }());
    StorageDAO.ConfiguracoesDAO = ConfiguracoesDAO;
})(StorageDAO || (StorageDAO = {}));
