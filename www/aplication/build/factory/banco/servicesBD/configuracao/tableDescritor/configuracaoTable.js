/// <reference path="../../../interfaceBD/itable.ts" />
var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por manter as informações da tabela de
     * Configuracoes para inicializar o banco de dados.
     */
    var ConfiguracoesTable = (function () {
        function ConfiguracoesTable() {
        }
        ConfiguracoesTable.prototype.populateTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql('INSERT INTO Configuracoes VALUES (?,?,?)', [1, 1, 1]);
            }, function (error) {
                console.log('ConfiguracoesTable : Transaction ERROR: ' + error.message);
            }, function () {
                console.log('ConfiguracoesTable : Populated table OK');
            });
            ;
        };
        ConfiguracoesTable.prototype.dropTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql('DROP TABLE IF EXISTS Configuracoes;');
            }, function (error) {
                console.log('ConfiguracoesTable : Delete Configuracoes ERROR: ' + error.message);
            }, function () {
                console.log('ConfiguracoesTable : Delete table Configuracoes OK');
            });
        };
        ConfiguracoesTable.prototype.createTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS Configuracoes ( "
                    + "versaoSoftware INTEGER NOT NULL,"
                    + "versaoBanco INTEGER NOT NULL,"
                    + "exibirAutoajuda INTEGER NOT NULL)");
            }, function (error) {
                console.log('ConfiguracoesTable : Create Configuracoes ERROR: ' + error.message);
            }, function () {
                console.log('ConfiguracoesTable : Create database Configuracoes OK');
            });
        };
        return ConfiguracoesTable;
    }());
    StorageDAO.ConfiguracoesTable = ConfiguracoesTable;
})(StorageDAO || (StorageDAO = {}));
