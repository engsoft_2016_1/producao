/// <reference path="../interface/iservicePrincipioativo.ts" />
var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por disponiblilizar o acesso aos dados do banco
     * referentes as configurações do aplicativo.
     */
    var PrincipioativoDAO = (function () {
        function PrincipioativoDAO(banco) {
            this.banco = banco;
        }
        ///**lembre-se que esta operação não retorna os principiosativos que estão acima do indice de redução.*/
        PrincipioativoDAO.prototype.pesquisarPrincipiosativosPorReducao = function (percentualReducao) {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this.banco.transaction(function (tx) {
                    tx.executeSql('SELECT * FROM Principioativo AS p'
                        + ' WHERE p.reducaoLdl <= ? '
                        + ' ORDER BY p.reducaoLdl DESC;', [percentualReducao], function (tx, rs) {
                        console.log('Principioativo : SELECT ok');
                        var list = [];
                        if (rs.rows.length > 0) {
                            console.log('pelo menos um registro foi encontrado: ' + rs.rows.length);
                            for (var i = 0; i < rs.rows.length; i++) {
                                list[i] = new StorageDAO.Principioativo(Number(rs.rows.item(i).id), String(rs.rows.item(i).nome), String(rs.rows.item(i).dosagem), Number(rs.rows.item(i).reducaoLdl), Number(rs.rows.item(i).reducaoTriglicerides), Number(rs.rows.item(i).aumentoHDL), String(rs.rows.item(i).meiaVidaPlasmatica), Number(rs.rows.item(i).penetracaoSistemaNervoso), Number(rs.rows.item(i).excrecaoRenalDoseAbsorvida), Number(rs.rows.item(i).custoEstimado), String(rs.rows.item(i).bula));
                            }
                            resolve(list);
                        }
                        else {
                            reject(new Error('Não foi encontrados Principiosativos.'));
                        }
                    }, function (tx, error) {
                        console.log('Principioativo : SELECT error: ' + error.message);
                        reject(error);
                    });
                });
            });
        };
        PrincipioativoDAO.prototype.obterPrincipiosativos = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this.banco.transaction(function (tx) {
                    tx.executeSql('SELECT * FROM Principioativo AS p ORDER BY p.reducaoLdl DESC', [], function (tx, rs) {
                        console.log('Principioativo : SELECT ok');
                        var list = [];
                        if (rs.rows.length > 0) {
                            console.log('pelo menos um registro foi encontrado: ' + rs.rows.length);
                            for (var i = 0; i < rs.rows.length; i++) {
                                list[i] = new StorageDAO.Principioativo(Number(rs.rows.item(i).id), String(rs.rows.item(i).nome), String(rs.rows.item(i).dosagem), Number(rs.rows.item(i).reducaoLdl), Number(rs.rows.item(i).reducaoTriglicerides), Number(rs.rows.item(i).aumentoHDL), String(rs.rows.item(i).meiaVidaPlasmatica), Number(rs.rows.item(i).penetracaoSistemaNervoso), Number(rs.rows.item(i).excrecaoRenalDoseAbsorvida), Number(rs.rows.item(i).custoEstimado), String(rs.rows.item(i).bula));
                            }
                            resolve(list);
                        }
                        else {
                            reject(new Error('Não foi encontrados Principiosativos.'));
                        }
                    }, function (tx, error) {
                        console.log('Principioativo : SELECT error: ' + error.message);
                        reject(error);
                    });
                });
            });
        };
        return PrincipioativoDAO;
    }());
    StorageDAO.PrincipioativoDAO = PrincipioativoDAO;
})(StorageDAO || (StorageDAO = {}));
