/// <reference path="../../../interfaceBD/itable.ts" />
var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por manter as informações da tabela de
     * Principioativo para inicializar o banco de dados.
     */
    var PrincipioativoTable = (function () {
        function PrincipioativoTable() {
        }
        PrincipioativoTable.prototype.populateTable = function (banco) {
            banco.transaction(function (tx) {
                var campos = ' (id,nome,dosagem,reducaoLdl,reducaoTriglicerides,aumentoHDL,meiaVidaPlasmatica,penetracaoSistemaNervoso,excrecaoRenalDoseAbsorvida,custoEstimado,bula) ';
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [1, 'Atorva', '10mg', 0.38, 0.20, 0.06, '14 h', 0, 0.02, 0, '{nome:Atorvastatina}']); //1
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [2, 'Atorva', '20mg', 0.42, 0.25, 0.09, '14 h', 0, 0.02, 0, '{nome:Atorvastatina}']); //2
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [3, 'Atorva', '40mg', 0.50, 0.29, 0.06, '14 h', 0, 0.02, 0, '{nome:Atorvastatina}']); //3
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [4, 'Atorva', '80mg', 0.60, 0.34, 0.05, '14 h', 0, 0.02, 0, '{nome:Atorvastatina}']); //4
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [5, 'Fluva', '20mg', 0.21, 0.07, 0.02, '1 a 2h', 0, 0.05, 0, '{nome:Fluvastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [6, 'Fluva', '40mg', 0.25, 0.10, 0.08, '1 a 2h', 0, 0.05, 0, '{nome:Fluvastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [7, 'Fluva', '80mg', 0.38, 0.14, 0.09, '1 a 2h', 0, 0.05, 0, '{nome:Fluvastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [8, 'Pitava', '1mg', 0.32, 0.15, 0.08, '12h', 0, 0.15, 0, '{nome:Pitavastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [9, 'Pitava', '2mg', 0.36, 0.19, 0.07, '12h', 0, 0.15, 0, '{nome:Pitavastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [10, 'Pitava', '4mg', 0.43, 0.18, 0.05, '12h', 0, 0.15, 0, '{nome:Pitavastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [11, 'Lova', '10mg', 0.15, 0.03, 0.02, '2 h', 1, 0.10, 0, '{nome:Lovastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [12, 'Lova', '20mg', 0.24, 0.10, 0.07, '2 h', 1, 0.10, 0, '{nome:Lovastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [13, 'Lova', '40mg', 0.34, 0.16, 0.09, '2 h', 1, 0.10, 0, '{nome:Lovastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [14, 'Lova', '80mg', 0.40, 0.19, 0.10, '2 h', 1, 0.10, 0, '{nome:Lovastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [15, 'Prava', '10mg', 0.19, 0.09, 0.12, '1 a 2 h', 0, 0.20, 0, '{nome:Pravastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [16, 'Prava', '20mg', 0.25, 0.13, 0.15, '1 a 2 h', 0, 0.20, 0, '{nome:Pravastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [17, 'Prava', '40mg', 0.27, 0.15, 0.16, '1 a 2 h', 0, 0.20, 0, '{nome:Pravastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [18, 'Prava', '80mg', 0.41, 0.20, 0.12, '1 a 2 h', 0, 0.20, 0, '{nome:Pravastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [19, 'Rosu', '5mg', 0.41, 0.14, 0.06, '19 h', 0, 0.10, 0, '{nome:Rosuvastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [20, 'Rosu', '10mg', 0.47, 0.20, 0.07, '19 h', 0, 0.10, 0, '{nome:Rosuvastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [21, 'Rosu', '20mg', 0.55, 0.26, 0.08, '19 h', 0, 0.10, 0, '{nome:Rosuvastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [22, 'Rosu', '40mg', 0.63, 0.28, 0.08, '19 h', 0, 0.10, 0, '{nome:Rosuvastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [23, 'Ezet+Sinva', '10/10 mg', 0.46, 0.21, 0.09, '12h', 0, 0.11, 0, '{nome:Ezet+Sinv}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [24, 'Ezet+Sinva', '10/20 mg', 0.51, 0.31, 0.08, '12h', 0, 0.11, 0, '{nome:Ezet+Sinv}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [25, 'Ezet+Sinva', '10/40 mg', 0.55, 0.32, 0.09, '12h', 0, 0.11, 0, '{nome:Ezet+Sinv}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [26, 'Ezet+Sinva', '10/80 mg', 0.61, 0.28, 0.06, '12h', 0, 0.11, 0, '{nome:Ezet+Sinv}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [27, 'Sinva', '10 mg', 0.29, 0.04, 0.05, '1 a 2 h', 1, 0.13, 0, '{nome:Sinvastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [28, 'Sinva', '20 mg', 0.36, 0.11, 0.05, '1 a 2 h', 1, 0.13, 0, '{nome:Sinvastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [29, 'Sinva', '40 mg', 0.41, 0.19, 0.12, '1 a 2 h', 1, 0.13, 0, '{nome:Sinvastatina}']);
                tx.executeSql('INSERT INTO Principioativo ' + campos + ' VALUES (?,?,?,?,?,?,?,?,?,?,?)', [30, 'Sinva', '80 mg', 0.47, 0.26, 0.13, '1 a 2 h', 1, 0.13, 0, '{nome:Sinvastatina}']);
            }, function (error) {
                console.log('PrincipioativoTable : Transaction ERROR: ' + error.message);
            }, function () {
                console.log('PrincipioativoTable : Populated table OK');
            });
            ;
        };
        PrincipioativoTable.prototype.dropTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql('DROP TABLE IF EXISTS Principioativo;');
            }, function (error) {
                console.log('PrincipioativoTable : Delete Principioativo ERROR: ' + error.message);
            }, function () {
                console.log('PrincipioativoTable : Delete table Principioativo OK');
            });
        };
        PrincipioativoTable.prototype.createTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS Principioativo ( "
                    + " id INTEGER PRIMARY KEY,"
                    + " nome TEXT NOT NULL,"
                    + " dosagem TEXT NOT NULL,"
                    + " reducaoLdl REAL NOT NULL,"
                    + " reducaoTriglicerides REAL NOT NULL,"
                    + " aumentoHDL REAL NOT NULL,"
                    + " meiaVidaPlasmatica TEXT NOT NULL,"
                    + " penetracaoSistemaNervoso INTEGER NOT NULL,"
                    + " excrecaoRenalDoseAbsorvida REAL NOT NULL,"
                    + " custoEstimado INTEGER NOT NULL,"
                    + " bula TEXT NOT NULL)");
            }, function (error) {
                console.log('PrincipioativoTable : Create Principioativo ERROR: ' + error.message);
            }, function () {
                console.log('PrincipioativoTable : Create table Principioativo OK');
            });
        };
        return PrincipioativoTable;
    }());
    StorageDAO.PrincipioativoTable = PrincipioativoTable;
})(StorageDAO || (StorageDAO = {}));
