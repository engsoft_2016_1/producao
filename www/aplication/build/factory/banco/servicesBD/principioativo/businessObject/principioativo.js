var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por agrupar os dados do banco para objeto de negócio Principioativo.
     * @param id @type number
     * @param nome @type String
     * @param dosagem @type String
     * @param reducaoLdl @type number
     * @param reducaoTriglicerides @type number
     * @param aumentoHDL  @type number
     * @param meiaVidaPlasmatica @type String
     * @param penetracaoSistemaNervoso  @type number
     * @param excrecaoRenalDoseAbsorvida @type number
     * @param bula @type String
     */
    var Principioativo = (function () {
        function Principioativo(id, nome, dosagem, reducaoLdl, reducaoTriglicerides, aumentoHDL, meiaVidaPlasmatica, penetracaoSistemaNervoso, excrecaoRenalDoseAbsorvida, custoEstimado, bula) {
            if (id === void 0) { id = 0; }
            if (nome === void 0) { nome = ''; }
            if (dosagem === void 0) { dosagem = ''; }
            if (reducaoLdl === void 0) { reducaoLdl = 0; }
            if (reducaoTriglicerides === void 0) { reducaoTriglicerides = 0; }
            if (aumentoHDL === void 0) { aumentoHDL = 0; }
            if (meiaVidaPlasmatica === void 0) { meiaVidaPlasmatica = ''; }
            if (penetracaoSistemaNervoso === void 0) { penetracaoSistemaNervoso = 0; }
            if (excrecaoRenalDoseAbsorvida === void 0) { excrecaoRenalDoseAbsorvida = 0; }
            if (custoEstimado === void 0) { custoEstimado = 0; }
            if (bula === void 0) { bula = ''; }
            this.id = 0;
            this.nome = '';
            this.dosagem = '';
            this.reducaoLdl = 0;
            this.reducaoTriglicerides = 0;
            this.aumentoHDL = 0;
            this.meiaVidaPlasmatica = '';
            this.penetracaoSistemaNervoso = 0;
            this.excrecaoRenalDoseAbsorvida = 0;
            this.custoEstimado = 0;
            this.bula = '';
            this.setId(id);
            this.setNome(nome);
            this.setDosagem(dosagem);
            this.setReducaoLdl(reducaoLdl);
            this.setReducaoTriglicerides(reducaoTriglicerides);
            this.setAumentoHDL(aumentoHDL);
            this.setMeiaVidaPlasmatica(meiaVidaPlasmatica);
            this.setPenetracaoSistemaNervoso(penetracaoSistemaNervoso);
            this.setExcrecaoRenalDoseAbsorvida(excrecaoRenalDoseAbsorvida);
            this.setBula(bula);
            this.setCustoEstimado(custoEstimado);
        }
        Principioativo.prototype.getId = function () {
            return this.id;
        };
        Principioativo.prototype.setId = function (v) {
            this.id = v;
        };
        Principioativo.prototype.getNome = function () {
            return this.nome;
        };
        Principioativo.prototype.setNome = function (v) {
            this.nome = v;
        };
        Principioativo.prototype.getDosagem = function () {
            return this.dosagem;
        };
        Principioativo.prototype.setDosagem = function (v) {
            this.dosagem = v;
        };
        Principioativo.prototype.getReducaoLdl = function () {
            return this.reducaoLdl;
        };
        Principioativo.prototype.setReducaoLdl = function (v) {
            this.reducaoLdl = v;
        };
        Principioativo.prototype.getReducaoTriglicerides = function () {
            return this.reducaoTriglicerides;
        };
        Principioativo.prototype.setReducaoTriglicerides = function (v) {
            this.reducaoTriglicerides = v;
        };
        Principioativo.prototype.getAumentoHDL = function () {
            return this.aumentoHDL;
        };
        Principioativo.prototype.setAumentoHDL = function (v) {
            this.aumentoHDL = v;
        };
        Principioativo.prototype.getMeiaVidaPlasmatica = function () {
            return this.meiaVidaPlasmatica;
        };
        Principioativo.prototype.setMeiaVidaPlasmatica = function (v) {
            this.meiaVidaPlasmatica = v;
        };
        Principioativo.prototype.getPenetracaoSistemaNervoso = function () {
            return this.penetracaoSistemaNervoso;
        };
        Principioativo.prototype.setPenetracaoSistemaNervoso = function (v) {
            this.penetracaoSistemaNervoso = v;
        };
        Principioativo.prototype.getExcrecaoRenalDoseAbsorvida = function () {
            return this.excrecaoRenalDoseAbsorvida;
        };
        Principioativo.prototype.setExcrecaoRenalDoseAbsorvida = function (v) {
            this.excrecaoRenalDoseAbsorvida = v;
        };
        Principioativo.prototype.getCustoEstimado = function () {
            return this.custoEstimado;
        };
        Principioativo.prototype.setCustoEstimado = function (v) {
            this.custoEstimado = v;
        };
        Principioativo.prototype.getBula = function () {
            return this.bula;
        };
        Principioativo.prototype.setBula = function (v) {
            this.bula = v;
        };
        return Principioativo;
    }());
    StorageDAO.Principioativo = Principioativo;
})(StorageDAO || (StorageDAO = {}));
