/// <reference path="../../../interfaceBD/itable.ts" />
var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por manter as informações da tabela de
     * Link para inicializar o banco de dados.
     */
    var LinkTable = (function () {
        function LinkTable() {
        }
        LinkTable.prototype.populateTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql('INSERT INTO Link VALUES (?,?,?,?)', [0, 0, '', '']);
            }, function (error) {
                console.log('LinkTable : population ERROR: ' + error.message);
            }, function () {
                console.log('LinkTable : Populated database OK');
            });
        };
        LinkTable.prototype.dropTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql('DROP TABLE IF EXISTS Link;');
            }, function (error) {
                console.log('LinkTable : Delete Link ERROR: ' + error.message);
            }, function () {
                console.log('LinkTable : Delete table Link OK');
            });
        };
        LinkTable.prototype.createTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS Link ( "
                    + "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + "idPrincipioativo INTEGER NOT NULL,"
                    + "nome TEXT NOT NULL,"
                    + "enderecoHttp TEXT NOT NULL,"
                    + "FOREIGN KEY (idPrincipioativo) REFERENCES Link(id))");
            }, function (error) {
                console.log('LinkTable : Create Link ERROR: ' + error.message);
            }, function () {
                console.log('LinkTable : Create table Link OK');
            });
        };
        return LinkTable;
    }());
    StorageDAO.LinkTable = LinkTable;
})(StorageDAO || (StorageDAO = {}));
