/// <reference path="../interface/iserviceLink.ts" />
var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por disponiblilizar o acesso aos dados do banco
     * referentes as configurações do aplicativo.
     */
    var LinkDAO = (function () {
        function LinkDAO(banco) {
            this.banco = banco;
        }
        LinkDAO.prototype.obterLinksDoPrincipioativo = function (idPrincipioativo) {
            var _this = this;
            return new Promise(function (resolve, reject) {
                var links = [];
                _this.banco.transaction(function (tx) {
                    tx.executeSql('SELECT * FROM Link'
                        + ' WHERE Link.idPrincipioativo = ?;', [idPrincipioativo], function (tx, rs) {
                        console.log('LinkDAO : SELECT ok');
                        if (rs.rows.length > 0) {
                            for (var i = 0; i < rs.rows.length; i++) {
                                links[i] = new StorageDAO.Link(Number(rs.rows.item(i).id), Number(rs.rows.item(i).idPrincipioativo), String(rs.rows.item(i).nome), String(rs.rows.item(i).enderecoHttp));
                            }
                            resolve(links);
                        }
                        else {
                            reject(new Error('Não foi encontrados Links.'));
                        }
                    }, function (tx, error) {
                        console.log('LinkDAO : SELECT error: ' + error.message);
                        reject(error);
                    });
                });
            });
        };
        LinkDAO.prototype.obterLinks = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this.banco.transaction(function (tx) {
                    tx.executeSql('SELECT * FROM Link', [], function (tx, rs) {
                        console.log('LinkDAO : SELECT ok');
                        for (var i = 0; i < rs.rows.length; i++) {
                            resolve(new StorageDAO.Link(Number(rs.rows.item(i).id), Number(rs.rows.item(i).idPrincipioativo), String(rs.rows.item(i).nome), String(rs.rows.item(i).enderecoHttp)));
                        }
                    }, function (tx, error) {
                        console.log('LinkDAO : SELECT error: ' + error.message);
                        reject(error);
                    });
                });
            });
        };
        return LinkDAO;
    }());
    StorageDAO.LinkDAO = LinkDAO;
})(StorageDAO || (StorageDAO = {}));
