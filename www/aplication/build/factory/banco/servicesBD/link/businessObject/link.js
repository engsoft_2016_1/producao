var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por agrupar os dados do banco para objeto de negócio Link.
     * @param id @type Number
     * @param idPrincipioativo @type Number
     * @param nome @type String
     * @param enderecoHttp @type String
     */
    var Link = (function () {
        function Link(id, idPrincipioativo, nome, enderecoHttp) {
            if (id === void 0) { id = 0; }
            if (idPrincipioativo === void 0) { idPrincipioativo = 0; }
            if (nome === void 0) { nome = ''; }
            if (enderecoHttp === void 0) { enderecoHttp = ''; }
            this.setId(id);
            this.setIdPrincipioativo(idPrincipioativo);
            this.setNome(nome);
            this.setEnderecoHttp(enderecoHttp);
        }
        Link.prototype.getId = function () {
            return this.id;
        };
        Link.prototype.setId = function (v) {
            this.id = v;
        };
        Link.prototype.getIdPrincipioativo = function () {
            return this.idPrincipioativo;
        };
        Link.prototype.setIdPrincipioativo = function (v) {
            this.idPrincipioativo = v;
        };
        Link.prototype.getNome = function () {
            return this.nome;
        };
        Link.prototype.setNome = function (v) {
            this.nome = v;
        };
        Link.prototype.getEnderecoHttp = function () {
            return this.enderecoHttp;
        };
        Link.prototype.setEnderecoHttp = function (v) {
            this.enderecoHttp = v;
        };
        return Link;
    }());
    StorageDAO.Link = Link;
})(StorageDAO || (StorageDAO = {}));
