/// <reference path="../interface/iserviceReferencialLipidico.ts" />
var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por disponiblilizar o acesso aos dados do banco
     * referentes as Referencial Lipidico que se adapta ao paciente.
     */
    var ReferencialLipidicoDAO = (function () {
        function ReferencialLipidicoDAO(banco) {
            this.banco = banco;
        }
        ReferencialLipidicoDAO.prototype.pesquisarReferencialLipidico = function (idFaixaEtaria) {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this.banco.transaction(function (tx) {
                    tx.executeSql('SELECT r.id AS id ,'
                        + ' r.colesterolTotal AS colesterolTotal,'
                        + ' r.hdl AS hdl,'
                        + ' r.triglicerides AS triglicerides,'
                        + ' r.ldl AS ldl,'
                        + ' r.noHdl AS noHdl'
                        + ' FROM FaixaEtaria AS f'
                        + ' JOIN ReferencialLipidico AS r ON (f.idReferencialLipidico = r.id)'
                        + ' WHERE f.id = ? ', [idFaixaEtaria], function (tx, rs) {
                        console.log('ReferencialLipidicoDAO : SELECT ok');
                        if (rs.rows.length > 0) {
                            console.log('pelo menos um resultado foi encontrado');
                            resolve(new StorageDAO.ReferencialLipidico(Number(rs.rows.item(0).id), Number(rs.rows.item(0).colesterolTotal), Number(rs.rows.item(0).hdl), Number(rs.rows.item(0).triglicerides), Number(rs.rows.item(0).ldl), Number(rs.rows.item(0).noHdl)));
                        }
                        else {
                            reject(new Error('Não foi encontrados Referenciais Lipidicos.'));
                        }
                    }, function (tx, error) {
                        console.log('ReferencialLipidico : SELECT error: ' + error.message);
                        reject(error);
                    });
                });
            });
        };
        ReferencialLipidicoDAO.prototype.obterReferencialLipidico = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this.banco.transaction(function (tx) {
                    tx.executeSql('SELECT * FROM ReferencialLipidico', [], function (tx, rs) {
                        console.log('ReferencialLipidicoDAO : SELECT ok');
                        if (rs.rows.length > 0) {
                            var list = [];
                            for (var i = 0; i < rs.rows.length; i++) {
                                list[i] = new StorageDAO.ReferencialLipidico(Number(rs.rows.item(i).id), Number(rs.rows.item(i).colesterolTotal), Number(rs.rows.item(i).hdl), Number(rs.rows.item(i).triglicerides), Number(rs.rows.item(i).ldl), Number(rs.rows.item(i).noHdl));
                            }
                            resolve(list);
                        }
                        else {
                            reject(new Error('Não foi encontrados ReferencialLipidico.'));
                        }
                    }, function (tx, error) {
                        console.log('ReferencialLipidico : SELECT error: ' + error.message);
                        reject(error);
                    });
                });
            });
        };
        return ReferencialLipidicoDAO;
    }());
    StorageDAO.ReferencialLipidicoDAO = ReferencialLipidicoDAO;
})(StorageDAO || (StorageDAO = {}));
