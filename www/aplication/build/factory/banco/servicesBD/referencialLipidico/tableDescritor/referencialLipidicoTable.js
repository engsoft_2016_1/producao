/// <reference path="../../../interfaceBD/itable.ts" />
var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por manter as informações da tabela de
     * ReferencialLipidico para inicializar o banco de dados.
     */
    var ReferencialLipidicoTable = (function () {
        function ReferencialLipidicoTable() {
        }
        ReferencialLipidicoTable.prototype.populateTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql('INSERT INTO ReferencialLipidico (colesterolTotal, hdl, triglicerides, ldl, noHdl)  VALUES (?,?,?,?,?)', [150, 45, 100, 100, 0]);
                tx.executeSql('INSERT INTO ReferencialLipidico (colesterolTotal, hdl, triglicerides, ldl, noHdl)  VALUES (?,?,?,?,?)', [200, 60, 150, 129, 159]);
            }, function (error) {
                console.log('ReferencialLipidicoTable : Transaction ERROR: ' + error.message);
            }, function () {
                console.log('ReferencialLipidicoTable : Populated table OK');
            });
        };
        ReferencialLipidicoTable.prototype.dropTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql('DROP TABLE IF EXISTS ReferencialLipidico;');
            }, function (error) {
                console.log('ReferencialLipidicoTable : Delete ReferencialLipidico ERROR: ' + error.message);
            }, function () {
                console.log('ReferencialLipidicoTable : Delete table ReferencialLipidico OK');
            });
        };
        ReferencialLipidicoTable.prototype.createTable = function (banco) {
            banco.transaction(function (tx) {
                tx.executeSql("CREATE TABLE IF NOT EXISTS ReferencialLipidico ( "
                    + "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + "colesterolTotal INTEGER NOT NULL,"
                    + "hdl INTEGER NOT NULL,"
                    + "triglicerides INTEGER NOT NULL,"
                    + "ldl INTEGER NOT NULL,"
                    + "noHdl INTEGER NOT NULL)");
            }, function (error) {
                console.log('ReferencialLipidicoTable : Create ReferencialLipidico ERROR: ' + error.message);
            }, function () {
                console.log('ReferencialLipidicoTable : Create table ReferencialLipidico OK');
            });
        };
        return ReferencialLipidicoTable;
    }());
    StorageDAO.ReferencialLipidicoTable = ReferencialLipidicoTable;
})(StorageDAO || (StorageDAO = {}));
