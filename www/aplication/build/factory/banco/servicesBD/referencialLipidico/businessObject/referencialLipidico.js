var StorageDAO;
(function (StorageDAO) {
    /**
     * Classe responsável por agrupar os dados do banco para objeto de negócio Referêncial Lipídico.
     * @param id @type number
     * @param colesterolTotal @type number
     * @param hdl @type number
     * @param triglicerides @type number
     * @param ldl @type number
     * @param noHdl  @type number
     */
    var ReferencialLipidico = (function () {
        function ReferencialLipidico(id, colesterolTotal, hdl, triglicerides, ldl, noHdl) {
            if (id === void 0) { id = 0; }
            if (colesterolTotal === void 0) { colesterolTotal = 0; }
            if (hdl === void 0) { hdl = 0; }
            if (triglicerides === void 0) { triglicerides = 0; }
            if (ldl === void 0) { ldl = 0; }
            if (noHdl === void 0) { noHdl = 0; }
            this.setId(id);
            this.setColesterolTotal(colesterolTotal);
            this.setHdl(hdl);
            this.setTriglicerides(triglicerides);
            this.setLdl(ldl);
            this.setNoHdl(noHdl);
        }
        ReferencialLipidico.prototype.getId = function () {
            return this.id;
        };
        ReferencialLipidico.prototype.setId = function (v) {
            this.id = v;
        };
        ReferencialLipidico.prototype.getColesterolTotal = function () {
            return this.colesterolTotal;
        };
        ReferencialLipidico.prototype.setColesterolTotal = function (v) {
            this.colesterolTotal = v;
        };
        ReferencialLipidico.prototype.getHdl = function () {
            return this.hdl;
        };
        ReferencialLipidico.prototype.setHdl = function (v) {
            this.hdl = v;
        };
        ReferencialLipidico.prototype.getTriglicerides = function () {
            return this.triglicerides;
        };
        ReferencialLipidico.prototype.setTriglicerides = function (v) {
            this.triglicerides = v;
        };
        ReferencialLipidico.prototype.getLdl = function () {
            return this.ldl;
        };
        ReferencialLipidico.prototype.setLdl = function (v) {
            this.ldl = v;
        };
        ReferencialLipidico.prototype.getNoHdl = function () {
            return this.noHdl;
        };
        ReferencialLipidico.prototype.setNoHdl = function (v) {
            this.noHdl = v;
        };
        return ReferencialLipidico;
    }());
    StorageDAO.ReferencialLipidico = ReferencialLipidico;
})(StorageDAO || (StorageDAO = {}));
