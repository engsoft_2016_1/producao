/// <reference path="../../../componentesNegocio/resultadoColesterol/businessObject/informacoesColesterol.ts" />
/// <reference path="../../../componentesNegocio/prescritorPrincipioativo/businessObject/prescritorPrincipioativo.ts" />






/**
 * @author Carlos Alberto 
 * @module ParamsService
 * @version 0.0.0.1 
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo manter objetos de parametros entre as views.
 * @description Data de criação : 12/04/2017
 * @description Data da última alteração : 12/04/2017
 * @file www/aplication/service/componentesAplicacao/params/businessObject/params.ts
 */
module ParamsService{
     
     /**
     * Esta classe é responsável por compartilhar parametros entre estados
     * das views da aplicação.
     */
    export class Params {
        private resultado: ResultadoColesterol.Informacoes;  
        private estatinas : PrescritorPrincipioativo.Item[];
        
        private teste : number ;
        
        
        
        constructor(){
           
        }
        public getteste() : number {
            return this.teste;
        }
        public setteste(v : number) {
            this.teste = v;
        }
        getResultado() : ResultadoColesterol.Informacoes{
            return this.resultado;
        }
        setResultado(v :ResultadoColesterol.Informacoes) {
             this.resultado = v;
        }
        getEstatinas() : PrescritorPrincipioativo.Item[] {
            return this.estatinas;
        }
        setEstatinas(v : PrescritorPrincipioativo.Item[]) {
            this.estatinas = v;
        }
    }
}
