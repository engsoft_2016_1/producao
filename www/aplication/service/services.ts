/// <reference path="../factory/banco/config/storage.ts"/>


/**
 * Classe responsável por disponibilizar os serviços aos 
 * controllers.
 */

class ServiceFacade implements Services {
    
    private factory: Storage;
    constructor(dao: any){
            this.factory = dao;
    }
    servico() {
    }
}

interface Services{
    servico():any;
}


