/// <reference path="../interfaces/icontraindicacoesService.ts" />
/// <reference path="../../../../factory/banco/interfaceBD/IFacadeBD.ts" />



/**
 * @author Carlos Alberto 
 * @module ContraindicacoesService
 * @version 0.0.0.1 
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo disponibilizar as informações das contraindicações aos controllers.
 * @description Data de criação : 04/03/2017
 * @description Data da última alteração : 04/03/2017
 * @file www/aplication/service/contraindicacoes/contraindicacoesService.ts
 */
module ContraindicacoesService{
        /**
         * Esta classe disponibiliza as funções que dão acesso as contraindicações dos 
         * princípios ativos.
         */
        export class Contraindicacoes implements IContraindicacoesService{
               private dao : StorageDAO.IFacadeBD;
                constructor(dao: StorageDAO.IFacadeBD) {
                   this.dao = dao;                  
                }

                async obterContraindicacoes(sexo: number,faixaEtaria: number):Promise<StorageDAO.Contraindicacao[]>{
                   return await this.dao.obterContraindicacoes(sexo,faixaEtaria);
                }
                
        }
}