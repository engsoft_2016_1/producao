module ContraindicacoesService{

    export interface IContraindicacoesService{
         /**
                 * Esta função retorna uma lista com as contraindicações já filtradas pelo
                 * sexo e faixa etária do paciente.
                 * @param sexo @type number
                 * @param faixaEtaria @type number
                 */
                obterContraindicacoes(sexo: number,faixaEtaria: number):Promise<StorageDAO.Contraindicacao[]>;
            
        
    }
}