/// <reference path="../../referencialColesterol/interfaces/ifactory.ts" />
/// <reference path="../interfaces/iresultadoColesterol.ts" />
/// <reference path="../../resultadoColesterol/businessObject/informacoesColesterol.ts" />
/// <reference path="../interfaces/iinformacoesPaciente.ts" />
/// <reference path="../businessObject/informacoesPaciente.ts" />
/// <reference path="../../calculadoraColesterol/interfaces/icalculadoraColesterol.ts" />




/**
 * @author Carlos Alberto 
 * @module ResultadoColesterol
 * @version 0.0.0.1 
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo agrupar e fornecer as informações sobre 
 * o colesterol do paciente e tornar transparente os cálculos do aplicativo.
 * @description Data de criação : 28/03/2017
 * @description Data da última alteração : 28/03/2017
 * @file www/aplication/service/resultadoColesterol/resultadoColesterol.ts
 */
module ResultadoColesterol {

        /**
         * Esta classe disponibiliza os dados que indicam os níveis de colesterol do paciente.
         */
        export class Resultado implements IResultadoColesterol {

                private informacoesPaciente: ResultadoColesterol.IInformacaoPaciente;
                private referencialColesterol: ReferencialColesterol.IFactory;
                private calculoColesterol: CalculoColesterolService.IServiceCalculadoraColesterol;
                constructor(referencialColesterol: ReferencialColesterol.IFactory,
                        calculoColesterol: CalculoColesterolService.IServiceCalculadoraColesterol) {
                        this.referencialColesterol = referencialColesterol;
                        this.calculoColesterol = calculoColesterol;
                }

                async obterResultado(paciente: DadosPacienteService.Paciente): Promise<Informacoes> {

                        console.log('Resultado.obterResultado(paciente) : Iniciado');
                        if (paciente == null || paciente == undefined) {
                                return Promise.reject(new Error('Resultado.obterResultado(paciente) : obterResultado : Parâmetro null'));
                        }
                        if (paciente.getLdl() <= 0) {
                                paciente.setLdl(this.calculoColesterol.calcularLDL(paciente));
                        }

                        if (paciente.getPercentualReducaoLdl() <= 0) {
                                await this.calculoColesterol.calcularPercentualReducaoLDL(paciente).then(
                                        (res: number) => {
                                                paciente.setPercentualReducaoLdl(res);
                                        }
                                ).catch((err: Error) => {
                                        return Promise.reject(new Error('Resultado.obterResultado(paciente) : calcularPercentualReducaoLDL : Ocorreu um problema no cálculo do Percentual de redução LDL.'));
                                });
                        }
                        if (paciente.getPercentualReducaoTg() <= 0) {
                                await this.calculoColesterol.calcularPercentualReducaoTG(paciente).then(
                                        (res: number) => {
                                                paciente.setPercentualReducaoTg(res);
                                        }
                                ).catch((err: Error) => {
                                        return Promise.reject(new Error('Resultado.obterResultado(paciente) : setPercentualReducaoTg : Ocorreu um problema no cálculo do Percentual de redução do Triglicerídeos (TG).'));
                                });
                        }
                        if (paciente.getPercentualAumentoHdl() <= 0) {
                                await this.calculoColesterol.calcularPercentualAumentoHdl(paciente).then(
                                        (res: number) => {
                                                paciente.setPercentualAumentoHdl(res);
                                        }
                                ).catch((err: Error) => {
                                        return Promise.reject(new Error('Resultado.obterResultado(paciente) : getPercentualAumentoHdl : Ocorreu um problema no cálculo do Percentual de aumento do HDL.'));
                                });
                        }

                        let informacoesPaciente = new ResultadoColesterol.InformacoesPaciente(paciente);

                        let informacoes = new ResultadoColesterol.Informacoes();
                        await informacoesPaciente.nivelLdl(this.referencialColesterol).then(
                                (res: ReferencialColesterol.TipoCategoria) => {
                                        informacoes.setNivelLdl(res);
                                }
                        ).catch((err: Error) => {
                                Promise.reject(new Error('Resultado.obterResultado(paciente) : nivelLdl : ' + err.message));
                        });

                        await informacoesPaciente.nivelHdl(this.referencialColesterol).then(
                                (res: ReferencialColesterol.TipoCategoria) => {
                                        informacoes.setNivelHdl(res);
                                }
                        ).catch((err: Error) => {
                                Promise.reject(new Error('Resultado.obterResultado(paciente) : nivelHdl : ' + err.message));
                        });

                        await informacoesPaciente.nivelTriglicerides(this.referencialColesterol).then(
                                (res: ReferencialColesterol.TipoCategoria) => {
                                        informacoes.setNivelTriglicerides(res);
                                }
                        ).catch((err: Error) => {
                                Promise.reject(new Error('Resultado.obterResultado(paciente) : nivelTriglicerides : ' + err.message));
                        });

                        await informacoesPaciente.nivelColesterolTotal(this.referencialColesterol).then(
                                (res: ReferencialColesterol.TipoCategoria) => {
                                        informacoes.setNivelColesterolTotal(res);
                                }
                        ).catch((err: Error) => {
                                Promise.reject(new Error('Resultado.obterResultado(paciente) : nivelColesterolTotal : ' + err.message));
                        });

                        await informacoesPaciente.reducaoLdl().then(
                                (res: number) => {
                                        informacoes.setReducaoLdl(res);
                                }
                        ).catch((err: Error) => {
                                Promise.reject(new Error('Resultado.obterResultado(paciente) : reducaoLdl : ' + err.message));
                        });

                        await informacoesPaciente.reducaoTriglicerides().then(
                                (res: number) => {
                                        informacoes.setReducaoTriglicerides(res);
                                }
                        ).catch((err: Error) => {
                                Promise.reject(new Error('Resultado.obterResultado(paciente) : reducaoTriglicerides : ' + err.message));
                        });

                        await informacoesPaciente.aumentoHdl().then(
                                (res: number) => {
                                        informacoes.setAumentoHdl(res);
                                }
                        ).catch((err: Error) => {
                                Promise.reject(new Error('Resultado.obterResultado(paciente) : aumentoHdl() : ' + err.message));
                        });

                        await informacoesPaciente.resultadoLdl(this.calculoColesterol).then(
                                (res: DadosPacienteService.Paciente) => {
                                        informacoes.setDadosPaciente(res);
                                }
                        ).catch((err: Error) => {
                                Promise.reject(new Error('Resultado.setDadosPaciente(calculoColesterol) : resultadoLdl : ' + err.message));
                        });

                        await informacoesPaciente.informacoesComplementaresLDL(
                                informacoes.getNivelLdl(),
                                paciente.getCodReferencial(),
                                this.referencialColesterol).then(
                                (res: string) => {
                                        informacoes.setInformacoesComplementaresLDL(res);
                                }
                                ).catch((err: Error) => {
                                        console.log('Resultado.obterResultado(paciente) : informacoesComplementaresLDL : ' + err.message);
                                        Promise.reject(new Error('Resultado.obterResultado(paciente) : informacoesComplementaresLDL : ' + err.message));
                                });

                        console.log('Resultado.obterResultado(paciente) : Finalizado');
                        return Promise.resolve(informacoes);
                }

        }
}
