/// <reference path="../interfaces/iinformacoesPaciente.ts" />
/// <reference path="../../referencialColesterol/businessObject/categoria.ts" />
/// <reference path="../../dadosPaciente/businessObject/dadosPaciente.ts" />
/// <reference path="../../referencialColesterol/businessObject/referencialColesterol.ts" />
/// <reference path="../../calculadoraColesterol/businessObject/calculoColesterol.ts" />




module ResultadoColesterol {

    /**
     * Esta classe extrai informações apartir dos dados do paciente.
     */
    export class InformacoesPaciente implements ResultadoColesterol.IInformacaoPaciente {


        private paciente: DadosPacienteService.Paciente;

        constructor(paciente: DadosPacienteService.Paciente) {
            this.paciente = paciente;
        }

        async nivelLdl(referenciais: ReferencialColesterol.IFactory): Promise<ReferencialColesterol.TipoCategoria> {
            let tipoCategoria: ReferencialColesterol.TipoCategoria;
            let ldl = this.paciente.getLdl();

            if (ldl == null || ldl == undefined) {
                Promise.reject(new Error('O ldl não possui um valor válido'));
            }
            console.log("code referencial : " + this.paciente.getCodReferencial());
            await referenciais.obterReferencial(this.paciente.getCodReferencial()).then(
                (res: ReferencialColesterol.IReferencial) => {
                    res.obterLdl().find(element => {
                        if (ldl >= element.getMin() && ldl <= element.getMax()) {
                            tipoCategoria = element.getTipoCategoria();
                            return true;
                        }
                    }
                    );
                }
            ).catch((err: Error) => {
                Promise.reject('InformacoesPaciente : ' + err.message);
            });
            return Promise.resolve(tipoCategoria);
        }
        async nivelHdl(referenciais: ReferencialColesterol.IFactory): Promise<ReferencialColesterol.TipoCategoria> {
            let tipoCategoria: ReferencialColesterol.TipoCategoria;
            let hdl = this.paciente.getHdl();

            if (hdl == null || hdl == undefined) {
                Promise.reject(new Error('O HDL não possui um valor válido'));
            }

            await referenciais.obterReferencial(this.paciente.getCodReferencial()).then(
                (res: ReferencialColesterol.IReferencial) => {
                    res.obterHdl().find(element => {
                        if (hdl >= element.getMin() && hdl <= element.getMax()) {
                            tipoCategoria = element.getTipoCategoria();
                            return true;
                        }
                    }
                    );
                }
            ).catch((err: Error) => {
                Promise.reject(new Error('InformacoesPaciente : ' + err.message));
            });
            return Promise.resolve(tipoCategoria);
        }
        async nivelTriglicerides(referenciais: ReferencialColesterol.IFactory): Promise<ReferencialColesterol.TipoCategoria> {

            let tipoCategoria: ReferencialColesterol.TipoCategoria;
            let tg = this.paciente.getTriglicerides();

            if (tg == null || tg == undefined) {
                Promise.reject(new Error('O HDL não possui um valor válido'));
            }

            await referenciais.obterReferencial(this.paciente.getCodReferencial()).then(
                (res: ReferencialColesterol.IReferencial) => {
                    res.obterTriglicerides().find(element => {
                        if (tg >= element.getMin() && tg <= element.getMax()) {
                            tipoCategoria = element.getTipoCategoria();
                            return true;
                        }
                    }
                    );
                }
            ).catch((err: Error) => {
                Promise.reject(new Error('InformacoesPaciente : ' + err.message));
            });
            return Promise.resolve(tipoCategoria);
        }

        async  nivelColesterolTotal(referenciais: ReferencialColesterol.IFactory): Promise<ReferencialColesterol.TipoCategoria> {
            let tipoCategoria: ReferencialColesterol.TipoCategoria;
            let ct = this.paciente.getColesterolTotal();

            if (ct == null || ct == undefined) {
                Promise.reject(new Error('O HDL não possui um valor válido'));
            }

            await referenciais.obterReferencial(this.paciente.getCodReferencial()).then(
                (res: ReferencialColesterol.IReferencial) => {
                    res.obterColesterolTotal().find(element => {
                        if (ct >= element.getMin() && ct <= element.getMax()) {
                            tipoCategoria = element.getTipoCategoria();
                            return true;
                        }
                    }
                    );
                }
            ).catch((err: Error) => {
                Promise.reject(new Error('InformacoesPaciente : ' + err.message));
            });

            return Promise.resolve(tipoCategoria);
        }
        async reducaoLdl(): Promise<number> {
            if (this.paciente.getPercentualReducaoLdl() > 0) {
                return Promise.resolve(this.paciente.getPercentualReducaoLdl());
            }
            return Promise.reject(new Error('InformacoesPaciente : O percentual de redução de LDL é menor que zero.'));
        }
        async  reducaoTriglicerides(): Promise<number> {
            if (this.paciente.getTriglicerides() > 0) {
                return Promise.resolve(this.paciente.getPercentualReducaoTg());
            }
            return Promise.reject(new Error('InformacoesPaciente : O percentual de redução  de triglicérides é menor que zero.'));

        }
        async aumentoHdl(): Promise<number> {
            if (this.paciente.getPercentualAumentoHdl() > 0) {
                return Promise.resolve(this.paciente.getPercentualAumentoHdl());
            }
            return Promise.reject(new Error('InformacoesPaciente : O percentual de aumento  do HDL é menor que zero.'));

        }
        async informacoesComplementaresLDL(
            nivelLdl: number,
            referencialDoPaciente: number,
            referencialFactory: ReferencialColesterol.IFactory)
            : Promise<string> {

             let categoriaLDL: ReferencialColesterol.Categoria;
               await referencialFactory.obterReferencial(referencialDoPaciente).then(
                    (res:ReferencialColesterol.IReferencial)=>{
                        categoriaLDL = res.obterLdl().find(
                            (element: ReferencialColesterol.Categoria)=>{
                                if(element.getTipoCategoria() == nivelLdl){
                                  categoriaLDL = element;   
                                  return true;
                                }
                            }
                        );
                    }
                );
             console.log('InformacoesPaciente.informacoesComplementaresLDL() :categoriaLDL: = '+categoriaLDL.getNome());
              
            switch (nivelLdl) {

                case ReferencialColesterol.TipoCategoria.Otimo: {
                    return Promise.resolve('Segundo a 5º diretriz Brasileira de '
                    +'dislipidemias e prevenção'
                    +' da aterosclerose: seu LDL-C está <= '+categoriaLDL.getMax()+', Ótimo.');
                }
                case ReferencialColesterol.TipoCategoria.Desejavel: {
                    if(ReferencialColesterol.Criancas2Ate19Anos.keyReferencial == referencialDoPaciente){
                            return Promise.resolve('Segundo a 5º diretriz Brasileira de '
                                    +'dislipidemias e prevenção'
                                    +' da aterosclerose: seu LDL-C está' 
                                    +' <= '+categoriaLDL.getMax()+', Desejável.');
                    }
                    return Promise.resolve('Segundo a 5º diretriz Brasileira de '
                    +'dislipidemias e prevenção'
                    +' da aterosclerose: seu LDL-C está >= '+categoriaLDL.getMin()
                    +' e <= '+categoriaLDL.getMax()+', Desejável.');
                }
                case ReferencialColesterol.TipoCategoria.Limitrofe: {
                    return Promise.resolve('Atenção. Segundo a 5º diretriz Brasileira de '
                    +'dislipidemias e prevenção'
                    +' da aterosclerose: seu LDL-C está no limite, de '+categoriaLDL.getMin()
                    +' à '+categoriaLDL.getMax());
                }
                case ReferencialColesterol.TipoCategoria.Alto: {
                    if(ReferencialColesterol.Criancas2Ate19Anos.keyReferencial == referencialDoPaciente){
                            return  Promise.resolve('Cuidado. Segundo a 5º diretriz Brasileira de '
                            +'dislipidemias e prevenção'
                            +' da aterosclerose: o LDL-C está em nível elevado, '
                            +' acima de '+categoriaLDL.getMin());
                    }
                    return Promise.resolve('Cuidado. Segundo a 5º diretriz Brasileira de '
                    +'dislipidemias e prevenção'
                    +' da aterosclerose: o LDL-C está em nível alto, de '+categoriaLDL.getMin()
                    +' à '+categoriaLDL.getMax());
                }
                case ReferencialColesterol.TipoCategoria.MuitoAlto: {
                  return Promise.resolve('Muito Cuidado! Segundo a 5º diretriz Brasileira de '
                    +'dislipidemias e prevenção'
                    +' da aterosclerose: o LDL-C está muito alto, acima de '+categoriaLDL.getMin());
                }

                default:
                    break;
            }
        }
        resultadoLdl(calculoColesterol: CalculoColesterolService.IServiceCalculadoraColesterol)
            : Promise<DadosPacienteService.Paciente> {
            let ldl = calculoColesterol.calcularLDL(this.paciente);
            if (ldl < 0) {
                return Promise.reject('Erro nos valores do colesterol.(CT),(TG),(HDL)');
            }
            this.paciente.setLdl(ldl);

            return Promise.resolve(this.paciente);
        }


    }
}