/// <reference path="../../referencialColesterol/businessObject/referencialColesterol.ts" />
/// <reference path="../../dadosPaciente/businessObject/dadosPaciente.ts" />



module ResultadoColesterol{
    /**
     * Esta classe agrupa todas as informações do paciente 
     * para poderem ser exibidas da melhor forma pelas UI (User Interface).
     */
   export class Informacoes {
        
        private nivelLdl : ReferencialColesterol.TipoCategoria;
        private nivelHdl : ReferencialColesterol.TipoCategoria;
        private nivelTriglicerides : ReferencialColesterol.TipoCategoria;
        private nivelColesterolTotal : ReferencialColesterol.TipoCategoria;
        private reducaoLdl : number;
        private reducaoTriglicerides : number;
        private aumentoHdl : number;
        private informacoesComplementaresLDL : string;
        private dadosPaciente: DadosPacienteService.Paciente = null;

        constructor(nivelLdl:ReferencialColesterol.TipoCategoria = 0,
                    nivelHdl:ReferencialColesterol.TipoCategoria = 0,
                    nivelTriglicerides : ReferencialColesterol.TipoCategoria = 0,
                    nivelColesterolTotal : ReferencialColesterol.TipoCategoria = 0,
                    reducaoLdl : number = 0,
                    reducaoTriglicerides : number = 0,
                    aumentoHdl : number = 0) {
            this.nivelLdl = nivelLdl;
            this.nivelHdl = nivelHdl;
            this.nivelTriglicerides = nivelTriglicerides;
            this.nivelColesterolTotal = nivelColesterolTotal;
            this.reducaoLdl = reducaoLdl;
            this.reducaoTriglicerides = reducaoTriglicerides;
            this.aumentoHdl = aumentoHdl;
        }

        public getNivelLdl() : ReferencialColesterol.TipoCategoria {
            return this.nivelLdl;
        }
        public setNivelLdl(v : ReferencialColesterol.TipoCategoria) {
            this.nivelLdl = v;
        }
        public getNivelHdl() : ReferencialColesterol.TipoCategoria {
            return this.nivelHdl;
        }
        public setNivelHdl(v : ReferencialColesterol.TipoCategoria) {
            this.nivelHdl = v;
        }
        public getNivelTriglicerides() : ReferencialColesterol.TipoCategoria {
            return this.nivelTriglicerides;
        }
        public setNivelTriglicerides(v : ReferencialColesterol.TipoCategoria) {
            this.nivelTriglicerides = v;
        }
        public getNivelColesterolTotal() : ReferencialColesterol.TipoCategoria {
            return this.nivelColesterolTotal;
        }
        public setNivelColesterolTotal(v : ReferencialColesterol.TipoCategoria) {
            this.nivelColesterolTotal = v;
        }
        public getReducaoLdl() : number {
            return this.reducaoLdl;
        }
        public setReducaoLdl(v : number) {
            this.reducaoLdl = v;
        }
        public getReducaoTriglicerides() : number {
            return this.reducaoTriglicerides;
        }
        public setReducaoTriglicerides(v : number) {
            this.reducaoTriglicerides = v;
        }
        public getAumentoHdl() : number {
            return this.aumentoHdl;
        }
        public setAumentoHdl(v : number) {
            this.aumentoHdl = v;
        }
        public getInformacoesComplementaresLDL() : string {
            return this.informacoesComplementaresLDL;
        }
        public setInformacoesComplementaresLDL(v : string) {
            this.informacoesComplementaresLDL = v;
        }
        public getDadosPaciente():DadosPacienteService.Paciente {
            return this.dadosPaciente;
        }
        public setDadosPaciente(v :DadosPacienteService.Paciente) {
            this.dadosPaciente = v;
        }
    }
}