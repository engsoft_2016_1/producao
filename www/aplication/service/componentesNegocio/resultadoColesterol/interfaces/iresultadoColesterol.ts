/// <reference path="../../dadosPaciente/businessObject/dadosPaciente.ts" />
/// <reference path="../businessObject/informacoesColesterol.ts" />




module ResultadoColesterol{
    /**
     * Esta é a interface do serviço ResultadoColesterol
     */
    export interface IResultadoColesterol{
        

            obterResultado(paciente: DadosPacienteService.Paciente):Promise<Informacoes>;
    }
}