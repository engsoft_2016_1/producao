/// <reference path="../../referencialColesterol/enums/tipoCategoria.ts" />


module ResultadoColesterol{
    /**
     * Esta interface define os as informações a serem extraídas dos dados do paciente. 
     */
    export interface IInformacaoPaciente{
            
            nivelLdl(referenciais:ReferencialColesterol.IFactory):Promise<ReferencialColesterol.TipoCategoria>;
            nivelHdl(referenciais:ReferencialColesterol.IFactory):Promise<ReferencialColesterol.TipoCategoria>;
            nivelTriglicerides(referenciais:ReferencialColesterol.IFactory):Promise<ReferencialColesterol.TipoCategoria>;
            nivelColesterolTotal(referenciais:ReferencialColesterol.IFactory):Promise<ReferencialColesterol.TipoCategoria>;
            reducaoLdl():Promise<number>;
            reducaoTriglicerides():Promise<number>;
            aumentoHdl():Promise<number>;
            informacoesComplementaresLDL(nivelLdl:number,referencialDoPaciente: number,referencialFactory: ReferencialColesterol.IFactory):Promise<string>;
            resultadoLdl(calculoColesterol:  CalculoColesterolService.IServiceCalculadoraColesterol):Promise<DadosPacienteService.Paciente>;
    }
}
        