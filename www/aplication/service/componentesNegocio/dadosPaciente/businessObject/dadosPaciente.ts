/**
 * @author Carlos Alberto 
 * @module DadosPacienteService
 * @version 0.0.0.1 
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo armazenar as informações que forem passadas pelo 
 * médico sobre o paciente.
 * @description Data de criação : 19/03/2017
 * @description Data da última alteração : 19/03/2017
 * @file www/aplication/service/dadosPaciente/dadosPaciente.ts
 */
module DadosPacienteService{
     /**
     * Esta classe agrupa as informações informadas pelo médico sobre o paciente na tela principal.
     */
    export class Paciente {
            private codReferencial:number = 0;
            private sexo : number = 0;
			private colesterolTotal:number = 0;
			private hdl:number = 0;
			private triglicerides:number = 0;
			private ldl:number = 0;
			private noHdl:number = 0;
            private contraindicacoes: number[] = [];
            private percentualReducaoLdl : number = 0;
            private percentualAumentoHdl : number = 0;
            private percentualReducaoTg : number = 0;
            
            
            
            
/**
 * @param codReferencial - @type number - Armazena o id da faixa etária do paciente.  
 * @param sexo - @type number - Armazena o id do sexo do paciente.  
 * @param colesterolTotal - @type number - Armazena o valor do ColesterolTotal do paciente.
 * @param hdl - @type number - Armazena o valor do HDL do paciente.
 * @param triglicerides - @type number - Armazena o valor dos Triglicérides do paciente.
 * @param contraindicacoes - @type number[] - Armazena um array com os ids das contraindicações do paciente.
 */
        constructor( 
             codReferencial:number = 0,
             sexo: number = 0,
			 colesterolTotal:number = 0,
			 hdl:number = 0,
			 triglicerides:number = 0,
             contraindicacoes: number[] = []) {
            this.setCodReferencial(codReferencial);
            this.setSexo(sexo);
            this.setColesterolTotal(colesterolTotal);
            this.setHdl(hdl);
            this.setTriglicerides(triglicerides);
            this.setContraindicacoes(contraindicacoes);
        }
        public setCodReferencial(v: number) {
            this.codReferencial = v;
        }
        public getCodReferencial(): number {
            return this.codReferencial;
        }
        
        public setColesterolTotal(v: number) {
            this.colesterolTotal = v;
        }
        public getColesterolTotal(): number {
            return this.colesterolTotal;
        }
        public setHdl(v: number) {
            this.hdl = v;
        }
        public getHdl(): number {
            return this.hdl;
        }
        public setTriglicerides(v: number) {
            this.triglicerides = v;
        }
        public getTriglicerides(): number {
            return this.triglicerides;
        }
        public setLdl(v: number) {
            this.ldl = v;
        }
        public getLdl(): number {
            return this.ldl;
        }
        public setNoHdl(v: number) {
            this.noHdl = v;
        }
        public getNoHdl(): number {
            return this.noHdl;
        }
        
        public setContraindicacoes(v: number[]) {
            this.contraindicacoes = v;
        }
        public getContraindicacoes(): number[] {
            return this.contraindicacoes;
        }
        public getPercentualReducaoLdl() : number {
                return this.percentualReducaoLdl;
        }
        public setPercentualReducaoLdl(v : number) {
                this.percentualReducaoLdl = v;
        }
        public getPercentualAumentoHdl() : number {
                return this.percentualAumentoHdl;
        }
        public setPercentualAumentoHdl(v : number) {
                this.percentualAumentoHdl = v;
        }
        public getPercentualReducaoTg() : number {
                return this.percentualReducaoTg;
        }
        public setPercentualReducaoTg(v : number) {
                this.percentualReducaoTg = v;
        }
        public getSexo() : number {
                return this.sexo;
        }
        public setSexo(v : number) {
                this.sexo = v;
        }
    }
}
