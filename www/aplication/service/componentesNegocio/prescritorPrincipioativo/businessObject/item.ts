/// <reference path="../../dadosPaciente/businessObject/dadosPaciente.ts" />
/// <reference path="../../../../factory/banco/servicesBD/principioativo/businessObject/principioativo.ts" />
/// <reference path="../../../../factory/banco/servicesBD/contraindicacao/businessObject/contraindicacao.ts" />




module PrescritorPrincipioativo{

         /**
         * Esta classe agrupa cada principioativo com metadados 
         * que serão gerados posteriormente.
         */
       export class Item {
            
            private principioativo : StorageDAO.Principioativo;
            private pontuacaoReducaoLdl : number = 0;
            private pontuacaoReducaoTg : number = 0;
            private pontuacaoAumentoHdl : number = 0;
            private dadosPaciente : DadosPacienteService.Paciente = null;
            private contraindicacoes : StorageDAO.Contraindicacao[] = [];            
            private total : number = 0;
            
            constructor(principioativo: StorageDAO.Principioativo) {
                this.principioativo = principioativo;
            }

           /**
             * Obtém um objeto do tipo Principioativo.
             */
            getPrincipioativo():StorageDAO.Principioativo{
                 return this.principioativo;
            }
            /**
             * Retorna a pontuação dada pelo avaliador quanto a redução do LDL.
             */
            getPontuacaoReducaoLdl() : number {
                return this.pontuacaoReducaoLdl;
            }
            /**
             * Define a pontuação dada pelo avaliador quanto a redução do LDL.
             */
            setPontuacaoReducaoLdl(v : number) {
                this.pontuacaoReducaoLdl = v;
            }
             /**
             * Obtém o resultado da pontuação dada pelo avaliador quanto a redução do triglicérides.
             */
            getPontuacaoReducaoTg() : number {
                return this.pontuacaoReducaoTg;
            }
            /**
             * Define a pontuação dada pelo avaliador quanto a redução do triglicérides.
             */
            setPontuacaoReducaoTg(v : number) {
                this.pontuacaoReducaoTg = v;
            }
             /**
             * Obtém o resultado da pontuação dada pelo avaliador quanto a redução do triglicérides.
             */
            getPontuacaoAumentoHdl() : number {
                return this.pontuacaoAumentoHdl;
            }
            /**
             * Define a pontuação dada pelo avaliador quanto a redução do triglicérides.
             */
            setPontuacaoAumentoHdl(v : number) {
                this.pontuacaoAumentoHdl = v;
            }
            
            /**
             * Retorna a pontuação total dada pelo avaliador com base em todos os requisitos
             * avaliados. Quanto maior este resultado, mais este principio deve ser indicado.
             */
            getTotal() : number {
                return this.total;
            }
             /**
             * Define a pontuação total dada pelo avaliador com base em todos os requisitos
             * avaliados. Quanto maior este resultado, mais este principio deve ser indicado.
             */
            setTotal(v : number) {
                this.total = v;
            }

            /**
             * Retorna os dados do paciente que foram usados como base na classificação do
             * principioativo.
             */
            getDadosPaciente() : DadosPacienteService.Paciente {
                return this.dadosPaciente;
            }
            /**
             * Define os dados do paciente que foram usados como base na classificação do
             * principioativo.
             */
            setDadosPaciente(v : DadosPacienteService.Paciente) {
                this.dadosPaciente = v;       
            }
             /**
             * Retorna as informações de contraindicações que casam com as que
             *  esta estatina possui.
             */
            getContraindicacoes() : StorageDAO.Contraindicacao[] {
                return this.contraindicacoes;
            }
            /**
             * Define as informações de contraindicações que casam com as que
             *  esta estatina possui.
             */
            setContraindicacoes(v : StorageDAO.Contraindicacao[]) {
                this.contraindicacoes = v;
            }
            
        }
}