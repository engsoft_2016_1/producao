/// <reference path="../interfaces/iservicePrescritor.ts" />
/// <reference path="../../../../factory/banco/interfaceBD/IFacadeBD.ts" />
/// <reference path="../../calculadoraColesterol/interfaces/icalculadoraColesterol.ts" />
/// <reference path="item.ts" />
/// <reference path="../../avaliador/interfaces/iserviceAvaliador.ts" />
/// <reference path="../../judge/businessObject/judge.ts" />







/**
 * @author Carlos Alberto 
 * @module PrescritorPrincipioativo
 * @version 0.0.0.1 
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo s funções que calculam os níveis de LDL, NoHDL e 
 * o percentual de redução LDL.
 * @description Data de criação : 19/03/2017
 * @description Data da última alteração : 19/03/2017
 * @file www/aplication/service/calculadoraColesterol/calculoColesterol.ts
 */
module PrescritorPrincipioativo{
     
     /**
     * Esta classe é responsável por calcular os valores do colesterol LDL,NoHDL e de redução do LDL.
     */
    export class Prescritor implements IServicePrescritor {
        
        private dao: StorageDAO.IFacadeBD;
        private calculoColesterol: CalculoColesterolService.IServiceCalculadoraColesterol;
        private avaliador: AvaliadorPrincipioativoService.IServiceAvaliador;
        private judge : JudgeService.IServiceJudge;

        constructor(dao: StorageDAO.IFacadeBD,
                    calculoColesterol: CalculoColesterolService.IServiceCalculadoraColesterol,
                    avaliador: AvaliadorPrincipioativoService.IServiceAvaliador,
                    judge: JudgeService.IServiceJudge){
                this.dao = dao;
                this.calculoColesterol = calculoColesterol;
                this.avaliador = avaliador;
                this.judge = judge;
        }

        async consultarPrincipioativo(paciente: DadosPacienteService.Paciente): Promise<Item[]> {
            let ldl = this.calculoColesterol.calcularLDL(paciente);
            if(ldl < 0){
                    return Promise.reject(new Error('Ooops...Parece que algum dos dados'
                    +' de colesterol estão incorretos. Por favor corrija os valores do '
                    +'Colesterol Toral, HDL-C, ou Triglicerídeos. '));
            }
            paciente.setLdl(ldl);

            console.log('Prescritor : valor do LDL paciente = '+paciente.getLdl());

            let percentualReducao: number;
            await this.calculoColesterol.calcularPercentualReducaoLDL(paciente).then(
                (res:number)=>{
                    percentualReducao = res;
            });
            console.log('Prescritor : percentual de redução LDL = '+percentualReducao);
        
            paciente.setPercentualReducaoLdl(percentualReducao);
            
            
            await this.calculoColesterol.calcularPercentualReducaoTG(paciente).then(
                    (res: number)=>{
                          console.log('Prescritor : percentual de redução TG = '+res);
                           paciente.setPercentualReducaoTg(res); 
                    }
            );
           
            await this.calculoColesterol.calcularPercentualAumentoHdl(paciente).then(
                    (res: number)=>{
                          console.log('Prescritor : percentual de aumento HDL = '+res);
                           paciente.setPercentualAumentoHdl(res); 
                    }
            );
           
            let itens: Item[] = []; 
            await this.dao.obterPrincipiosativos().then(
                    (res: StorageDAO.Principioativo[]) => {
                        console.log('Prescritor : quantidade de princ, retornados '+res.length);
                       
                       for(let i = 0; i < res.length; i++){
                           itens[i] = new Item(res[i]);
                           itens[i].setDadosPaciente(paciente);
                            
                       }
                       
                    }
                    
                ).catch((reason:Error)=>{
                     console.log('PrescritorPrincipioativo : '+ reason.message);
                     return Promise.resolve([]);
                });
                   
            let array = itens.map((e:PrescritorPrincipioativo.Item)=>{
                return new Promise<PrescritorPrincipioativo.Item>((resolve:any,reject:any)=>{
                             this.dao.obterContraindicacaoPrincipioativo(
                                    e.getPrincipioativo().getId(),paciente.getContraindicacoes())
                                    .then((res:StorageDAO.Contraindicacao[])=>{
                                        e.setContraindicacoes(res);
                                        resolve(e);
                                    
                                    }).catch((err:Error)=>{
                                        console.log('PrescritorPrincipioativo : Error = '+err.message);
                                        resolve(e);
                                    });
                });
                       
            });
            
            let itensJulgados: PrescritorPrincipioativo.Item[] = []; 
      
             await Promise.all(array).then(
                            (list:PrescritorPrincipioativo.Item[])=>{
                                console.log('Prescritor : setPercentualReducaoTg = '+paciente.getPercentualReducaoTg())
                                
                                list.forEach((e:Item)=>{
                                    console.log('Nome :'+ e.getPrincipioativo().getNome());
                                    e.getContraindicacoes().forEach(e=>{
                                        console.log('Contraindicação '+e.getNome());
                                    });
                                });
                                //os principios ativos seriam passados para o avaliador.
                                let itensAvaliados =  this.avaliador.avaliar(list); 
                                //o juiz comparará os valores de todos os requisitos para obter as melhores estatinas.
                                itensJulgados = this.judge.julgar(itensAvaliados);
                              
                            }
                        ).catch(
                            (err:Error)=>{
                                console.log('PrescritorPrincipioativo : Error = '+err.message);
                            });   
                /*});*/

               return Promise.resolve(itensJulgados);
         
                                  
        }

    }
}
