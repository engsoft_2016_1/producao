/// <reference path="../../prescritorPrincipioativo/businessObject/item.ts" />
/// <reference path="../../dadosPaciente/businessObject/dadosPaciente.ts" />



module PrescritorPrincipioativo{

       export interface IServicePrescritor{
            /**
             * Esta função busca no banco de dados os principios ativos e classifica-os em ordem do 
             * mais indicado ao menos indicado.
             */
            consultarPrincipioativo(paciente: DadosPacienteService.Paciente):Promise<Item[]>;
             
            
        }
}