/// <reference path="../../dadosPaciente/businessObject/dadosPaciente.ts" />


module CalculoColesterolService{

       export interface IServiceCalculadoraColesterol{
            /**
             * Esta função calcula o LDL do paciente a partir dos dados informados pelo médico
             * e retorna o valor em número Interiro do valor do LDL.
             */
            calcularLDL(paciente: DadosPacienteService.Paciente):number;
             /**
              * Esta função calcula o valor do NoHDL do paciente a partir dos dados informados pelo
              * médico, ele retorna um resultado em número interiro.
              */            
            calcularNoHDL(paciente: DadosPacienteService.Paciente):number;
            /**
             * Esta função calcula o valor de redução do colesterol LDL do paciente a partir dos dados informados pelo
             * médico, ele retorna um resultado em número interiro.
             */
            calcularPercentualReducaoLDL(paciente : DadosPacienteService.Paciente): Promise<number>;
            /**
             * Esta função calcula o valor de redução do Triglicérides de acordo com o referêncial lipídico.
             * Ele retorna a porcentagem de redução.
             */
            calcularPercentualReducaoTG(paciente : DadosPacienteService.Paciente): Promise<number>;
            /**
             * Esta função calcula o valor de aumento do HDl de acordo com o referêncial lipídico.
             * Ele retorna a porcentagem de aumento.
             */
            calcularPercentualAumentoHdl(paciente : DadosPacienteService.Paciente): Promise<number>;
        }
}