
describe("CalculoColesterol.CalculadoraColesterol", function() {
  it("deve retornar o valor do LDL colesterol como um número real com até 2 casas decimais entre 0 e 1. ", function() {
    var paciente = new DadosPaciente.Paciente( 
       1,  //referencial Lipídico.
       1,  //sexo Masculino.
			 200,//colesterol total.
			 45, //Hdl.
			 150, //Triglicerides.
       []  //contraindicações.
       );
    var calculadora = new CalculoColesterol.CalculadoraColesterol();
    var r = calculadora.calcularLDL(paciente);
    expect(r).toBeGreaterThan(0);
  });
});

