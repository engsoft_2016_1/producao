/// <reference path="../interfaces/icalculadoraColesterol.ts" />
/// <reference path="../../../../factory/banco/servicesBD/referencialLipidico/businessObject/referencialLipidico.ts" />
/// <reference path="../../../../factory/banco/interfaceBD/IFacadeBD.ts" />
/// <reference path="../../dadosPaciente/businessObject/dadosPaciente.ts" />
/// <reference path="../../referencialColesterol/interfaces/ifactory.ts" />
/// <reference path="../../referencialColesterol/businessObject/referenciais/adultosAcima20anos.ts" />



/**
 * @author Carlos Alberto 
 * @module CalculoColesterolService
 * @version 0.0.0.1 
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo manter as funções que calculam os níveis de LDL, NoHDL e 
 * o percentual de redução LDL.
 * @description Data de criação : 19/03/2017
 * @description Data da última alteração : 19/03/2017
 * @file www/aplication/service/calculadoraColesterol/calculoColesterol.ts
 */
module CalculoColesterolService {

    /**
    * Esta classe é responsável por calcular os valores do colesterol LDL,NoHDL e de redução do LDL.
    */
    export class CalculadoraColesterolService implements IServiceCalculadoraColesterol {

        private referencialColesterol: ReferencialColesterol.IFactory;
        constructor(referencialColesterol: ReferencialColesterol.IFactory) {
            this.referencialColesterol = referencialColesterol;
        }
        calcularLDL(paciente: DadosPacienteService.Paciente): number {
            if (!paciente) {
                throw new Error('CalculadoraColesterolService.calcularLDL : Parâmetro paciente é nulo.');

            }
            let ct: number = paciente.getColesterolTotal();
            let hdl: number = paciente.getHdl();
            let tr: number = paciente.getTriglicerides();
            let result = ct - hdl - (tr / 5);
            return Number(result.toFixed(2));
        }
        calcularNoHDL(paciente: DadosPacienteService.Paciente): number {
            if (!paciente) {
                throw new Error('CalculadoraColesterol : Parâmetro paciente é undefined.');

            }
            let CT = paciente.getColesterolTotal();
            let hdl = paciente.getHdl();
            let result = CT - hdl;
            return Number(result.toFixed(2));
        }
        async calcularPercentualReducaoLDL(paciente: DadosPacienteService.Paciente): Promise<number> {
            if (!paciente) {
                return Promise.reject(new Error('CalculadoraColesterolService.calcularPercentualReducaoLDL : Parâmetro paciente é undefined.'));

            }
            let referencial: ReferencialColesterol.Categoria;

            await this.referencialColesterol.obterReferencial(paciente.getCodReferencial()).then(
                (res: ReferencialColesterol.IReferencial) => {
                    referencial = res.obterLdl().find(
                        (categoria) => {
                            if (categoria.getTipoCategoria() == ReferencialColesterol.TipoCategoria.Desejavel) {
                                return true;
                            }

                        });
                    
                }).catch((reason: Error) => {
                    console.log('CalculadoraColesterolService.calcularPercentualReducaoLDL : obterReferencial : ' + reason.message);
                    return Promise.reject(new Error('CalculadoraColesterolService.calcularPercentualReducaoLDL : obterReferencial : '  + reason.message));
                })


            if (paciente.getLdl() > 0) {
                console.log('CalculadoraColesterolService.calcularPercentualReducaoLDL : ' + 
                   ' Dados do paciente, LDL : ' + paciente.getLdl());
                let result = (paciente.getLdl()*100) / referencial.getMax();
                
                let percentual = result - 100;
                console.log('CalculadoraColesterolService.calcularPercentualReducaoLDL : ' + 
                   ' resultado : ' + result);
                return Promise.resolve(Number(percentual.toFixed(2)));
            }
            return Promise.reject(new Error('O Ldl deve ser maior que zero.'));

        }

        async calcularPercentualReducaoTG(paciente: DadosPacienteService.Paciente): Promise<number> {
            if (!paciente) {
                return Promise.reject(new Error('CalculadoraColesterol : Parâmetro paciente é nulo.'));
            }
            let referencial: any;

            await this.referencialColesterol.obterReferencial(paciente.getCodReferencial()).then(
                (res: ReferencialColesterol.IReferencial) => {
                    referencial = res.obterTriglicerides().find(
                        (categoria, i, o) => {
                            if (categoria.getTipoCategoria() == ReferencialColesterol.TipoCategoria.Desejavel) {
                                return true;
                            }
                            return false;
                        });
                }).catch((reason: Error) => {
                    console.log(reason.message);
                    return Promise.reject(new Error('CalculoColesterol : Erro = ' + reason.message));
                })


            if (paciente.getTriglicerides() > 0) {
                let result = (paciente.getTriglicerides()*100) / referencial.getMax();
              let percentual = result - 100;
                console.log('CalculadoraColesterolService.calcularPercentualReducaoTG : ' + 
                   ' resultado : ' + result);
                return Promise.resolve(Number(percentual.toFixed(2)));
            }
            return Promise.reject(new Error('CalculoColesterol : O Triglicérides deve ser maior que zero.'));

        }

        async calcularPercentualAumentoHdl(paciente: DadosPacienteService.Paciente): Promise<number> {
            if (!paciente) {
                return Promise.reject(new Error('CalculadoraColesterol : Parâmetro paciente é nulo.'));
            }
            let referencial: ReferencialColesterol.Categoria;

            await this.referencialColesterol.obterReferencial(paciente.getCodReferencial()).then(
                (res: ReferencialColesterol.IReferencial) => {
                    referencial = res.obterHdl().find(
                        (categoria, i, o) => {
                            if (categoria.getTipoCategoria() == ReferencialColesterol.TipoCategoria.Desejavel) {
                                return true;
                            }
                            return false;
                        });
                    console.log('Referencial : getHdl = ' + referencial.getMin());
                }).catch((reason: Error) => {
                    console.log(reason.message);
                    return Promise.reject(new Error('CalculadoraColesterol : Error = ' + reason.message));
                })


            if (paciente.getHdl() > 0) {
                let result = (paciente.getHdl() * 100)/referencial.getMin();
               let percentual = 100 - result ;
                console.log('CalculadoraColesterolService.calcularPercentualAumentoHdl : ' + 
                   ' resultado : ' + result);
                return Promise.resolve(Number(percentual.toFixed(2)));
            }
            return Promise.reject(new Error('O Hdl deve ser maior que zero.'));

        }


    }
}
