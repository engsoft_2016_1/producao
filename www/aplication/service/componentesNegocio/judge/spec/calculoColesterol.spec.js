
describe("CalculoColesterol.CalculadoraColesterol", function() {
  it("deve retornar o valor do LDL colesterol como um número Inteiro. ", function() {
    var paciente = new DadosPaciente.Paciente();
    var calculadora = new CalculoColesterol.CalculadoraColesterol();
    expect(calculadora.calcularLDL(paciente)).toBe(0);
  });
});

