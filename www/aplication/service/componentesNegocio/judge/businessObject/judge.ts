/// <reference path="../../prescritorPrincipioativo/businessObject/item.ts" />
/// <reference path="../../dadosPaciente/businessObject/dadosPaciente.ts" />
/// <reference path="../interfaces/iserviceJudge.ts" />
/// <reference path="../interfaces/iserviceJudge.ts" />











/**
 * @author Carlos Alberto 
 * @module JudgeService
 * @version 0.0.0.1 
 * @since 0.0.0.1
 * @description Este módulo é o componente responsável por produzir um resultado que indique
 * qual(ais) são os melhores principios ativos para o tratamento do paciente.
 * @description Data de criação : 23/03/2017
 * @description Data da última alteração : 23/03/2017
 * @file www/aplication/service/componentesNegocio/judge/businessObject/judge.ts
 */
module JudgeService{
     
     /**
     * Esta classe é o serviço que reune as informações do avaliador e calcula um único 
     * valor que representa as melhores características para o tratamento de um 
     * paciente.
     */
    export class Judge implements IServiceJudge {
       
        private regras : IServiceJudge;

        constructor(){}

        julgar(lista: PrescritorPrincipioativo.Item[]): PrescritorPrincipioativo.Item[] {
                     
            lista.sort(
                (a,b)=>{
                    if(a.getPontuacaoReducaoLdl() != b.getPontuacaoReducaoLdl()){
                        return b.getPontuacaoReducaoLdl() - a.getPontuacaoReducaoLdl();
                    }if(a.getPontuacaoReducaoTg() != b.getPontuacaoReducaoTg()){
                            return b.getPontuacaoReducaoTg() - a.getPontuacaoReducaoTg();
                    }
                      return b.getPontuacaoAumentoHdl() - a.getPontuacaoAumentoHdl();
                });
            return lista;
        }
        
        total(param:number[]): number{
            let total:number = 0;
            param.forEach(e =>{
                    total += e;
            });
            return total;
        }
    }
}
