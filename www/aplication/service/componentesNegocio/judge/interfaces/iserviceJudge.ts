/// <reference path="../../dadosPaciente/businessObject/dadosPaciente.ts" />
/// <reference path="../../prescritorPrincipioativo/businessObject/item.ts" />



module JudgeService{
            /**
             * Esta interface define o métodos da classe Judge.
             */
       export interface IServiceJudge{
            /**
             * .
             */
            julgar(lista: PrescritorPrincipioativo.Item[]):PrescritorPrincipioativo.Item[];
             
            
        }
}