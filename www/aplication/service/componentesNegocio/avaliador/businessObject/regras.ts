/// <reference path="../interfaces/iregras.ts" />
/// <reference path="../../calculadoraColesterol/businessObject/calculoColesterol.ts" />




module AvaliadorPrincipioativoService {

    /**
    * Esta classe armazena as regras que serão testadas em cada principioativo.
    */
    export class Regras implements IRegras {

        private melhorPontuacao: number;
        private pontuacaoMediana: number;
        private piorPontuacao: number;

        constructor(melhorPontuacao: number,
            pontuacaoMediana: number,
            piorPontuacao: number) {
            this.melhorPontuacao = melhorPontuacao;
            this.pontuacaoMediana = pontuacaoMediana;
            this.piorPontuacao = piorPontuacao;
        }

        melhorReducaoLdl(lista: PrescritorPrincipioativo.Item[]): PrescritorPrincipioativo.Item[] {

            lista.forEach(element => {
                let a = element.getPrincipioativo().getReducaoLdl();
                let b = Number(
                    (element.getDadosPaciente().getPercentualReducaoLdl() / 100)
                        .toFixed(2)
                );

                if (a == b) {
                    let result = this.melhorPontuacao + (this.melhorPontuacao - element.getPrincipioativo().getReducaoLdl());
                    element.setPontuacaoReducaoLdl(Number(result.toFixed(2)));
                } else if (a > b) {
                    let result = this.pontuacaoMediana + (this.pontuacaoMediana - element.getPrincipioativo().getReducaoLdl());
                    element.setPontuacaoReducaoLdl(Number(result.toFixed(2)));
                } else {
                    let result = this.piorPontuacao + (this.piorPontuacao + element.getPrincipioativo().getReducaoLdl());
                    element.setPontuacaoReducaoLdl(Number(result.toFixed(2)));
                }

            });
            return lista;
        }

        melhorReducaoTg(lista: PrescritorPrincipioativo.Item[]): PrescritorPrincipioativo.Item[] {

            lista.forEach(element => {
                let a = element.getPrincipioativo().getReducaoTriglicerides();
                let b = Number(
                    (element.getDadosPaciente().getPercentualReducaoLdl() / 100)
                        .toFixed(2)
                );

                if (a == b) {
                    let result = (this.melhorPontuacao - 30) + ((this.melhorPontuacao - 30) - element.getPrincipioativo().getReducaoTriglicerides());
                    element.setPontuacaoReducaoTg(Number(result.toFixed(2)));
                } else if (a > b) {
                    let result = (this.pontuacaoMediana - 30) + ((this.pontuacaoMediana - 30) - element.getPrincipioativo().getReducaoTriglicerides());
                    element.setPontuacaoReducaoTg(Number(result.toFixed(2)));
                } else {
                    let result = (this.piorPontuacao - 30) + ((this.piorPontuacao - 30) + element.getPrincipioativo().getReducaoTriglicerides());
                    element.setPontuacaoReducaoTg(Number(result.toFixed(2)));
                }

            });
            return lista;
        }
        melhorAumentoHdl(lista: PrescritorPrincipioativo.Item[]): PrescritorPrincipioativo.Item[] {

            lista.forEach(element => {
                let a = element.getPrincipioativo().getAumentoHDL();
                let b = Number(
                    (element.getDadosPaciente().getPercentualReducaoLdl() / 100)
                        .toFixed(2)
                );

                if (a == b) {
                    let result = (this.melhorPontuacao - 60) + ((this.melhorPontuacao - 60) - element.getPrincipioativo().getAumentoHDL());
                    element.setPontuacaoAumentoHdl(Number(result.toFixed(2)));
                } else if (a > b) {
                    let result = (this.pontuacaoMediana - 60) + ((this.pontuacaoMediana - 60) - element.getPrincipioativo().getAumentoHDL());
                    element.setPontuacaoAumentoHdl(Number(result.toFixed(2)));
                } else {
                    let result = (this.piorPontuacao - 60) + ((this.piorPontuacao - 60) + element.getPrincipioativo().getAumentoHDL());
                    element.setPontuacaoAumentoHdl(Number(result.toFixed(2)));
                }

            });
            return lista;
        }
        estatinasContraindicadas(lista: PrescritorPrincipioativo.Item[]): PrescritorPrincipioativo.Item[] {
            lista.forEach(e => {
                if (e.getContraindicacoes().length > 0) {
                    e.setPontuacaoReducaoLdl(0);
                    e.setPontuacaoReducaoTg(0);
                    e.setPontuacaoAumentoHdl(0);
                }
            })

            return lista;
        }


    }
}