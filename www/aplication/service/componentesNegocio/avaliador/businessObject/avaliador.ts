/// <reference path="../../prescritorPrincipioativo/businessObject/item.ts" />
/// <reference path="../../dadosPaciente/businessObject/dadosPaciente.ts" />
/// <reference path="../interfaces/iserviceAvaliador.ts" />
/// <reference path="../interfaces/iregras.ts" />
/// <reference path="../businessObject/regras.ts" />










/**
 * @author Carlos Alberto 
 * @module AvaliadorPrincipioativoService
 * @version 0.0.0.1 
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo de definir os pesos e atribuir notas as estatinas 
 * de acordo com os dados do paciente.
 * @description Data de criação : 20/03/2017
 * @description Data da última alteração : 20/03/2017
 * @file www/aplication/service/componentesNegocio/avaliador/businessObject/avaliador.ts
 */
module AvaliadorPrincipioativoService{
     
     /**
     * Esta classe é responsável por atribuir os pesos e dar pontuações aos principiosativos 
     * que mais se adequam ao problema do paciente.
     */
    export class AvaliadorService implements IServiceAvaliador {
        
        private regras : IRegras;

        constructor(){
            this.regras = new AvaliadorPrincipioativoService.Regras(100,90,80);
        }

        avaliar(lista: PrescritorPrincipioativo.Item[]): PrescritorPrincipioativo.Item[]{
                let list : PrescritorPrincipioativo.Item[] = this.regras.melhorReducaoLdl(lista);
                list = this.regras.melhorReducaoTg(list);
                list = this.regras.melhorAumentoHdl(list);
                list = this.regras.estatinasContraindicadas(list);
                return list;
        }


    }
}
