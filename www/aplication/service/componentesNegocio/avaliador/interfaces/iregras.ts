/// <reference path="../../prescritorPrincipioativo/businessObject/item.ts" />


  module AvaliadorPrincipioativoService{
              



/**
 * Esta interface reune todas as regras que avaliarão os princípios ativos.
 */                
            export interface IRegras{

                melhorReducaoLdl(lista:PrescritorPrincipioativo.Item[]):PrescritorPrincipioativo.Item[];
                melhorReducaoTg(lista:PrescritorPrincipioativo.Item[]):PrescritorPrincipioativo.Item[];
                melhorAumentoHdl(lista:PrescritorPrincipioativo.Item[]):PrescritorPrincipioativo.Item[];
                estatinasContraindicadas(lista:PrescritorPrincipioativo.Item[]):PrescritorPrincipioativo.Item[];
            }
  }