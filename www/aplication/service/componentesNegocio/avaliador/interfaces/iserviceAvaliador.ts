/// <reference path="../../dadosPaciente/businessObject/dadosPaciente.ts" />
/// <reference path="../../prescritorPrincipioativo/businessObject/item.ts" />



module AvaliadorPrincipioativoService{

       export interface IServiceAvaliador{
            /**
             * Esta função avalia uma lista de principiosativos.
             */
            avaliar(lista: PrescritorPrincipioativo.Item[]):PrescritorPrincipioativo.Item[];
             
            
        }
}