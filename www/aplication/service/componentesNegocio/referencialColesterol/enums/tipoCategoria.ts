module ReferencialColesterol{

    export const enum TipoCategoria {
        Otimo,      // = 0
        Desejavel,  // = 1
        Limitrofe,  // = 2
        Alto,       // = 3
        MuitoAlto,  // = 4
        Baixo       // = 5
    }
}