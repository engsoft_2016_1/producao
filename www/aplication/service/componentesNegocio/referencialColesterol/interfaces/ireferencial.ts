


module ReferencialColesterol{
        /**
         * Esta interface serve para definir o tipo de retorno
         * da ReferencialColesterolFactory.
         */
       export interface IReferencial{
         obterColesterolTotal() : ReferencialColesterol.Categoria[];
         definirColesterol(v : ReferencialColesterol.Categoria[]):void;
         obterTriglicerides() : ReferencialColesterol.Categoria[];
         definirTriglicerides(v : ReferencialColesterol.Categoria[]):void;
         obterHdl() : ReferencialColesterol.Categoria[];
         definirHdl(v : ReferencialColesterol.Categoria[]):void;
         obterLdl() : ReferencialColesterol.Categoria[];
         definirLdl(v : ReferencialColesterol.Categoria[]):void;
        }
}