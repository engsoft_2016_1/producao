module ReferencialColesterol{


/**
 * Esta interface define os métodos a serem implementados pela
 * classe NivelColesterolFactory.
 */
    export interface IFactory{

        obterReferencial(key: number):Promise<ReferencialColesterol.IReferencial>;
    }
}