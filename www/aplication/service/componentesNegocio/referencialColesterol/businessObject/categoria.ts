/// <reference path="../enums/tipoCategoria.ts" />


module ReferencialColesterol{

    /**
     * Esta classe armazena o nome de uma categoria do referencial
     * lipídico e seu intervalo.
     */
   export class Categoria {
        
        private tipoCategoria : ReferencialColesterol.TipoCategoria; 
        private nome : string;
        private min : number;
        private max : number;
        
        constructor(tipoCategoria:ReferencialColesterol.TipoCategoria,
         nome: string, min: number, max:number) {
            this.setTipoCategoria(tipoCategoria);
            this.setNome(nome);
            this.setMin(min);
            this.setMax(max);
        }

        public getNome() : string {
            return this.nome;
        }
        public setNome(v : string) {
            this.nome = v;
        }
        public getMin() : number {
            return this.min;
        }
        public setMin(v : number) {
            this.min = v;
        }
        public getMax() : number {
            return this.max;
        }
        public setMax(v : number) {
            this.max = v;
        }
        public getTipoCategoria() : ReferencialColesterol.TipoCategoria {
            return this.tipoCategoria;
        }
        public setTipoCategoria(v : ReferencialColesterol.TipoCategoria) {
            this.tipoCategoria = v;
        }
    }
}