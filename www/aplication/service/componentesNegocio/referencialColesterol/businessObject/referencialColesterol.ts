/// <reference path="../interfaces/ireferencial.ts" />
/// <reference path="../interfaces/ifactory.ts" />
/// <reference path="../businessObject/referenciais/adultosAcima20anos.ts" />
/// <reference path="../../../../../../node_modules/typescript/lib/lib.es6.d.ts" />
/// <reference path="../businessObject/referenciais/criancas2Ate20anos.ts" />





/**
 * @author Carlos Alberto 
 * @module ReferencialColesterol
 * @version 0.0.0.1 
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo de armazenar os valores de cada categoria dos referenciais lipídicos 
 * e exibir cada nível de colesterol do paciente.
 * @description Data de criação : 27/03/2017
 * @description Data da última alteração : 27/03/2017
 * @file www/aplication/service/componentesNegocio/niveisColesterol/businessObject/niveisColesterol.ts
 */
module ReferencialColesterol{

     /**
     * Esta classe é responsável por disponibilizar os dados dos referênciais lipidicos
     * e seus níveis de colesterol.
     */
    export class ReferencialColesterolFactory implements IFactory{

       private referenciaisMap : Map<number,ReferencialColesterol.IReferencial>;

       constructor(){
           this.referenciaisMap = new Map<number, ReferencialColesterol.IReferencial>();
           
           this.referenciaisMap.set(ReferencialColesterol.AdultosApartir20Anos.keyReferencial,
           new ReferencialColesterol.AdultosApartir20Anos());

           this.referenciaisMap.set(ReferencialColesterol.Criancas2Ate19Anos.keyReferencial,
           new ReferencialColesterol.Criancas2Ate19Anos());
       }

       async obterReferencial(key: number):Promise<ReferencialColesterol.IReferencial>{
            return new Promise<ReferencialColesterol.IReferencial>(
                (resolve, reject) =>{
                    let a = this.referenciaisMap.get(key);
                    if(a != undefined){
                        resolve(a);
                    }
                    reject(new Error('Esse objeto não foi encontrado.'));
            });
       }
    }
}
