/// <reference path="../categoria.ts" />
/// <reference path="../../../referencialColesterol/enums/tipoCategoria.ts" />
/// <reference path="../../interfaces/ireferencial.ts" />


module ReferencialColesterol{

    /**
     * Esta classe representa o refrencial de adultos com faixa etária 
     * acima dos 20 anos.
     */
    export class AdultosApartir20Anos implements ReferencialColesterol.IReferencial{
        static readonly keyReferencial : number = 1; 
        private colesterolTotal: ReferencialColesterol.Categoria[];
        private triglicerides : ReferencialColesterol.Categoria[];
        private hdl : ReferencialColesterol.Categoria[];
        private ldl : ReferencialColesterol.Categoria[];
        private noHdl : ReferencialColesterol.Categoria[];
       
        

        constructor() {
            this.colesterolTotal = [
                new ReferencialColesterol.Categoria(TipoCategoria.Desejavel,'Desejável',Number.MIN_VALUE,199),
                new ReferencialColesterol.Categoria(TipoCategoria.Limitrofe,'Limítrofe',200,239),
                new ReferencialColesterol.Categoria(TipoCategoria.Alto,'Alto',240,Number.MAX_VALUE)
            ];

            this.triglicerides = [
                new ReferencialColesterol.Categoria(TipoCategoria.Desejavel,'Desejável',Number.MIN_VALUE,149),
                new ReferencialColesterol.Categoria(TipoCategoria.Limitrofe,'Limítrofe',150,199),
                new ReferencialColesterol.Categoria(TipoCategoria.Alto,'Alto',200,499),
                new ReferencialColesterol.Categoria(TipoCategoria.MuitoAlto,'Muito Alto',500,Number.MAX_VALUE)
            ];
            
            this.hdl = [
                new ReferencialColesterol.Categoria(TipoCategoria.Desejavel,'Desejável',61,Number.MAX_VALUE),
                new ReferencialColesterol.Categoria(TipoCategoria.Limitrofe,'Limítrofe',40,60),
                new ReferencialColesterol.Categoria(TipoCategoria.Baixo,'Baixo',Number.MIN_VALUE,40)
                
            ];
            this.ldl = [
                new ReferencialColesterol.Categoria(TipoCategoria.Otimo,'Ótimo',Number.MIN_VALUE,99),
                new ReferencialColesterol.Categoria(TipoCategoria.Desejavel,'Desejável',100,129),
                new ReferencialColesterol.Categoria(TipoCategoria.Limitrofe,'Limitrofe',130,159),
                new ReferencialColesterol.Categoria(TipoCategoria.Alto,'Alto',160,189),
                new ReferencialColesterol.Categoria(TipoCategoria.MuitoAlto,'Muito Alto',190,Number.MAX_VALUE)
            ];
            this.noHdl = [
                new ReferencialColesterol.Categoria(TipoCategoria.Otimo,'Ótimo',Number.MIN_VALUE,129),
                new ReferencialColesterol.Categoria(TipoCategoria.Desejavel,'Desejável',130,159),
                new ReferencialColesterol.Categoria(TipoCategoria.Alto,'Alto',160,189),
                new ReferencialColesterol.Categoria(TipoCategoria.MuitoAlto,'Muito Alto',190,Number.MAX_VALUE)
            ];
        }
        
        public obterColesterolTotal() : ReferencialColesterol.Categoria[] {
            return this.colesterolTotal;
        }
        
        public definirColesterol(v : ReferencialColesterol.Categoria[]) {
            this.colesterolTotal = v;
        }

        public obterTriglicerides() : ReferencialColesterol.Categoria[] {
            return this.triglicerides;
        }
        public definirTriglicerides(v : ReferencialColesterol.Categoria[]) {
            this.triglicerides = v;
        }
        public obterHdl() : ReferencialColesterol.Categoria[] {
            return this.hdl;
        }
        public definirHdl(v : ReferencialColesterol.Categoria[]) {
            this.hdl = v;
        }
        public obterLdl() : ReferencialColesterol.Categoria[] {
            return this.ldl;
        }
        public definirLdl(v : ReferencialColesterol.Categoria[]) {
            this.ldl = v;
        }
         public obterNoHdl() : ReferencialColesterol.Categoria[] {
            return this.noHdl;
        }
        public definirNoHdl(v : ReferencialColesterol.Categoria[]) {
            this.noHdl = v;
        }
        
    }

    
}