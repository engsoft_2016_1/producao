/// <reference path="../categoria.ts" />
/// <reference path="../../../referencialColesterol/enums/tipoCategoria.ts" />
/// <reference path="../../interfaces/ireferencial.ts" />


module ReferencialColesterol{

    /**
     * Esta classe representa o refrencial de crianças com faixa etária 
     * dos 2 a 20 anos.
     */
    export class Criancas2Ate19Anos implements ReferencialColesterol.IReferencial{
        static readonly keyReferencial : number = 2; 
        private colesterolTotal: ReferencialColesterol.Categoria[];
        private triglicerides : ReferencialColesterol.Categoria[];
        private hdl : ReferencialColesterol.Categoria[];
        private ldl : ReferencialColesterol.Categoria[];
        

        constructor() {
            this.colesterolTotal = [
                new ReferencialColesterol.Categoria(TipoCategoria.Desejavel,'Desejável',Number.MIN_VALUE,149),
                new ReferencialColesterol.Categoria(TipoCategoria.Limitrofe,'Limítrofe',150,169),
                new ReferencialColesterol.Categoria(TipoCategoria.Alto,'Alto',170,Number.MAX_VALUE)
            ];

            this.triglicerides = [
                new ReferencialColesterol.Categoria(TipoCategoria.Desejavel,'Desejável',Number.MIN_VALUE,99),
                new ReferencialColesterol.Categoria(TipoCategoria.Limitrofe,'Limítrofe',100,129),
                new ReferencialColesterol.Categoria(TipoCategoria.Alto,'Alto',130,Number.MAX_VALUE)
            ];
            
            this.hdl = [
                new ReferencialColesterol.Categoria(TipoCategoria.Desejavel,'Desejável',45,Number.MAX_VALUE),
                new ReferencialColesterol.Categoria(TipoCategoria.Baixo,'Baixo',Number.MIN_VALUE,39)
                
            ];
            this.ldl = [
                new ReferencialColesterol.Categoria(TipoCategoria.Desejavel,'Desejável',Number.MIN_VALUE,99),
                new ReferencialColesterol.Categoria(TipoCategoria.Limitrofe,'Limitrofe',100,129),
                new ReferencialColesterol.Categoria(TipoCategoria.Alto,'Alto',130,Number.MAX_VALUE)
            ];
           
        }
        
        public obterColesterolTotal() : ReferencialColesterol.Categoria[] {
            return this.colesterolTotal;
        }
        
        public definirColesterol(v : ReferencialColesterol.Categoria[]) {
            this.colesterolTotal = v;
        }

        public obterTriglicerides() : ReferencialColesterol.Categoria[] {
            return this.triglicerides;
        }
        public definirTriglicerides(v : ReferencialColesterol.Categoria[]) {
            this.triglicerides = v;
        }
        public obterHdl() : ReferencialColesterol.Categoria[] {
            return this.hdl;
        }
        public definirHdl(v : ReferencialColesterol.Categoria[]) {
            this.hdl = v;
        }
        public obterLdl() : ReferencialColesterol.Categoria[] {
            return this.ldl;
        }
        public definirLdl(v : ReferencialColesterol.Categoria[]) {
            this.ldl = v;
        }
         
        
    }

    
}