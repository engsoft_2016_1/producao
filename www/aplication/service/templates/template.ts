
/**
 * @author Carlos Alberto 
 * @module MyModule
 * @version 0.0.0.1 
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo ser um template de 
 * documentação de código Typescript.
 * @description Data de criação : 02/03/2017
 * @description Data da última alteração : 02/03/2017
 * @file www/aplication/service/templates/template.js
 */
module MyModule{

/**
 * Esta Classe que representa um exemplo de documentação.
 * @export
 */
    export class Example implements IExample {

        /**
         * Exemplo de uma variável. 
         * @type {String}
         * @private
         */
        private name : String;
         /**    
         * Exemplo de documentação de um construtor. 
         * @param parameters {any}  - aqui parametros do construtor da Classe 
         * são descritos e o tipo é colocado entre parênteses.
         */ 
        constructor (parameters: String) {
             
               this.name = parameters;
        }   
        
        /**
         * Exemplo de documentação de uma operação/metodo/função.
         * @param params {any} - descrição do parâmetro e seu tipo.
         */
        public  setName(params:String){
            this.name = params;
        }

        /**
         * Exemplo de documentação de uma operação/metodo/function com notação lâmbda
         * @returns {String}
         */
        public  getName(){
                return 'text';
        }
    
        showName(): void {
            throw new Error('Method not implemented.');
        }
    }

    /**
     * Interface de exemplo
     * @export
     */
    export interface IExample{
        showName():void;
    }

    /**
     * Esta Classe que representa um exemplo de implementação
     * de uma interface.
     */   
    class Teste2 implements IExample {
        showName(): void {
            throw new Error('Method not implemented.');
        }


    }

}