/// <reference path="../servicesBD/configuracao/dao/configuracoesDao.ts" />
/// <reference path="../servicesBD/configuracao/businessObject/configuracao.ts" />
/// <reference path="../interfaceBD/itable.ts" />


module StorageDAO{

    /**
     * Responsável por checar se é a primeira vez que o banco é aberto.
     */
   export class Check {

        private config: Configuracoes;

        public bancoInstalado(versao:Number){
            if(localStorage.getItem('bancoInstalado') == '1'){
                    return true;
             }
             localStorage.setItem('bancoInstalado', '1');
             localStorage.setItem('versaoBanco',String(versao))
             return false;
        }

        public novaVersaoBanco(versao:Number){
             if(Number(localStorage.getItem('versaoBanco')) < versao ){
                 localStorage.setItem('versaoBanco',String(versao))
                    return true;
             }
             return false;
        }

    }
}