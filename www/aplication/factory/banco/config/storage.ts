/// <reference path='references.ts'/>

/**
 * @author Carlos Alberto 
 * @module StorageDAO
 * @version 0.0.0.1 
 * @since 0.0.0.1
 * @description Este módulo tem por objetivo construir e gerenciar as dependencias do banco de dados bem como
 * adaptá-lo para a aplicação.
 * @description Data de criação : 19/03/2017
 * @description Data da última alteração : 24/03/2017
 * @file www/aplication/factory/banco/config/storage.ts
 */
module StorageDAO {


		declare let db:any;
		declare let window:any;
	
		/**
		 * Classe responsável por acessar os dados do banco de dados.
		 */
        export class DAO implements IFacadeBD {
           

			private banco: any = null; 
			private check: Check = null;
			private versaoBanco : Number = 16;

			constructor(){ 
						
				    document.addEventListener('deviceready', () => {
					
						this.banco = window.sqlitePlugin.openDatabase({name: 'demo.db',key: 'your-password-here', location: 'default'},
						function(){
							console.log("banco aberto.");
								/**
							 * Evento que mostra que o banco já criou as tabelas e está pronto para 
							 * ser usado.
							 * @event bancoPronto
							 */
							let event = new CustomEvent("bancoPronto", {});

							setTimeout(function() {
									// Dispatch/Trigger/Fire the event
									document.dispatchEvent(event);
	
							}, 3000);
							
						},
						function(){
							console.log('O banco não foi aberto.');
						});
							let tables : ITable[] =  [
									new ConfiguracoesTable(),
									new PrincipioativoTable(),
									new LinkTable(),
									new EfeitosColateraisTable(),
									new PrincipioativoContraindicacoesTable(),
									new ContraindicacaoTable(),
									new SexoTable(),
									new FaixaEtariaTable(),
									new ReferencialLipidicoTable()
								];

						this.check = new Check();
						if(!this.check.bancoInstalado(this.versaoBanco)){
								console.log('StorageDAO : App aberto pela primeira vez!');
								
								tables.forEach(element => {
									element.createTable(this.banco);
									element.populateTable(this.banco);

								});
								
						}

						if(this.check.novaVersaoBanco(this.versaoBanco)){
								tables.forEach(element => {
									element.dropTable(this.banco);
									element.createTable(this.banco);
									element.populateTable(this.banco);
								});
						}
					});

							
						
			}//fim do constructor			

			async obterConfiguracoes(): Promise<Configuracoes> {
				return await new ConfiguracoesDAO(this.banco).obterConfiguracoes();
			}
			async obterContraindicacoes(sexo: number,faixaEtaria:number): Promise<Contraindicacao[]> {
				return await new ContraindicacaoDAO(this.banco).obterContraindicacoes(sexo,faixaEtaria);
			}
			async obterEfeitosColaterais(): Promise<EfeitoColateral> {
				return await new EfeitosColateraisDAO(this.banco).obterEfeitosColaterais();
			}
			async obterFaixasEtarias(): Promise<FaixaEtaria[]> {
				return await new FaixaEtariaDAO(this.banco).obterFaixasEtarias();
			}
			async obterLinks(): Promise<Link> {
				return await new LinkDAO(this.banco).obterLinks(); 
			}
			async obterLinksDoPrincipioativo(idPrincipioativo: Number): Promise<Link[]> {
				return await new LinkDAO(this.banco).obterLinksDoPrincipioativo(idPrincipioativo);
			}
			async obterPrincipiosativos(): Promise<Principioativo[]> {
				return await new PrincipioativoDAO(this.banco).obterPrincipiosativos();
			}
			async obterReferencialLipidico(): Promise<ReferencialLipidico[]> {
				return await new ReferencialLipidicoDAO(this.banco).obterReferencialLipidico();
			}
			async pesquisarPrincipiosativosPorReducao(percentualReducao: number): Promise<Principioativo[]> {
				return await new PrincipioativoDAO(this.banco).pesquisarPrincipiosativosPorReducao(percentualReducao);
			}
			async pesquisarReferencialLipidico(idFaixaEtaria: number): Promise<ReferencialLipidico> {
				return await new ReferencialLipidicoDAO(this.banco).pesquisarReferencialLipidico(idFaixaEtaria);
			}
			async obterContraindicacaoPrincipioativo(codPrincipioativo: number,codContraindicacaoPaciente:number[]): Promise<Contraindicacao[]> {
                return await new ContraindicacaoDAO(this.banco).obterContraindicacaoPrincipioativo(codPrincipioativo,codContraindicacaoPaciente);
            }
		
		}

	

	

    }

