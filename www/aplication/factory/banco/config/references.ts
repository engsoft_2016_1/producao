
//configuracões
/// <reference path='../servicesBD/configuracao/interface/iserviceConfiguracoes.ts'/>
/// <reference path='../servicesBD/configuracao/dao/configuracoesDao.ts'/>
/// <reference path='../servicesBD/configuracao/businessObject/configuracao.ts'/>
/// <reference path='../servicesBD/configuracao/tableDescritor/configuracaoTable.ts'/>


//Principioativo
/// <reference path='../servicesBD/principioativo/interface/iservicePrincipioativo.ts'/>
/// <reference path='../servicesBD/principioativo/dao/principioativoDao.ts'/>
/// <reference path='../servicesBD/principioativo/businessObject/principioativo.ts'/>
/// <reference path='../servicesBD/principioativo/tableDescritor/principioativoTable.ts'/>

//Link
/// <reference path='../servicesBD/link/interface/iserviceLink.ts'/>
/// <reference path='../servicesBD/link/dao/linkDao.ts'/>
/// <reference path='../servicesBD/link/businessObject/link.ts'/>
/// <reference path='../servicesBD/link/tableDescritor/linkTable.ts'/>

//EfeitoColateral
/// <reference path='../servicesBD/efeitosColaterais/interface/iserviceEfeitosColaterais.ts'/>
/// <reference path='../servicesBD/efeitosColaterais/dao/efeitosColateraisDao.ts'/>
/// <reference path='../servicesBD/efeitosColaterais/businessObject/efeitoColateral.ts'/>
/// <reference path='../servicesBD/efeitosColaterais/tableDescritor/efeitosColateaisTable.ts'/>


//Principioativo_contraindicacoes
/// <reference path='../servicesBD/principioativo_contraindicacoes/tableDescritor/principioativo_contraindicacoesTable.ts'/>

//Contraindicacoes
/// <reference path='../servicesBD/contraindicacao/interface/iserviceContraindicacao.ts'/>
/// <reference path='../servicesBD/contraindicacao/dao/contraindicacaoDao.ts'/>
/// <reference path='../servicesBD/contraindicacao/businessObject/contraindicacao.ts'/>
/// <reference path='../servicesBD/contraindicacao/tableDescritor/contraindicacaoTable.ts'/>

//Sexo
/// <reference path='../servicesBD/sexo/tableDescritor/sexoTable.ts'/>

//FaixaEtaria
/// <reference path='../servicesBD/faixaEtaria/interface/iserviceFaixaEtaria.ts'/>
/// <reference path='../servicesBD/faixaEtaria/dao/faixaEtariaDao.ts'/>
/// <reference path='../servicesBD/faixaEtaria/businessObject/faixaEtaria.ts'/>
/// <reference path='../servicesBD/faixaEtaria/tableDescritor/faixaEtariaTable.ts'/>

//Referencial Lipidico
/// <reference path='../servicesBD/referencialLipidico/interface/iserviceReferencialLipidico.ts'/>
/// <reference path='../servicesBD/referencialLipidico/dao/referencialLipidicoDao.ts'/>
/// <reference path='../servicesBD/referencialLipidico/businessObject/referencialLipidico.ts'/>
/// <reference path='../servicesBD/referencialLipidico/tableDescritor/referencialLipidicoTable.ts'/>

//Check
/// <reference path="../tools/checkBD.ts" />

//Itable
/// <reference path="../interfaceBD/itable.ts" />
/// <reference path="../interfaceBD/IFacadeBD.ts" />



