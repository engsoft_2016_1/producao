
module StorageDAO{


/**
 * Interface para as classes que representam as tabelas 
 * do banco, elas ajudam na inicialização do banco.
 */
    export interface ITable{
            createTable(banco:any):void;
            dropTable(banco:any):void;
            populateTable(banco:any):void;
        }

}