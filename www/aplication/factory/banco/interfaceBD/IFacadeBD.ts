/// <reference path="../config/references.ts" />


module StorageDAO{

    export interface IFacadeBD 
    extends    IServiceConfiguracoesDAO,
               IServiceContraindicacaoDAO,
               IServiceEfeitosColateraisDAO,
               IServiceFaixaEtariaDAO,
               IServiceLinkDAO,
               IServicePrincipioativoDAO,
               IServiceReferencialLipidicoDAO
    {}

}