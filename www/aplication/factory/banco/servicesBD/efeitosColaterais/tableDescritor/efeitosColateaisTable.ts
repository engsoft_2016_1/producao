/// <reference path="../../../interfaceBD/itable.ts" />

module StorageDAO{
    
     /**
      * Classe responsável por manter as informações da tabela de 
      * EfeitosColaterais para inicializar o banco de dados.
      */
        export class EfeitosColateraisTable implements ITable {

                constructor(){

                }
                public populateTable(banco:any){
                    banco.transaction((tx:any)=>{
                            tx.executeSql('INSERT INTO EfeitoColateral VALUES (?,?,?)',[0,'',0]);
                        
                        }, (error:any)=>{
                            console.log('EfeitoColateral : Transaction ERROR: ' + error.message);
                        }, ()=>{
                            console.log('EfeitoColateral : Populated table OK');
                        });	;						
                }

                public dropTable(banco:any): void {
                banco.transaction((tx:any) => {
                    tx.executeSql('DROP TABLE IF EXISTS EfeitoColateral;');
                                    
                        }, (error:any)=>{
                            console.log('EfeitoColateral : Delete Configuracoes ERROR: ' + error.message);
                        }, ()=>{
                            console.log('EfeitoColateral : Delete table Configuracoes OK');
                        });
                } 

                public createTable(banco: any): void {
                    banco.transaction((tx:any) => {
                    tx.executeSql("CREATE TABLE IF NOT EXISTS EfeitoColateral ( "
                            + " id INTEGER PRIMARY KEY AUTOINCREMENT," 
                            + " nome TEXT NOT NULL,"
                            + " idPrincipioAtivo INTEGER NOT NULL," 
                            + " FOREIGN KEY (idPrincipioAtivo) REFERENCES Principioativo(id))");
                                    
                        }, (error:any)=>{
                            console.log('EfeitoColateral : Create EfeitoColateral ERROR: ' + error.message);
                        }, ()=>{
                            console.log('EfeitoColateral : Create table EfeitoColateral OK');
                        });                       
                }
        }
}