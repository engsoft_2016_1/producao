/// <reference path="../interface/iserviceEfeitosColaterais.ts" />

module StorageDAO{
    
    /**
	 * Classe responsável por disponiblilizar o acesso aos dados do banco
	 * referentes aos efeitos colaterais da aplicação.
	 */
	export class EfeitosColateraisDAO implements IServiceEfeitosColateraisDAO {
		private banco : any;
		constructor(banco:any){
			this.banco = banco;
		}
		obterEfeitosColaterais(): Promise<EfeitoColateral> {
					return new Promise<EfeitoColateral>((resolve:any, reject:any) =>{
							this.banco.transaction(function(tx:any) {
									tx.executeSql('SELECT * FROM EfeitoColateral', [],
									function(tx:any, rs:any) {
										console.log('EfeitosColateraisDAO : SELECT ok');
										
										if(rs.rows.length > 0){
										resolve(new EfeitoColateral(
											Number(rs.rows.item(0).id),
											String(rs.rows.item(0).nome),
											Number(rs.rows.item(0).idPrincipioAtivo)
										));
										}else{reject(new Error('Não foi encontrados Efeitos Colaterais.'))}
									}
									, function(tx:any, error:any) : any{
										console.log('EfeitosColateraisDAO : SELECT error: ' + error.message);
										reject(error);
									});
								});
							});	
				}

	}
}