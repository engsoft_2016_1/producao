module StorageDAO{

/**
 * Classe responsável por agrupar os dados do banco para objeto de negócio EfeitoColateral. 
 * @param id @type Number
 * @param nome @type String
 * @param idPrincipioativo  @type Number
 */

export  class EfeitoColateral{
			private id:Number;
			private nome:String;
			private idPrincipioativo:Number;

			constructor(
				id:Number = 0,
				nome:String = '',
				idPrincipioativo:Number = 0
			){
				this.setId(id);
				this.setNome(nome);
				this.setIdPrincipioativo(idPrincipioativo);
			}
			
			public getId() : Number {
				return this.id;
			}
			public setId(v : Number) {
				this.id = v;
			}
			public getIdPrincipioativo() : Number {
				return this.idPrincipioativo;
			}
			public setIdPrincipioativo(v : Number) {
				this.idPrincipioativo = v;
			}
			public getNome() : 	String {
				return this.nome;
			}
			public setNome(v : String) {
				this.nome = v;
			}
	}
}