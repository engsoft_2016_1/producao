/// <reference path="../businessObject/efeitoColateral.ts" />

module StorageDAO{	
    
	/**
	 * Interface para a classe IServiceEfeitosColateraisDAO.
	 */
	
    export interface IServiceEfeitosColateraisDAO{
				obterEfeitosColaterais(): Promise<EfeitoColateral>;	
			}
}