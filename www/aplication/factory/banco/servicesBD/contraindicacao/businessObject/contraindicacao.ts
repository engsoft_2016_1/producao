module StorageDAO{

/**
 * Classe responsável por agrupar os dados do banco para objeto de negócio Contraindicacoes. 
 * @param id @type Number
 * @param nome @type string
 * @param generoAlvo  @type Number
 * @param faixaEtaria  @type Number
 */

export  class Contraindicacao{
			private id:number = 0;
			private nome:string = '';
			private codGeneroAlvo:number = 0;
			private codFaixaEtaria : number = 0;
			private descricao : string = '';


			constructor(
				id:number = 0,
				nome:string = '',
				generoAlvo: number = 0,
				faixaEtaria : number = 0,
				descricao : string = ''
			){
				this.setId(id);
				this.setNome(nome);
				this.setGeneroAlvo(generoAlvo);
				this.setFaixaEtaria(faixaEtaria);
				this.setDescricao(descricao);
			}
			
			public getId() : number {
				return this.id;
			}
			public setId(v : number) {
				this.id = v;
			}
			public getNome() : 	string {
				return this.nome;
			}
			public setNome(v : string) {
				this.nome = v;
			}
			public getGeneroAlvo() : 	number {
				return this.codGeneroAlvo;
			}
			public setGeneroAlvo(v : number) {
				this.codGeneroAlvo = v;
			}
			public getFaixaEtaria() : number {
				return this.codFaixaEtaria;
			}
			public setFaixaEtaria(v : number) {
				this.codFaixaEtaria= v;
			}
			public getDescricao() : string {
				return this.descricao;
			}
			public setDescricao(v : string) {
				this.descricao = v;
			}
	}
}