/// <reference path="../../../interfaceBD/itable.ts" />
/// <reference path="../../../../../service/componentesNegocio/referencialColesterol/businessObject/referencialColesterol.ts" />


module StorageDAO{
    
     /**
      * Classe responsável por manter as informações da tabela de 
      * Contraindicacao para inicializar o banco de dados.
      */
        export class ContraindicacaoTable implements ITable {

                constructor(){

                }
                public populateTable(banco:any){
                    banco.transaction((tx:any)=>{   
                        let campos = '(id,nome,idGeneroAlvo,idFaixaEtaria,descricao)';
                            tx.executeSql('INSERT INTO Contraindicacao '+campos+' VALUES (?,?,?,?,?)',[1,'Hepatopatia',2,3,'']);
                            tx.executeSql('INSERT INTO Contraindicacao '+campos+' VALUES (?,?,?,?,?)',[2,'Miopatia',2,3,'']);
                            tx.executeSql('INSERT INTO Contraindicacao '+campos+' VALUES (?,?,?,?,?)',[3,'Entre 2 e 7 anos.',2,ReferencialColesterol.Criancas2Ate19Anos.keyReferencial,'']);
                            tx.executeSql('INSERT INTO Contraindicacao '+campos+' VALUES (?,?,?,?,?)',[4,'Grávida ou Lactente',0,ReferencialColesterol.AdultosApartir20Anos.keyReferencial,'']);
                            tx.executeSql('INSERT INTO Contraindicacao '+campos+' VALUES (?,?,?,?,?)',[5,'Mulher em idade fértil',0,3,'Que não faz o uso de anticoncepcional.']);
                            tx.executeSql('INSERT INTO Contraindicacao '+campos+' VALUES (?,?,?,?,?)',[6,'Problemas na tireóide.',2,3,'']);
                            tx.executeSql('INSERT INTO Contraindicacao '+campos+' VALUES (?,?,?,?,?)',[7,'Diabético',2,3,'']);
                            tx.executeSql('INSERT INTO Contraindicacao '+campos+' VALUES (?,?,?,?,?)',[8,'Problemas renais.',2,3,'']);
                            tx.executeSql('INSERT INTO Contraindicacao '+campos+' VALUES (?,?,?,?,?)',[9,'Alcoolismo',2,3,'']);
                            tx.executeSql('INSERT INTO Contraindicacao '+campos+' VALUES (?,?,?,?,?)',[10,'Entre 8 e 18 anos.',2,ReferencialColesterol.Criancas2Ate19Anos.keyReferencial,'']);
                            tx.executeSql('INSERT INTO Contraindicacao '+campos+' VALUES (?,?,?,?,?)',[11,'Idoso.',2,ReferencialColesterol.AdultosApartir20Anos.keyReferencial,'Acima de 65 anos.']);
                             tx.executeSql('INSERT INTO Contraindicacao '+campos+' VALUES (?,?,?,?,?)',[12,'Polifarmácia.',2,3,'Para quem já utiliza muitos medicamentos.']);

                        }, (error:any)=>{
                            console.log('ContraindicacaoTable : Transaction ERROR: ' + error.message);
                        }, ()=>{
                            console.log('ContraindicacaoTable : Populated table OK');
                        });	;						
                }

                public dropTable(banco:any): void {
                banco.transaction((tx:any) => {
                    tx.executeSql('DROP TABLE IF EXISTS Contraindicacao;');
                                    
                        }, (error:any)=>{
                            console.log('ContraindicacaoTable : Delete Configuracoes ERROR: ' + error.message);
                        }, ()=>{
                            console.log('ContraindicacaoTable : Delete table Configuracoes OK');
                        });
                } 

                public createTable(banco: any): void {
                    banco.transaction((tx:any) => {
                    tx.executeSql("CREATE TABLE IF NOT EXISTS Contraindicacao ( "
                            + " id INTEGER PRIMARY KEY," 
                            + " nome TEXT NOT NULL,"
                            + " idGeneroAlvo INTEGER NOT NULL,"
                            + " idFaixaEtaria INTEGER NOT NULL,"
                            + " descricao TEXT NOT NULL," 
                            + " FOREIGN KEY (idGeneroAlvo) REFERENCES Sexo(id),"
                            + " FOREIGN KEY (idFaixaEtaria) REFERENCES FaixaEtaria(id))");
                                    
                        }, (error:any)=>{
                            console.log('ContraindicacaoTable : Create Contraindicacao ERROR: ' + error.message);
                        }, ()=>{
                            console.log('ContraindicacaoTable : Create table ContraindicacaO OK');
                        });                       
                }
        }
}