/// <reference path="../businessObject/contraindicacao.ts" />

module StorageDAO{	
    
	/**
	 * Interface para a classe IServiceEfeitosColateraisDAO.
	 */
	
    export interface IServiceContraindicacaoDAO{
				obterContraindicacoes(sexo: number, faixaEtaria: number): Promise<Contraindicacao[]>;
				obterContraindicacaoPrincipioativo(codPrincipioativo: number, contraindicacoesPaciente:number[]): Promise<Contraindicacao[]>;
			}
}