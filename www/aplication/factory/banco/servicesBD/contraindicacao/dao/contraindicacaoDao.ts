/// <reference path="../interface/iserviceContraindicacao.ts" />

module StorageDAO{
    
    /**
	 * Classe responsável por disponiblilizar o acesso aos dados do banco
	 * referentes as contraindicações dos principiosativos.
	 */
	export class ContraindicacaoDAO implements IServiceContraindicacaoDAO {
	

		private banco : any;
		constructor(banco:any){
			this.banco = banco;
		}
		obterContraindicacaoPrincipioativo(codPrincipioativo: number,codContraindicacaoPaciente: number[]): Promise<Contraindicacao[]> {
			return new Promise<Contraindicacao[]>((resolve:any, reject:any) =>{
				console.log('estatina: '+codPrincipioativo);
				console.log('contraindicação: '+codContraindicacaoPaciente);
							 this.banco.transaction(function(tx:any) {
									tx.executeSql('SELECT '
												+ ' c.id AS id,'
												+ ' c.nome AS nome,'
												+ ' c.idGeneroAlvo AS idGeneroAlvo,'
												+ ' c.idFaixaEtaria AS idFaixaEtaria,'
												+ ' c.descricao AS descricao'
												+ ' FROM PrincipioativoPossuiContraindicacao AS p '
												+ ' JOIN Contraindicacao AS c ON (p.contraindicacaoId = c.id)'
												+ ' WHERE  p.contraindicacaoId IN ('+codContraindicacaoPaciente.toString()+') AND p.principioativoId = '+codPrincipioativo+'', [],
											
											function(tx:any, rs:any) {
												console.log('Contraindicacao : SELECT ok');

												
											if(rs.rows.length > 0){
													console.log('pelo menos um registro foi encontrado: '+ rs.rows.length);
																let list:Contraindicacao[] = [];
																for(let i=0; i < rs.rows.length;i++){									
																	list[i] = new Contraindicacao(
																				Number(rs.rows.item(i).id),
																				String(rs.rows.item(i).nome),
																				Number(rs.rows.item(i).idGeneroAlvo),
																				Number(rs.rows.item(i).idFaixaEtaria),
																				String(rs.rows.item(i).descricao)
																			);	
																	}
														resolve(list);			
												}
											else{reject(new Error('Não foram encontradas contraindicações.'))}}
											, function(tx:any, error:any) : any{
												console.log('ContraindicacaoDAO : SELECT error: ' + error.message);
												reject(new Error(error.message));
											});
										});//fim transaction
							});
		}
		obterContraindicacoes(sexo: number, faixaEtaria: number): Promise<Contraindicacao[]> {
					return new Promise<Contraindicacao[]>((resolve:any, reject:any) =>{
							this.banco.transaction(function(tx:any) {
									tx.executeSql('SELECT' 
										+ ' r.id AS id,'
										+ ' r.idSexo, '
										+ ' f.id AS idFaixaEtaria, '
										+ ' r.nome AS nome, '
										+ ' r.descricao AS descricao,'
										+ ' r.idGeneroAlvo AS idGeneroAlvo,'
										+ ' r.idFaixaEtaria AS idFaixaEtaria'
										+ ' FROM (SELECT c.id,'
										+ ' s.id AS idSexo,'
										+ ' c.nome, '
										+ ' c.idGeneroAlvo,'
										+ ' c.idFaixaEtaria, '
										+ ' c.descricao'
										+ ' FROM Contraindicacao AS c '
										+ ' JOIN Sexo AS s ON(c.idGeneroAlvo = s.id)) AS r'
										+ ' JOIN FaixaEtaria AS f ON(r.idFaixaEtaria = f.id)'
										+ ' WHERE (r.idSexo = ? OR r.idSexo = 2) AND (f.id = ? OR f.id = 3);', [sexo, faixaEtaria],
									function(tx:any, rs:any) {
										console.log('Contraindicacao : SELECT ok');
										
										let list: Contraindicacao[] = [];
									if(rs.rows.length > 0){
											console.log('pelo menos um registro foi encontrado: '+ rs.rows.length);
												for(let i = 0; i < rs.rows.length; i++){
														list[i] = new Contraindicacao(
															Number(rs.rows.item(i).id),
															String(rs.rows.item(i).nome),
															Number(rs.rows.item(i).idGeneroAlvo),
															Number(rs.rows.item(i).idFaixaEtaria),
															String(rs.rows.item(i).descricao)
														);
													}
													resolve(list);
									}else{reject(new Error('Não foi encontradas Contraindicações.'))}
									}
									, function(tx:any, error:any) : any{
										console.log('ContraindicacaoDAO : SELECT error: ' + error.message);
										reject(new Error(error.message));
									});
								});
							});	
				}

	}
}