/// <reference path="../businessObject/referencialLipidico.ts" />

module StorageDAO{	
    
	/**
	 * Interface para a classe ReferencialLipidicoDAO.
	 */
	
    export interface IServiceReferencialLipidicoDAO{
				obterReferencialLipidico(): Promise<ReferencialLipidico[]>;
				pesquisarReferencialLipidico(idFaixaEtaria:number): Promise<ReferencialLipidico>;	
			}
}