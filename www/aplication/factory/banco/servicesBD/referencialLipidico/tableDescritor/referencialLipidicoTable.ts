/// <reference path="../../../interfaceBD/itable.ts" />

module StorageDAO{
    
     /**
      * Classe responsável por manter as informações da tabela de 
      * ReferencialLipidico para inicializar o banco de dados.
      */
        export class ReferencialLipidicoTable implements ITable {

                constructor(){

                }
                public populateTable(banco:any){
                    banco.transaction((tx:any)=>{
                            tx.executeSql('INSERT INTO ReferencialLipidico (colesterolTotal, hdl, triglicerides, ldl, noHdl)  VALUES (?,?,?,?,?)',[150,45,100,100,0]);
                            tx.executeSql('INSERT INTO ReferencialLipidico (colesterolTotal, hdl, triglicerides, ldl, noHdl)  VALUES (?,?,?,?,?)',[200,60,150,129,159]);
                        }, (error:any)=>{
                            console.log('ReferencialLipidicoTable : Transaction ERROR: ' + error.message);
                        }, ()=>{
                            console.log('ReferencialLipidicoTable : Populated table OK');
                        });							
                }

                public dropTable(banco:any): void {
                banco.transaction((tx:any) => {
                    tx.executeSql('DROP TABLE IF EXISTS ReferencialLipidico;');
                                    
                        }, (error:any)=>{
                            console.log('ReferencialLipidicoTable : Delete ReferencialLipidico ERROR: ' + error.message);
                        }, ()=>{
                            console.log('ReferencialLipidicoTable : Delete table ReferencialLipidico OK');
                        });
                } 

                public createTable(banco: any): void {
                    banco.transaction((tx:any) => {
                    tx.executeSql("CREATE TABLE IF NOT EXISTS ReferencialLipidico ( "
                            + "id INTEGER PRIMARY KEY AUTOINCREMENT," 
                            + "colesterolTotal INTEGER NOT NULL,"
                            + "hdl INTEGER NOT NULL,"
                            + "triglicerides INTEGER NOT NULL,"
                            + "ldl INTEGER NOT NULL,"
                            + "noHdl INTEGER NOT NULL)"
                                    
                    )}, (error:any)=>{
                            console.log('ReferencialLipidicoTable : Create ReferencialLipidico ERROR: ' + error.message);
                        }, ()=>{
                            console.log('ReferencialLipidicoTable : Create table ReferencialLipidico OK');
                        });                       
                }
        }
}