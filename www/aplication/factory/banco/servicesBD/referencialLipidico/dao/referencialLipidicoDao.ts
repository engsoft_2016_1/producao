/// <reference path="../interface/iserviceReferencialLipidico.ts" />

module StorageDAO{
    
    /**
	 * Classe responsável por disponiblilizar o acesso aos dados do banco
	 * referentes as Referencial Lipidico que se adapta ao paciente.
	 */
	export class ReferencialLipidicoDAO implements IServiceReferencialLipidicoDAO {
		
		private banco:any;
		constructor(banco:any){
			this.banco = banco;
		}
		pesquisarReferencialLipidico(idFaixaEtaria: number): Promise<ReferencialLipidico> {
			return new Promise<ReferencialLipidico>((resolve:any, reject:any) =>{
							this.banco.transaction(function(tx:any) {
									tx.executeSql('SELECT r.id AS id ,'
									+ ' r.colesterolTotal AS colesterolTotal,'
									+ ' r.hdl AS hdl,'
									+ ' r.triglicerides AS triglicerides,'
									+ ' r.ldl AS ldl,'
									+ ' r.noHdl AS noHdl'
									+ ' FROM FaixaEtaria AS f'
									+ ' JOIN ReferencialLipidico AS r ON (f.idReferencialLipidico = r.id)'
									+ ' WHERE f.id = ? ', [idFaixaEtaria],
									function(tx:any, rs:any) {
										console.log('ReferencialLipidicoDAO : SELECT ok');
										
										if(rs.rows.length > 0){
											console.log('pelo menos um resultado foi encontrado');
												resolve(new ReferencialLipidico(
													Number(rs.rows.item(0).id),
													Number(rs.rows.item(0).colesterolTotal),
													Number(rs.rows.item(0).hdl),
													Number(rs.rows.item(0).triglicerides),
													Number(rs.rows.item(0).ldl),
													Number(rs.rows.item(0).noHdl),
												));
										}else{reject(new Error('Não foi encontrados Referenciais Lipidicos.'))}
									
									}
									, function(tx:any, error:any) : any{
										console.log('ReferencialLipidico : SELECT error: ' + error.message);
										reject(error);
									});
								});
							});
		}
		obterReferencialLipidico():Promise<ReferencialLipidico[]> {
					return new Promise<ReferencialLipidico[]>((resolve:any, reject:any) =>{
							this.banco.transaction(function(tx:any) {
									tx.executeSql('SELECT * FROM ReferencialLipidico', [],
									function(tx:any, rs:any) {
										console.log('ReferencialLipidicoDAO : SELECT ok');
										if(rs.rows.length > 0){
											let list:ReferencialLipidico[] = [];

										for(let i = 0; i < rs.rows.length; i++){
												list[i] = new ReferencialLipidico(
													Number(rs.rows.item(i).id),
													Number(rs.rows.item(i).colesterolTotal),
													Number(rs.rows.item(i).hdl),
													Number(rs.rows.item(i).triglicerides),
													Number(rs.rows.item(i).ldl),
													Number(rs.rows.item(i).noHdl),
												);
											}
											resolve(list);
										}else{reject(new Error('Não foi encontrados ReferencialLipidico.'))}
									
									}
									, function(tx:any, error:any) : any{
										console.log('ReferencialLipidico : SELECT error: ' + error.message);
										reject(error);
									});
								});
							});	
				}

	}
}