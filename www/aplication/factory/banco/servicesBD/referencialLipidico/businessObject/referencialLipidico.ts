module StorageDAO{

/**
 * Classe responsável por agrupar os dados do banco para objeto de negócio Referêncial Lipídico.
 * @param id @type number 
 * @param colesterolTotal @type number
 * @param hdl @type number 
 * @param triglicerides @type number 
 * @param ldl @type number
 * @param noHdl  @type number 
 */

export  class ReferencialLipidico{
			private id:number;
			private colesterolTotal:number;
			private hdl:number;
			private triglicerides:number;
			private ldl:number;
			private noHdl:number;

			constructor(
					id:number = 0,
					colesterolTotal:number = 0,
					hdl:number = 0,
					triglicerides:number = 0,
					ldl:number = 0,
					noHdl:number = 0
			){
				this.setId(id);
				this.setColesterolTotal(colesterolTotal);
				this.setHdl(hdl);
				this.setTriglicerides(triglicerides);
				this.setLdl(ldl);
				this.setNoHdl(noHdl);
			}
			
			
			public getId() : number {
				return this.id;
			}
			public setId(v : number) {
				this.id = v;
			}
			
			public getColesterolTotal() : number {
				return this.colesterolTotal;
			}
			public setColesterolTotal(v : number) {
				this.colesterolTotal = v;
			}
			public getHdl() : number {
				return this.hdl;
			}
		    public setHdl(v : number) {
				this.hdl = v;
			}
			public getTriglicerides() : number {
				return this.triglicerides;
			}
			public setTriglicerides(v : number) {
				this.triglicerides = v;
			}
			public getLdl() : number {
				return this.ldl;
			}
			public setLdl(v : number) {
				this.ldl = v;
			}
			public getNoHdl():number {
				return this.noHdl;	
			}
			public setNoHdl(v :number) {
				this.noHdl= v;
			}
			
			
			
	}
}