/// <reference path="../businessObject/faixaEtaria.ts" />

module StorageDAO{	
    
	/**
	 * Interface para a classe IServiceEfeitosFaixaEtariaDAO.
	 */
	
    export interface IServiceFaixaEtariaDAO{
				obterFaixasEtarias(): Promise<FaixaEtaria[]>;	
			}
}