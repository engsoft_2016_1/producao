module StorageDAO{

/**
 * Classe responsável por agrupar os dados do banco para objeto de negócio para FaixaEtaria. 
 * @param id @type number
 * @param intervalo @type string
 * @param referencialLipidico  @type number
 */

export  class FaixaEtaria{
			private id:number;
			private intervalo:string;
			

			constructor(
				id:number = 0,
				intervalo:string = '',
				
			){
				this.setId(id);
				this.setIntervalo(intervalo);
				
			}
			
			public getId() : number {
				return this.id;
			}
			public setId(v : number) {
				this.id = v;
			}
			public getIntervalo() : 	string {
				return this.intervalo;
			}
			public setIntervalo(v : string) {
				this.intervalo = v;
			}
			
	}
}