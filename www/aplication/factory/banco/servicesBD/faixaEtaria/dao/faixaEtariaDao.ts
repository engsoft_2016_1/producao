/// <reference path="../interface/iserviceFaixaEtaria.ts" />

module StorageDAO{
    
    /**
	 * Classe responsável por disponiblilizar o acesso aos dados do banco
	 * referentes as Faixas etárias dos pacientes.
	 */
	export class FaixaEtariaDAO implements IServiceFaixaEtariaDAO {
		private banco : any;
		constructor(banco:any){
			this.banco = banco;
		}
		obterFaixasEtarias(): Promise<FaixaEtaria[]> {
					return new Promise<FaixaEtaria[]>((resolve:any, reject:any) =>{
							this.banco.transaction(function(tx:any) {
									tx.executeSql('SELECT * FROM FaixaEtaria', [],
									function(tx:any, rs:any) {
										console.log('FaixaEtariaDAO : SELECT ok');
										
										if(rs.rows.length > 0){
										let list:FaixaEtaria[] = [];
										
										for(let i=0; i < rs.rows.length; i++ ){
												list[i] = new FaixaEtaria(
													Number(rs.rows.item(0).id),
													String(rs.rows.item(0).intervalo),
												);
										}
										resolve(list);
										}else{reject(new Error('Não foi encontradas Faixa Etária.'))}
									}
									, function(tx:any, error:any) : any{
										console.log('FaixaEtariaDAO : SELECT error: ' + error.message);
										reject(new Error(error.message));
									});
								});
							});	
				}

	}
}