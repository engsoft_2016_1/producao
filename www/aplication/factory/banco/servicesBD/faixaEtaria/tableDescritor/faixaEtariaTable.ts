/// <reference path="../../../interfaceBD/itable.ts" />
/// <reference path="../../../../../service/componentesNegocio/referencialColesterol/businessObject/referenciais/adultosAcima20anos.ts" />
/// <reference path="../../../../../service/componentesNegocio/referencialColesterol/businessObject/referenciais/criancas2Ate20anos.ts" />


module StorageDAO{
    
     /**
      * Classe responsável por manter as informações da tabela de 
      * FaixaEtaria para inicializar o banco de dados.
      */
        export class FaixaEtariaTable implements ITable {

                constructor(){

                }
                public populateTable(banco:any){
                    banco.transaction((tx:any)=>{
                            tx.executeSql('INSERT INTO FaixaEtaria (id,intervalo) VALUES (?,?)',[ReferencialColesterol.Criancas2Ate19Anos.keyReferencial,'2 a 19 anos']);
                            tx.executeSql('INSERT INTO FaixaEtaria (id,intervalo) VALUES (?,?)',[ReferencialColesterol.AdultosApartir20Anos.keyReferencial,'20 a 65 anos']);
                            tx.executeSql('INSERT INTO FaixaEtaria (id,intervalo) VALUES (?,?)',[3,'Todas as idades.']);
                            
                        
                        }, (error:any)=>{
                            console.log('FaixaEtariaTable : Transaction ERROR: ' + error.message);
                        }, ()=>{
                            console.log('FaixaEtariaTable : Populated table OK');
                        });	;						
                }

                public dropTable(banco:any): void {
                banco.transaction((tx:any) => {
                    tx.executeSql('DROP TABLE IF EXISTS FaixaEtaria;');
                                    
                        }, (error:any)=>{
                            console.log('FaixaEtariaTable : Delete Configuracoes ERROR: ' + error.message);
                        }, ()=>{
                            console.log('FaixaEtariaTable : Delete table FaixaEtaria OK');
                        });
                } 

                public createTable(banco: any): void {
                    banco.transaction((tx:any) => {
                    tx.executeSql("CREATE TABLE IF NOT EXISTS FaixaEtaria ( "
                            + "id INTEGER PRIMARY KEY," 
                            + "intervalo TEXT NOT NULL)");
                                    
                        }, (error:any)=>{
                            console.log('FaixaEtariaTable : Create FaixaEtaria ERROR: ' + error.message);
                        }, ()=>{
                            console.log('FaixaEtariaTable : Create table FaixaEtaria OK');
                        });                       
                }
        }
}