module StorageDAO{

/**
 * Classe responsável por agrupar os dados do banco para objeto de negócio Configuracoes. 
 * @param versaoSoftware @type Number
 * @param versaoBanco @type Number
 * @param exibirAutoajuda  @type Number
 */

export  class Configuracoes{
			private versaoSoftware:Number;
			private versaoBanco:Number;
			private exibirAutoajuda:Number;

			constructor(
				versaoSoftware:Number = 0,
				versaoBanco:Number = 0,
				exibirAutoajuda:Number = 0
			){
				this.setVersaoBanco(versaoBanco);
				this.setVersaoSoftware(versaoSoftware);
				this.setExibirAutoajuda(exibirAutoajuda);
			}
			
			public getVersaoSoftware() : Number {
				return this.versaoSoftware;
			}
			public setVersaoSoftware(v : Number) {
				this.versaoSoftware = v;
			}
			public getVersaoBanco() : Number {
				return this.versaoBanco;
			}
			public setVersaoBanco(v : Number) {
				this.versaoBanco = v;
			}
			public getExibirAutoajuda() : Number {
				return this.exibirAutoajuda;
			}
			public setExibirAutoajuda(v : Number) {
				this.exibirAutoajuda = v;
			}
	}
}