/// <reference path="../businessObject/configuracao.ts" />

module StorageDAO{	
    
	/**
	 * Interface para a classe ConfiguracoesDAO.
	 */
	
    export interface IServiceConfiguracoesDAO{
				obterConfiguracoes():/**@ignore*/ Promise<Configuracoes>;	
			}
}