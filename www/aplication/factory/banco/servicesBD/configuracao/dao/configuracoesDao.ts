/// <reference path="../interface/iserviceConfiguracoes.ts" />

module StorageDAO{
    
    /**
	 * Classe responsável por disponiblilizar o acesso aos dados do banco
	 * referentes as configurações do aplicativo.
	 */
    export class ConfiguracoesDAO implements IServiceConfiguracoesDAO {
				private banco : any = null;

				constructor(banco:any){
						this.banco = banco;
				}
				obterConfiguracoes():Promise<Configuracoes> {
					return new Promise<Configuracoes>((resolve:any, reject:any) =>{
							this.banco.transaction(function(tx:any) {
									tx.executeSql('SELECT * FROM Configuracoes', [],
									function(tx:any, rs:any) {
										console.log('ConfiguracoesDAO : SELECT ok');
										
										if(rs.rows.length > 0){
										resolve(new Configuracoes(
											Number(rs.rows.item(0).versaoSoftware),
											Number(rs.rows.item(0).versaoBanco),
											Number(rs.rows.item(0).exibirAutoajuda)
										));
										}else{reject(new Error('Não foi encontradas configurações.'))}
									}
									, function(tx:any, error:any) : any{
										console.log('ConfiguracoesDAO : SELECT error: ' + error.message);
										reject(error);
									});
								});
							});	
				}

	}
}