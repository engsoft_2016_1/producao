/// <reference path="../../../interfaceBD/itable.ts" />

module StorageDAO{
    
     /**
      * Classe responsável por manter as informações da tabela de 
      * Configuracoes para inicializar o banco de dados.
      */
        export class ConfiguracoesTable implements ITable {

                constructor(){

                }
                public populateTable(banco:any){
                    banco.transaction((tx:any)=>{
                            tx.executeSql('INSERT INTO Configuracoes VALUES (?,?,?)',[1,1,1]);
                        
                        }, (error:any)=>{
                            console.log('ConfiguracoesTable : Transaction ERROR: ' + error.message);
                        }, ()=>{
                            console.log('ConfiguracoesTable : Populated table OK');
                        });	;						
                }

                public dropTable(banco:any): void {
                banco.transaction((tx:any) => {
                    tx.executeSql('DROP TABLE IF EXISTS Configuracoes;');
                                    
                        }, (error:any)=>{
                            console.log('ConfiguracoesTable : Delete Configuracoes ERROR: ' + error.message);
                        }, ()=>{
                            console.log('ConfiguracoesTable : Delete table Configuracoes OK');
                        });
                } 

                public createTable(banco: any): void {
                    banco.transaction((tx:any) => {
                    tx.executeSql("CREATE TABLE IF NOT EXISTS Configuracoes ( "
                            + "versaoSoftware INTEGER NOT NULL," 
                            + "versaoBanco INTEGER NOT NULL,"
                            + "exibirAutoajuda INTEGER NOT NULL)");
                                    
                        }, (error:any)=>{
                            console.log('ConfiguracoesTable : Create Configuracoes ERROR: ' + error.message);
                        }, ()=>{
                            console.log('ConfiguracoesTable : Create database Configuracoes OK');
                        });                       
                }
        }
}