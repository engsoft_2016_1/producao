/// <reference path="../businessObject/link.ts" />

module StorageDAO{	
    
	/**
	 * Interface para a classe LinkDAO.
	 */
	
    export interface IServiceLinkDAO{
				obterLinks():Promise<Link>;
				obterLinksDoPrincipioativo(idPrincipioativo: Number):Promise<Link[]>;		
			}
}