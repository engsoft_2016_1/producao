/// <reference path="../interface/iserviceLink.ts" />

module StorageDAO{
    
    /**
	 * Classe responsável por disponiblilizar o acesso aos dados do banco
	 * referentes as configurações do aplicativo.
	 */
	export class LinkDAO implements IServiceLinkDAO {
		private banco : any;
		constructor(banco:any){
			this.banco = banco;
		}

		obterLinksDoPrincipioativo(idPrincipioativo: Number): Promise<Link[]> {
			return new Promise<Link[]>((resolve:any,reject:any)=>{
				let links: Link[] = [];
				this.banco.transaction(function(tx:any) {
									tx.executeSql(
										'SELECT * FROM Link'
									   +' WHERE Link.idPrincipioativo = ?;', [idPrincipioativo],
									function(tx:any, rs:any) {
										console.log('LinkDAO : SELECT ok');
										if(rs.rows.length > 0){
											for(let i = 0; i < rs.rows.length; i++){
												links[i] = new Link(
															Number(rs.rows.item(i).id),
															Number(rs.rows.item(i).idPrincipioativo),
															String(rs.rows.item(i).nome),
															String(rs.rows.item(i).enderecoHttp));			
											}
											resolve(links);
										}else{reject(new Error('Não foi encontrados Links.'))}
										
									}
									, function(tx:any, error:any) : any{
										console.log('LinkDAO : SELECT error: ' + error.message);
										reject(error);
									});
								});	
			});
		}

		obterLinks():Promise<Link> {
					return new Promise<Link>((resolve:any, reject:any) =>{
							this.banco.transaction(function(tx:any) {
									tx.executeSql('SELECT * FROM Link', [],
									function(tx:any, rs:any) {
										console.log('LinkDAO : SELECT ok');
										
									for(let i = 0; i < rs.rows.length; i++){
												resolve(new Link(
													Number(rs.rows.item(i).id),
													Number(rs.rows.item(i).idPrincipioativo),
													String(rs.rows.item(i).nome),
													String(rs.rows.item(i).enderecoHttp)
												));
											}
									}
									, function(tx:any, error:any) : any{
										console.log('LinkDAO : SELECT error: ' + error.message);
										reject(error);
									});
								});
							});	
				}

	}
}