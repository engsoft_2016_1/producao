/// <reference path="../../../interfaceBD/itable.ts" />

module StorageDAO{
    
     /**
      * Classe responsável por manter as informações da tabela de 
      * Link para inicializar o banco de dados.
      */
        export class LinkTable implements ITable {

                constructor(){

                }
                public populateTable(banco:any){
                    banco.transaction((tx:any)=>{
                            tx.executeSql('INSERT INTO Link VALUES (?,?,?,?)',[0,0,'','']);
                        
                        }, (error:any)=>{
                            console.log('LinkTable : population ERROR: ' + error.message);
                        }, ()=>{
                            console.log('LinkTable : Populated database OK');
                        });						
                }

                public dropTable(banco:any): void {
                banco.transaction((tx:any) => {
                    tx.executeSql('DROP TABLE IF EXISTS Link;');
                                    
                        }, (error:any)=>{
                            console.log('LinkTable : Delete Link ERROR: ' + error.message);
                        }, ()=>{
                            console.log('LinkTable : Delete table Link OK');
                        });
                } 

                public createTable(banco: any): void {
                    banco.transaction((tx:any) => {
                    tx.executeSql("CREATE TABLE IF NOT EXISTS Link ( "
                            + "id INTEGER PRIMARY KEY AUTOINCREMENT," 
                            + "idPrincipioativo INTEGER NOT NULL,"
                            + "nome TEXT NOT NULL,"
                            + "enderecoHttp TEXT NOT NULL,"
                            + "FOREIGN KEY (idPrincipioativo) REFERENCES Link(id))");
                                    
                        }, (error:any)=>{
                            console.log('LinkTable : Create Link ERROR: ' + error.message);
                        }, ()=>{
                            console.log('LinkTable : Create table Link OK');
                        });                       
                }
        }
}