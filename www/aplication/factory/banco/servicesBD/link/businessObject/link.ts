module StorageDAO{

/**
 * Classe responsável por agrupar os dados do banco para objeto de negócio Link. 
 * @param id @type Number
 * @param idPrincipioativo @type Number
 * @param nome @type String
 * @param enderecoHttp @type String
 */

export  class Link{
			private id : Number;
			private idPrincipioativo : Number;
			private nome : String;
			private enderecoHttp:String;

			constructor(
					id : Number = 0,
					idPrincipioativo : Number = 0,
			 		nome : String = '',
			 		enderecoHttp:String = ''
			){
				this.setId(id);
				this.setIdPrincipioativo(idPrincipioativo);
				this.setNome(nome);
				this.setEnderecoHttp(enderecoHttp);
			}
			
			public getId() : Number {
				return this.id;
			}
			public setId(v : Number) {
				this.id = v;
			}
			public getIdPrincipioativo() : Number {
				return this.idPrincipioativo;
			}
			public setIdPrincipioativo(v : Number) {
				this.idPrincipioativo = v;
			}
			public getNome() : String {
				return this.nome;
			}
			public setNome(v : String) {
				this.nome = v;
			}
			public getEnderecoHttp() : String {
				return this.enderecoHttp;
			}
			public setEnderecoHttp(v : String) {
				this.enderecoHttp = v;
			}
	}
}