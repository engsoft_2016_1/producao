/// <reference path="../interface/iservicePrincipioativo.ts" />

module StorageDAO{
    
    /**
	 * Classe responsável por disponiblilizar o acesso aos dados do banco
	 * referentes as configurações do aplicativo.
	 */
	export class PrincipioativoDAO implements IServicePrincipioativoDAO {
	

				private banco : any;
				constructor(banco:any){
					this.banco = banco;
				}

				
				///**lembre-se que esta operação não retorna os principiosativos que estão acima do indice de redução.*/
				pesquisarPrincipiosativosPorReducao(percentualReducao: number): Promise<Principioativo[]> {
					return new Promise<Principioativo[]>((resolve:any, reject:any) =>{
							this.banco.transaction(function(tx:any) {
									tx.executeSql('SELECT * FROM Principioativo AS p' 
									+' WHERE p.reducaoLdl <= ? '
									+' ORDER BY p.reducaoLdl DESC;', [percentualReducao],
									function(tx:any, rs:any) {
										console.log('Principioativo : SELECT ok');
										let list:Principioativo[] = [];

										if(rs.rows.length > 0){
											console.log('pelo menos um registro foi encontrado: '+ rs.rows.length);
												for(let i = 0; i < rs.rows.length; i++){
														list[i] = new Principioativo(
															Number(rs.rows.item(i).id),
															String(rs.rows.item(i).nome),
															String(rs.rows.item(i).dosagem),
															Number(rs.rows.item(i).reducaoLdl),
															Number(rs.rows.item(i).reducaoTriglicerides),
															Number(rs.rows.item(i).aumentoHDL),
															String(rs.rows.item(i).meiaVidaPlasmatica),
															Number(rs.rows.item(i).penetracaoSistemaNervoso),
															Number(rs.rows.item(i).excrecaoRenalDoseAbsorvida),
															Number(rs.rows.item(i).custoEstimado),
															String(rs.rows.item(i).bula)

														);
													}
											resolve(list);
										}else{reject(new Error('Não foi encontrados Principiosativos.'))}
									
									}, function(tx:any, error:any) : any{
										console.log('Principioativo : SELECT error: ' + error.message);
										reject(error);
									});
								});
							});
				}

				obterPrincipiosativos():Promise<Principioativo[]> {
					return new Promise<Principioativo[]>((resolve:any, reject:any) =>{
							this.banco.transaction(function(tx:any) {
									tx.executeSql('SELECT * FROM Principioativo AS p ORDER BY p.reducaoLdl DESC', [],
									function(tx:any, rs:any) {
										console.log('Principioativo : SELECT ok');
										
										let list: Principioativo[] = [];
									if(rs.rows.length > 0){
											console.log('pelo menos um registro foi encontrado: '+ rs.rows.length);
												for(let i = 0; i < rs.rows.length; i++){
														list[i] = new Principioativo(
															Number(rs.rows.item(i).id),
															String(rs.rows.item(i).nome),
															String(rs.rows.item(i).dosagem),
															Number(rs.rows.item(i).reducaoLdl),
															Number(rs.rows.item(i).reducaoTriglicerides),
															Number(rs.rows.item(i).aumentoHDL),
															String(rs.rows.item(i).meiaVidaPlasmatica),
															Number(rs.rows.item(i).penetracaoSistemaNervoso),
															Number(rs.rows.item(i).excrecaoRenalDoseAbsorvida),
															Number(rs.rows.item(i).custoEstimado),
															String(rs.rows.item(i).bula)

														);
													}
											resolve(list);
										}else{reject(new Error('Não foi encontrados Principiosativos.'))}
									
									}, function(tx:any, error:any) : any{
										console.log('Principioativo : SELECT error: ' + error.message);
										reject(error);
									});
								});
							});	
				}

	}
}