module StorageDAO{

/**
 * Classe responsável por agrupar os dados do banco para objeto de negócio Principioativo.
 * @param id @type number 
 * @param nome @type String
 * @param dosagem @type String 
 * @param reducaoLdl @type number 
 * @param reducaoTriglicerides @type number
 * @param aumentoHDL  @type number 
 * @param meiaVidaPlasmatica @type String
 * @param penetracaoSistemaNervoso  @type number 
 * @param excrecaoRenalDoseAbsorvida @type number
 * @param bula @type String
 */

export  class Principioativo{
			private id:number = 0;
			private nome:String = '';
			private dosagem:String = '';
			private reducaoLdl:number = 0;
			private reducaoTriglicerides:number = 0;
			private aumentoHDL:number = 0;
			private meiaVidaPlasmatica:String = '';
			private penetracaoSistemaNervoso:number = 0;
			private excrecaoRenalDoseAbsorvida:number = 0;
			private custoEstimado : number = 0;
			private bula:String = '';

			constructor(
					id:number = 0,
					nome:String = '',
					dosagem:String = '',
					reducaoLdl:number = 0,
					reducaoTriglicerides:number = 0,
					aumentoHDL:number = 0,
					meiaVidaPlasmatica:String = '',
					penetracaoSistemaNervoso:number = 0,
					excrecaoRenalDoseAbsorvida:number = 0,
					custoEstimado = 0,
					bula:String = ''
			){
				this.setId(id);
				this.setNome(nome);
				this.setDosagem(dosagem);
				this.setReducaoLdl(reducaoLdl);
				this.setReducaoTriglicerides(reducaoTriglicerides);
				this.setAumentoHDL(aumentoHDL);
				this.setMeiaVidaPlasmatica(meiaVidaPlasmatica);
				this.setPenetracaoSistemaNervoso(penetracaoSistemaNervoso);
				this.setExcrecaoRenalDoseAbsorvida(excrecaoRenalDoseAbsorvida);
				this.setBula(bula);
				this.setCustoEstimado(custoEstimado);
			}
			
			
			public getId() : number {
				return this.id;
			}
			public setId(v : number) {
				this.id = v;
			}
			
			public getNome() : String {
				return this.nome;
			}
			public setNome(v : String) {
				this.nome = v;
			}
			public getDosagem() : String {
				return this.dosagem;
			}
		    public setDosagem(v : String) {
				this.dosagem = v;
			}
			public getReducaoLdl() : number {
				return this.reducaoLdl;
			}
			public setReducaoLdl(v : number) {
				this.reducaoLdl = v;
			}
			public getReducaoTriglicerides() : number {
				return this.reducaoTriglicerides;
			}
			public setReducaoTriglicerides(v : number) {
				this.reducaoTriglicerides = v;
			}
			public getAumentoHDL():number {
				return this.aumentoHDL;	
			}
			public setAumentoHDL(v :number) {
				this.aumentoHDL= v;
			}
			public getMeiaVidaPlasmatica() : String {
				return this.meiaVidaPlasmatica;
			}
			public setMeiaVidaPlasmatica(v : String) {
				this.meiaVidaPlasmatica = v;
			}
			public getPenetracaoSistemaNervoso() : number {
				return this.penetracaoSistemaNervoso;
			}
			public setPenetracaoSistemaNervoso(v : number) {
				this.penetracaoSistemaNervoso = v;
			}
			public getExcrecaoRenalDoseAbsorvida() : number {
				return this.excrecaoRenalDoseAbsorvida;
			}
			public setExcrecaoRenalDoseAbsorvida(v : number) {
				this.excrecaoRenalDoseAbsorvida = v;
			}
			public getCustoEstimado() : number {
				return this.custoEstimado;
			}
			public setCustoEstimado(v : number) {
				this.custoEstimado = v;
			}
			public getBula() : String {
				return this.bula;
			}
			public setBula(v : String) {
				this.bula = v;
			}
			
			
			
			
			
			
	}
}