/// <reference path="../businessObject/principioativo.ts" />



module StorageDAO{	
    
	/**
	 * Interface para a classe ConfiguracoesDAO.
	 */
	
    export interface IServicePrincipioativoDAO{
				obterPrincipiosativos(): Promise<Principioativo[]>;
				pesquisarPrincipiosativosPorReducao(percentualReducao:number): Promise<Principioativo[]>;	
			}
}