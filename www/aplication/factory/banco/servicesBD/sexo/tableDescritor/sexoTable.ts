/// <reference path="../../../interfaceBD/itable.ts" />

module StorageDAO{
    
     /**
      * Classe responsável por manter as informações da tabela de 
      * Contraindicacao para inicializar o banco de dados.
      */
        export class SexoTable implements ITable {

                constructor(){

                }
                public populateTable(banco:any){
                    banco.transaction((tx:any)=>{
                            tx.executeSql('INSERT INTO Sexo VALUES (?,?)',[0,'Feminino']);
                            tx.executeSql('INSERT INTO Sexo VALUES (?,?)',[1,'Masculino']);
                            tx.executeSql('INSERT INTO Sexo VALUES (?,?)',[2,'Ambos']);
                        
                        }, (error:any)=>{
                            console.log('SexoTable : Transaction ERROR: ' + error.message);
                        }, ()=>{
                            console.log('SexoTable : Populated table OK');
                        });	;						
                }

                public dropTable(banco:any): void {
                banco.transaction((tx:any) => {
                    tx.executeSql('DROP TABLE IF EXISTS Sexo;');
                                    
                        }, (error:any)=>{
                            console.log('SexoTable : Delete Configuracoes ERROR: ' + error.message);
                        }, ()=>{
                            console.log('SexoTable : Delete table Configuracoes OK');
                        });
                } 

                public createTable(banco: any): void {
                    banco.transaction((tx:any) => {
                    tx.executeSql("CREATE TABLE IF NOT EXISTS Sexo ( "
                            + "id INTEGER PRIMARY KEY," 
                            + "nome TEXT NOT NULL)");
                                    
                        }, (error:any)=>{
                            console.log('SexoTable : Create Sexo ERROR: ' + error.message);
                        }, ()=>{
                            console.log('SexoTable : Create table Sexo OK');
                        });                       
                }
        }
}