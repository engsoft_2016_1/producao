/// <reference path="../../../interfaceBD/itable.ts" />

module StorageDAO{
    
     /**
      * Classe responsável por manter as informações da tabela de 
      * EfeitosColaterais para inicializar o banco de dados.
      */
        export class PrincipioativoContraindicacoesTable implements ITable {

                constructor(){

                }
                public populateTable(banco:any){
                    banco.transaction((tx:any)=>{
                        let campos = '(principioativoId,contraindicacaoId)';
                        let casas = '(?,?)';
                        //ARTOVA:::Hepatopatia
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[1,1]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[2,1]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[3,1]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[4,1]);
                        //ARTOVA:::Miopatia                          
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[1,2]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[2,2]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[3,2]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[4,2]);
                        //ARTOVA:::Entre 2 e 7 anos.     
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[1,3]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[2,3]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[3,3]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[4,3]);
                        //ARTOVA:::Grávida ou Lactante    
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[1,4]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[2,4]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[3,4]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[4,4]);
                       //ARTOVA:::Polifarmácia  
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[1,12]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[2,12]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[3,12]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[4,12]);
                        //ARTOVA:::Mulher em idade fértil    
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[1,5]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[2,5]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[3,5]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[4,5]);
                        //FLUVA:::Diabéticos  
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[5,7]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[6,7]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[7,7]);
                        //FLUVA:::Problemas na tireóide  
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[5,6]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[6,6]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[7,6]);
                        //FLUVA:::Problemas renais. 
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[5,8]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[6,8]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[7,8]);
                        //FLUVA:::Hepatopatia.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[5,1]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[6,1]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[7,1]);
                        //FLUVA:::Alcoolismo.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[5,9]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[6,9]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[7,9]);
                        //FLUVA:::Miopatia.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[5,2]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[6,2]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[7,2]);
                        //FLUVA:::Entre 8 e 18 anos.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[5,10]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[6,10]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[7,10]);        
                        //PITA:::Hepatopatia
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[8,1]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[9,1]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[10,1]);
                        //PITA:::Alcoolismo
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[8,9]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[9,9]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[10,9]); 
                        //PITA:::Miopatia
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[8,2]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[9,2]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[10,2]);            
                        //PITA:::Grávida ou Lactente
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[8,4]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[9,4]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[10,4]);
                        //PITA:::Problemas renais.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[8,8]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[9,8]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[10,8]);
                        //PITA:::Idoso.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[8,11]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[9,11]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[10,11]);
                        //LOVA:::Hepatopatia.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[11,1]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[12,1]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[13,1]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[14,1]);
                        //LOVA:::Miopatia.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[11,2]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[12,2]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[13,2]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[14,2]); 
                         //LOVA:::Entre 2 e 7 anos.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[11,3]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[12,3]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[13,3]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[14,3]);
                         //LOVA:::Grávida ou lactante.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[11,4]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[12,4]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[13,4]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[14,4]);
                        //PRAVA:::Hepatopatia.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[15,1]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[16,1]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[17,1]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[18,1]);    
                        //PRAVA:::Alcoolismo.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[15,9]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[16,9]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[17,9]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[18,9]);
                        //PRAVA:::Miopatia.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[15,2]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[16,2]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[17,2]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[18,2]);
                        //PRAVA:::Grávida ou Lactente
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[15,4]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[16,4]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[17,4]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[18,4]);
                         //PRAVA:::Entre 2 e 7 anos.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[15,3]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[16,3]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[17,3]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[18,3]);
                         //PRAVA:::Problemas renais.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[15,8]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[16,8]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[17,8]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[18,8]);    
                        //ROSU:::Hepatopatia.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[19,1]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[20,1]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[21,1]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[22,1]);
                        //ROSU:::Polifarmácia.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[19,12]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[20,12]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[21,12]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[22,12]);
                        //ROSU:::Grávida ou Lactente.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[19,4]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[20,4]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[21,4]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[22,4]);
                        //ROSU:::Alcoolismo.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[19,9]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[20,9]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[21,9]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[22,9]);
                        //ROSU:::Miopatia.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[19,2]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[20,2]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[21,2]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[22,2]);    
                        //EZET+SINV:::Hepatopatia.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[23,1]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[24,1]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[25,1]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[26,1]);
                        //EZET+SINV:::Grávida ou Lactente.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[23,4]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[24,4]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[25,4]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[26,4]);
                        //SIMVA:::Hepatopatia.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[27,1]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[28,1]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[29,1]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[30,1]);
                        //SIMVA:::Miopatia.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[27,2]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[28,2]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[29,2]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[30,2]);
                        //SIMVA:::Entre 2 e 7 anos.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[27,3]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[28,3]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[29,3]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[30,3]);    
                        //SIMVA:::Grávida ou Lactente.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[27,4]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[28,4]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[29,4]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[30,4]);
                        //SIMVA:::Idoso.
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[28,11]);
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[29,11]);                
                            tx.executeSql('INSERT INTO PrincipioativoPossuiContraindicacao '+campos+' VALUES '+casas,[30,11]);
                            
                        }, (error:any)=>{
                            console.log('PrincipioativoContraindicacoesTable : Transaction ERROR: ' + error.message);
                        }, ()=>{
                            console.log('PrincipioativoContraindicacoesTable : Populated table OK');
                        });	;						
                }

                public dropTable(banco:any): void {
                banco.transaction((tx:any) => {
                    tx.executeSql('DROP TABLE IF EXISTS PrincipioativoPossuiContraindicacao;');
                                    
                        }, (error:any)=>{
                            console.log('PrincipioativoContraindicacoesTable : Delete Configuracoes ERROR: ' + error.message);
                        }, ()=>{
                            console.log('PrincipioativoContraindicacoesTable : Delete table Configuracoes OK');
                        });
                } 

                public createTable(banco: any): void {
                    banco.transaction((tx:any) => {
                    tx.executeSql("CREATE TABLE IF NOT EXISTS PrincipioativoPossuiContraindicacao ( "
                            + " principioativoId INTEGER NOT NULL ,"
                            + " contraindicacaoId INTEGER NOT NULL," 
                            + " FOREIGN KEY (contraindicacaoId) REFERENCES Contraindicacao(id),"
                            + " FOREIGN KEY (principioativoId) REFERENCES Principioativo(id))");
                                    
                        }, (error:any)=>{
                            console.log('PrincipioativoContraindicacoesTable : Create PrincipioativoPossuiContraindicacao ERROR: ' + error.message);
                        }, ()=>{
                            console.log('PrincipioativoContraindicacoesTable : Create table PrincipioativoPossuiContraindicacao OK');
                        });                       
                }
        }
}