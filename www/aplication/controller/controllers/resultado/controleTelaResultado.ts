/// <reference path="../../../service/componentesAplicacao/params/businessObject/params.ts" />

/**
 * Controller da resultado
 */
class ResultadoController{
  
        constructor($scope:any,  $ionicPopup: any , $state: any, $stateParams:any,
        params:ParamsService.Params, $actionButton: any){
              
                    $scope.resultado = params.getResultado();
                    $scope.melhorNivel = (nivel:number)=>{
                        let niveis = ['balanced',
                                      'calm',
                                      'energized',
                                      'assertive',
                                      'assertive',
                                      'assertive'];
                        return niveis[nivel];
                    }
                    $scope.mostrarEstatinas = ()=>{
                        if(params.getResultado().getReducaoLdl() <= 0){
                            showAlert('Não é recomendado o uso de estatinas.',
                                'Seu colesterol LDL-C está bom. Procure fazer'
                                +' exercícios e se alimentar corretamente.',
                                null);
                        }else{
                             $state.go('estatinas');
                        }
                    }

                    let showAlert = (title:string, msg:string, callback:any)=>{
                    var alertPopup = $ionicPopup.alert({
                        title: title,
                        template: msg 
                    });

                    alertPopup.then((res:any)=> {
                            if(callback !=  null){
                                callback();
                            }
                            
                    });
                    };

                    
               
                
        }
}
 