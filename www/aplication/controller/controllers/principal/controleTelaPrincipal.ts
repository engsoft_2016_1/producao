/// <reference path="../../../service/componentesNegocio/dadosPaciente/businessObject/dadosPaciente.ts" />
/// <reference path="../../../service/componentesNegocio/calculadoraColesterol/businessObject/calculoColesterol.ts" />
/// <reference path="../../../service/componentesNegocio/prescritorPrincipioativo/interfaces/iservicePrescritor.ts" />
/// <reference path="../../../service/componentesNegocio/avaliador/interfaces/iregras.ts" />
/// <reference path="../../../service/componentesNegocio/avaliador/interfaces/iserviceAvaliador.ts" />
/// <reference path="../../../service/componentesNegocio/referencialColesterol/interfaces/ifactory.ts" />
/// <reference path="../../../service/componentesNegocio/referencialColesterol/businessObject/referencialColesterol.ts" />
/// <reference path="../../../service/componentesNegocio/resultadoColesterol/interfaces/iresultadoColesterol.ts" />
/// <reference path="../../../service/componentesNegocio/contraindicacoes/interfaces/icontraindicacoesService.ts" />
/// <reference path="../../../service/componentesAplicacao/params/businessObject/params.ts" />

/**
 * Controller da tela principal
 */
class PrincipalController{
   /**
    * @param scope 
    * @param ionicModal 
    * @param dao  @type Storage
    */
        constructor($scope:any,  $ionicPopup: any , $state: any,
          prescritor: PrescritorPrincipioativo.IServicePrescritor,
          resultadoPaciente: ResultadoColesterol.IResultadoColesterol,
          contraindicacoes: ContraindicacoesService.IContraindicacoesService,
          params:ParamsService.Params,
          $ionicPopover:any){
            
             $scope.referencial = '3';//padrão para ambas as idades
             $scope.contraindicacoes = [];
             $scope.ct = {value:''};
             $scope.tg = {value:''};
             $scope.hdl = {value:''};

             let paciente = new DadosPacienteService.Paciente();
                
               document.addEventListener('bancoPronto',  ()=>{
                  let obterContraindicacoes =  async()=>{
                   
                   await  contraindicacoes.obterContraindicacoes(
                       Number($scope.sexo),
                        Number($scope.referencial)).then(
                           (res:StorageDAO.Contraindicacao[])=>{
                            
                                let itens:any[] = [];
                                res.forEach(e=>{
                                    itens.push({selected:false,item: e});   
                                });
                    
                                $scope.contraindicacoes = itens;
                                $scope.$apply();

                            /**
                             * Evento para ser lançado quando os dados da tela estiver
                             *  totalmente carregada.
                             *  @event telapronta
                             */
                                let event = new CustomEvent("telapronta", {});
                                // Dispatch/Trigger/Fire the event
                                document.dispatchEvent(event);
                           }
                       ).catch(
                           (err:Error)=>{
                               console.log('Contraindicações : '+err.message);
                            }
                        );    
                }
            

/**
 * Função que inicia os watcher depois que o dispositivo estiver pronto.
 */
               let initWatchers = () =>{
                        $scope.$watch('choose.selected', (newValue:any, oldValue:any) =>{
                            console.log('choose.selected alterado!');
                            $scope.sexo = newValue;
                            obterContraindicacoes();
                        });
                        $scope.$watch('faixaEtaria.selected', (newValue:any, oldValue:any)=> {
                            console.log('faixaEtaria.selected alterado!');
                            if(  newValue === '0' ){
                                $scope.referencial = ReferencialColesterol.Criancas2Ate19Anos.keyReferencial;
                            }else{
                                $scope.referencial = ReferencialColesterol.AdultosApartir20Anos.keyReferencial;
                            }
                            obterContraindicacoes();
                        });
               }

               initWatchers();
                        $scope.choose = {
                            masculino: '1',
                            feminino: '0',
                            selected: '1'
                        };//padrão para ambos os sexos

                        $scope.faixaEtaria = {
                            maiorDe20anos: '1',
                            menorDe20anos: '0',
                            selected: '1'
                        };
                        $scope.$apply();
               });
                
               
                        $scope.choose = {
                            masculino: '1',
                            feminino: '0',
                            selected: '1'
                        };//padrão para ambos os sexos

                        $scope.faixaEtaria = {
                            maiorDe20anos: '1',
                            menorDe20anos: '0',
                            selected: '1'
                        };
                console.log('choose: = '+ $scope.choose);
                console.log('choose: = '+ $scope.faixaEtaria);
                
                //Exemplo de uso do consultarPrincipioativo.         
                $scope.calcularColesterol = async () =>{
                   
                    let ct:number = $scope.ct.value;
                    let tg:number = $scope.tg.value;
                    let hdl: number = $scope.hdl.value;
                    let sexo: number = Number($scope.choose.selected);
                    let faixaEtaria : number = Number($scope.referencial);
                    
                    if(validar(ct,tg,hdl).length > 0){
                        return;
                    }

                    paciente =  new DadosPacienteService.Paciente(
                         faixaEtaria,sexo,ct,hdl,tg,[]);
                    
                    let contraindicacoes:any[] = [];
                    $scope.contraindicacoes.forEach((e:any)=>{
                        if(e.selected){
                            contraindicacoes.push(e.item.getId());
                        }
                    });
                     
                     paciente.setContraindicacoes(contraindicacoes);
                  
                  let informacoes: ResultadoColesterol.Informacoes;
                     await resultadoPaciente.obterResultado(paciente).then(
                            (res:ResultadoColesterol.Informacoes)=>{
                                    console.log('Teste nivelLDL: ='+res.getNivelLdl());
                                    console.log('Teste nivelHDL: ='+res.getNivelHdl());
                                    console.log('Teste nivelTG: =' + res.getNivelTriglicerides());
                                    console.log('Teste nivelCT: =' + res.getNivelColesterolTotal());
                                    console.log('Teste redução Ldl: =' + res.getReducaoLdl());
                                    console.log('Teste aumento Hdl: =' + res.getAumentoHdl());
                                    console.log('Teste redução TG: =' + res.getReducaoTriglicerides());
                                    informacoes = res; 
                            }
                        ).catch(
                            (err:Error)=>{
                                    console.log('ControllerPrincipal : '+err.message);
                             }
                        );
                             
                        await prescritor.consultarPrincipioativo(paciente).then(
                            (res:PrescritorPrincipioativo.Item[])=>{
                                params.setResultado(informacoes);
                                params.setEstatinas(res);
                                $state.go('resultado');
                        }).catch(((err: Error)=>{
                              showAlert('Desculpe o incoveniente...',err.message, null);
                        }));
                        
                        
                           
                }
               

                /**
                 * Esta função valida o valor do colesterol total.
                 * @param ct 
                 */
               let validarCT = (ct:any) : Error => {
                    if( ct == undefined){

                        return new Error('Digite o valor do colesterol total (CT).');
                     }
                    if( ct > 99999){
                            return new Error('O valor do Colesterol total não é válido.(CT)');
                    }
                           return undefined;
                }
                /**
                 * Valida o valor do triglicerido.
                 * @param tg 
                 */
                let validarTG = (tg:any) : Error => {
                    
                    if( tg == undefined){
                            return new Error('Digite o valor do Triglicerides (TG).');
                    }  
                    if( tg > 400){
                            return new Error('O valor do triglicerídeos não pode ser maior que 400.');
                    }       
                    return undefined;             
                }
                /**
                 * Valida o valor do HDL.
                 * @param hdl 
                 */
                let validarHDL = (hdl:any) : Error => {
                    
                    if( hdl == undefined){
                            return new Error('Digite o valor do HDL (HDL).');
                    }
                    if( hdl > 99999){
                            return new Error('O valor do HDL-colesterol não é válido.(HDL-c)');
                    }
                        return undefined;
                }
                /**
                 * Reune todos os validadores dos inputs da tela.
                 * @param ct 
                 * @param tg 
                 * @param hdl 
                 */
                let validar = (ct:any,tg:any,hdl:any):Error[] => {
                    let a:Error[] = [] ;
                    if(validarCT(ct) != undefined){
                        a.push(validarCT(ct));
                    }
                    if(validarTG(tg) != undefined){
                        a.push(validarTG(tg));
                    }
                    if(validarHDL(hdl) != undefined){
                        a.push(validarHDL(hdl)); 
                    }
                    
                    
                    $scope.validar = a;
                    return a;
                }

                /**Este alerte mostra um simples popup com um botão ok 
                *  @param title @type string 
                *  @description Recebe uma string que será o título da aplicação.
                *  @param msg   @type string
                *  @description Recebe uma string que será exibida no corpo do alert.
                *  @param callback @type function
                *  @description Recebe uma função que será executada ao clicar no botão ok.
                */
                
                let showAlert = (title:string, msg:string, callback:any)=>{
                    var alertPopup = $ionicPopup.alert({
                        title: title,
                        template: msg 
                    });

                    alertPopup.then((res:any)=> {
                            if(callback !=  null){
                                callback();
                            }
                            
                    });
                    };

            // .fromTemplate() method
           $scope.message = "testando o popover asfkahlfkhsdfuwqef wiefhijhksdlh lqiuwhfklasjhf qiuehfalksjdfh qiwerhkladsjfh";
            $ionicPopover.fromTemplateUrl('aplication/view/popovers/templatePopover.html', {
                scope: $scope
            }).then(function(popover:any) {
                $scope.popover = popover;
            })

            $scope.openPopover = function($event:any) {
                $scope.popover.show($event);
            };
            $scope.closePopover = function() {
                $scope.popover.hide();
            };

        }

        
}