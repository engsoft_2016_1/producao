
/* Filtro responsável por exibir frações em porcentagem */
app.filter('percentResultado', function() {
    return function(x) {
        var res = Number(x);
        var per = res < 0 ? 0 : res;
        
        return (per).toFixed(2)+'%';
    };
});

/* Filtro responsável por exibir frações em porcentagem */
app.filter('percentPrincipioativo', function() {
    return function(x) {
        var p = Number(x);

        return (p*100).toFixed(0)+'%';
    };
});