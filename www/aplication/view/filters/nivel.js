/* Filtro responsável por exibir frações em porcentagem */
app.filter('nivel', function() {
    return function(x) {
        var niveis = ['Ótimo',
                      'Desejável',
                      'Limítrofe',
                      'Alto',
                      'Muito alto',
                      'Baixo'];
   
        var p = Number(x);
        return niveis[p];
    };
});