
// An alert dialog
/**Este alerte mostra um simples popup com um botão ok 
*  @param title @type string 
*  @description Recebe uma string que será o título da aplicação.
*  @param msg   @type string
*  @description Recebe uma string que será exibida no corpo do alert.
*  @param callback @type function
*  @description Recebe uma função que será executada ao clicar no botão ok.
*  @param $ionicPopup @type any
*  @description modulo do ionic responsável por inicar o alert.
*/
function showAlert(title,msg, callback, $ionicPopup) {
   var alertPopup = $ionicPopup.alert({
     title: title,
     template: msg 
   });

   alertPopup.then(function(res) {
        if(callback()){
            callback();
        }
        
   });
 };
